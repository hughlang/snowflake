﻿package com.motiondraw
{

    public class LineGeneralization extends Object
    {

        public function LineGeneralization()
        {
            return;
        }// end function

        private function recursiveToleranceBar_old(param1, param2, param3, param4) : Number
        {
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_13:* = undefined;
            var _loc_15:* = undefined;
            var _loc_5:* = param3;
            _loc_6 = param1[param2];
            _loc_8 = {x:param1[param2 + _loc_5].x - _loc_6.x, y:param1[param2 + _loc_5].y - _loc_6.y};
            var _loc_14:* = 1;
            while (_loc_14 <= _loc_5)
            {
                
                _loc_7 = param1[param2 + _loc_14];
                _loc_9 = {x:_loc_7.x - _loc_6.x, y:_loc_7.y - _loc_6.y};
                _loc_10 = Math.acos((_loc_8.x * _loc_9.x + _loc_8.y * _loc_9.y) / (Math.sqrt(_loc_8.y * _loc_8.y + _loc_8.x * _loc_8.x) * Math.sqrt(_loc_9.y * _loc_9.y + _loc_9.x * _loc_9.x)));
                if (isNaN(_loc_10))
                {
                    _loc_10 = 0;
                }
                _loc_11 = _loc_6.x - _loc_7.x;
                _loc_12 = _loc_6.y - _loc_7.y;
                _loc_15 = Math.sqrt(_loc_11 * _loc_11 + _loc_12 * _loc_12);
                if (Math.sin(_loc_10) * _loc_15 >= param4)
                {
                    _loc_5 = _loc_5 - 1;
                    if (_loc_5 > 0)
                    {
                        _loc_13 = recursiveToleranceBar(param1, param2, _loc_5, param4);
                        break;
                    }
                    else
                    {
                        _loc_13 = 0;
                        break;
                    }
                }
                _loc_14 = _loc_14 + 1;
            }
            return _loc_13;
        }// end function

        private static function recursiveToleranceBar(param1, param2, param3, param4) : Number
        {
            var _loc_6:* = undefined;
            var _loc_7:* = undefined;
            var _loc_8:* = undefined;
            var _loc_9:* = undefined;
            var _loc_10:* = undefined;
            var _loc_11:* = undefined;
            var _loc_12:* = undefined;
            var _loc_14:* = undefined;
            var _loc_5:* = param3;
            _loc_6 = param1[param2];
            if (!param1[param2 + _loc_5])
            {
                return 0;
            }
            _loc_8 = {x:param1[param2 + _loc_5].x - _loc_6.x, y:param1[param2 + _loc_5].y - _loc_6.y};
            var _loc_13:* = 1;
            while (_loc_13 <= _loc_5)
            {
                
                _loc_7 = param1[param2 + _loc_13];
                _loc_9 = {x:_loc_7.x - _loc_6.x, y:_loc_7.y - _loc_6.y};
                _loc_10 = Math.acos((_loc_8.x * _loc_9.x + _loc_8.y * _loc_9.y) / (Math.sqrt(_loc_8.y * _loc_8.y + _loc_8.x * _loc_8.x) * Math.sqrt(_loc_9.y * _loc_9.y + _loc_9.x * _loc_9.x)));
                if (isNaN(_loc_10))
                {
                    _loc_10 = 0;
                }
                _loc_11 = _loc_6.x - _loc_7.x;
                _loc_12 = _loc_6.y - _loc_7.y;
                _loc_14 = Math.sqrt(_loc_11 * _loc_11 + _loc_12 * _loc_12);
                if (Math.sin(_loc_10) * _loc_14 >= param4)
                {
                    _loc_5 = _loc_5 - 1;
                    if (_loc_5 > 0)
                    {
                        return recursiveToleranceBar(param1, param2, _loc_5, param4);
                    }
                    return 0;
                }
                _loc_13 = _loc_13 + 1;
            }
            return _loc_5;
        }// end function

        public static function smoothMcMaster(param1:Array) : Array
        {
            var _loc_4:* = undefined;
            var _loc_5:* = undefined;
            var _loc_6:* = undefined;
            var _loc_2:* = [];
            var _loc_3:* = param1.length;
            if (_loc_3 < 5)
            {
                return param1;
            }
            var _loc_7:* = _loc_3;
            while (_loc_7--)
            {
                
                if (_loc_7 == (_loc_3 - 1) || _loc_7 == _loc_3 - 2 || _loc_7 == 1 || _loc_7 == 0)
                {
                    _loc_2[_loc_7] = {x:param1[_loc_7].x, y:param1[_loc_7].y};
                    continue;
                }
                _loc_4 = 5;
                _loc_5 = 0;
                _loc_6 = 0;
                while (_loc_4--)
                {
                    
                    _loc_5 = _loc_5 + param1[_loc_7 + 2 - _loc_4].x;
                    _loc_6 = _loc_6 + param1[_loc_7 + 2 - _loc_4].y;
                }
                _loc_5 = _loc_5 / 5;
                _loc_6 = _loc_6 / 5;
                var _loc_8:* = {x:(param1[_loc_7].x + _loc_5) / 2, y:(param1[_loc_7].y + _loc_6) / 2};
                _loc_2[_loc_7] = {x:(param1[_loc_7].x + _loc_5) / 2, y:(param1[_loc_7].y + _loc_6) / 2};
                _loc_2[_loc_7] = _loc_8;
            }
            return _loc_2;
        }// end function

        public static function simplifyLang(param1:Number, param2:Number, param3:Array) : Array
        {
            var _loc_5:* = NaN;
            var _loc_6:* = NaN;
            var _loc_7:* = NaN;
            if (param1 <= 1 || param3.length < 3)
            {
                return param3;
            }
            var _loc_4:* = new Array();
            _loc_6 = param3.length;
            if (param1 > (_loc_6 - 1))
            {
                param1 = _loc_6 - 1;
            }
            _loc_4[0] = {x:param3[0].x, y:param3[0].y};
            _loc_7 = 1;
            var _loc_8:* = 0;
            while (_loc_8 < _loc_6)
            {
                
                if (_loc_8 + param1 > _loc_6)
                {
                    param1 = _loc_6 - _loc_8 - 1;
                }
                _loc_5 = recursiveToleranceBar(param3, _loc_8, param1, param2);
                if (_loc_5 > 0 && param3[_loc_8 + _loc_5])
                {
                    _loc_4[_loc_7] = {x:param3[_loc_8 + _loc_5].x, y:param3[_loc_8 + _loc_5].y};
                    _loc_8 = _loc_8 + (_loc_5 - 1);
                    _loc_7 = _loc_7 + 1;
                }
                _loc_8 = _loc_8 + 1;
            }
            _loc_4[(_loc_7 - 1)] = {x:param3[(_loc_6 - 1)].x, y:param3[(_loc_6 - 1)].y};
            return _loc_4;
        }// end function

    }
}
