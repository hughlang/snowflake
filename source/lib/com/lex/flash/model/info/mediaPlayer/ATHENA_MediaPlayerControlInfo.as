package com.lex.flash.model.info.mediaPlayer {

	/**
	 * @author hugh
	 */
	public class ATHENA_MediaPlayerControlInfo 
	{
		public static const BACKGROUND_BAR : String = "buffer_bar";
		public static const BUFFER_BAR : String = "buffer_bar";
		public static const BUFFER_ANIM : String = "buffer_anim";
		public static const CAPTION_BUTTON : String = "caption_button";
		public static const CAPTION: String = "caption";
		public static const FULLSCREEN_BUTTON : String = "fullscreen_button";
		public static const MUTE_BUTTON : String = "mute_button";
		public static const PLAY_PAUSE_BUTTON : String = "play_pause_button";
		public static const PROGRESS_BAR : String = "progress_bar";
		public static const REWIND_BUTTON : String = "rewind_button";
		public static const SCRUBBER : String = "scrubber";
		public static const TIME_CODE : String = "time_code";
		public static const VOLUME_SCRUBBER : String = "volume_scrubber";
	}
}
