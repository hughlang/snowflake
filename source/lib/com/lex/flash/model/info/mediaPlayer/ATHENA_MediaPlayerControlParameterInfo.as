package com.lex.flash.model.info.mediaPlayer {
	import flash.display.DisplayObject;
	import flash.geom.Rectangle;

	/**
	 * @author hugh
	 */
	public class ATHENA_MediaPlayerControlParameterInfo 
	{
		private var _behavior : String;
		private var _asset : DisplayObject;
		private var _bounds : Rectangle;
		private var _tabIndex : int;
		
		
		public function ATHENA_MediaPlayerControlParameterInfo( behavior:String, asset:DisplayObject, bounds:Rectangle=null, tabIndex:int=1 )
		{
			_behavior = behavior;
			_asset = asset;
			_bounds = bounds;
			_tabIndex = tabIndex;
		}
		
		
		public function set behavior( str:String ) : void
		{
			_behavior = str;
		}
		public function get behavior() : String
		{
			return _behavior;
		}
		
		
		public function set asset( dispObj:DisplayObject ) : void
		{
			_asset = dispObj;
		}
		public function get asset() : DisplayObject
		{
			return _asset;
		}
		
		
		public function set bounds( rect:Rectangle ) : void
		{
			_bounds = rect;
		}
		public function get bounds() : Rectangle
		{
			return _bounds;
		}
		
		
		public function set tabIndex( num:int ) : void
		{
			_tabIndex = num;
		}
		public function get tabIndex() : int
		{
			return _tabIndex;
		}
		
		
	}
}
