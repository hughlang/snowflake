package com.lex.flash.model.info.mediaElement {

	/**
	 * @author hugh
	 */
	public class ATHENA_MediaElementInfo 
	{
		private var _url : String;
		private var _titleLabel : String;
		private var _descriptionLabel : String;
		private var _thumbnail : String;
		private var _deeplink : String;
		private var _captionFile : String;
		
		public function set url( str:String ) : void
		{
			_url = str;
		}
		public function get url() : String
		{
			return _url;
		}
		
		
		public function set titleLabel( str:String ) : void
		{
			_titleLabel = str;
		}
		public function get titleLabel() : String
		{
			return _titleLabel;
		}
		
		
		public function set descriptionLabel( str:String ) : void
		{
			_descriptionLabel = str;
		}
		public function get descriptionLabel() : String
		{
			return _descriptionLabel;
		}
		
		
		public function set thumbnail( str:String ) : void
		{
			_thumbnail = str;
		}
		public function get thumbnail() : String
		{
			return _thumbnail;
		}
		
		
		public function set deeplink( str:String ) : void
		{
			_deeplink = str;
		}
		public function get deeplink() : String
		{
			return _deeplink;
		}
		
		
		public function set captionFile( str:String ) : void
		{
			_captionFile = str;
		}
		public function get captionFile() : String
		{
			return _captionFile;
		}
		
		
		
	}
}
