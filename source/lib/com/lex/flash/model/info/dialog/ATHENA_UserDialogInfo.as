package com.lex.flash.model.info.dialog {

	/**
	 * @author hugh
	 */
	public class ATHENA_UserDialogInfo 
	{
		private var _id : String;
		private var _title : String;
		private var _msg : String;
		private var _icon : String;
		
		public function set id( str:String ) : void
		{
			_id = str;
		}
		public function get id() : String
		{
			return _id;
		}
		
		public function set title( str:String ) : void
		{
			_title = str;
		}
		public function get title() : String
		{
			return _title;
		}
		
		public function set msg( str:String ) : void
		{
			_msg = str;
		}
		public function get msg() : String
		{
			return _msg;
		}
		
		public function set icon( str:String ) : void
		{
			_icon = str;
		}
		public function get icon() : String
		{
			return _icon;
		}
		
		
	}
}
