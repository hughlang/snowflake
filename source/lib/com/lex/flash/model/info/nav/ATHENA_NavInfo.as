package com.lex.flash.model.info.nav {

	/**
	 * @author hugh
	 */
	public class ATHENA_NavInfo 
	{
		private var _linkageId : String;
		private var _className : String;
		private var _x : Number;
		private var _y : Number;
		private var _navItems : Vector.<ATHENA_NavItemInfo>;
		
		public function set linkageId( str:String ) : void
		{
			_linkageId = str;
		}
		public function get linkageId() : String
		{
			return _linkageId;
		}
		
		
		public function set className( str:String ) : void
		{
			_className = str;
		}
		public function get className() : String
		{
			return _className;
		}
		
		
		public function set x( num:Number ) : void
		{
			_x = num;
		}
		public function get x() : Number
		{
			return _x;
		}
		
		
		public function set y( num:Number ) : void
		{
			_y = num;
		}
		public function get y() : Number
		{
			return _y;
		}
		
		
		public function set navItems( vect:Vector.<ATHENA_NavItemInfo> ) : void
		{
			_navItems = vect;
		}
		public function get navItems() : Vector.<ATHENA_NavItemInfo>
		{
			return _navItems;
		}
		
		
		public function ATHENA_NavInfo()
		{
			_navItems = new Vector.<ATHENA_NavItemInfo>();
		}
		
	}
}
