package com.lex.flash.model.info.nav {

	/**
	 * @author hugh
	 */
	public class ATHENA_NavItemInfo 
	{
		private var _id : String;
		private var _title : String;
		private var _deeplink : String;
		private var _linkageId : String;
		private var _className : String;
		private var _parameter : *;
		private var _thumbnail : String;
		
		public function set id( str:String ) : void
		{
			_id = str;
		}
		public function get id() : String
		{
			return _id;
		}
		
		
		public function set title( str:String ) : void
		{
			_title = str;
		}
		public function get title() : String
		{
			return _title;
		}
		
		
		public function set deeplink( str:String ) : void
		{
			_deeplink = str;
		}
		public function get deeplink() : String
		{
			return _deeplink;
		}
		
		
		public function set linkageId( str:String ) : void
		{
			_linkageId = str;
		}
		public function get linkageId() : String
		{
			return _linkageId;
		}
		
		
		public function set className( str:String ) : void
		{
			_className = str;
		}
		public function get className() : String
		{
			return _className;
		}
		
		
		public function set parameter( param:* ) : void
		{
			_parameter = param;
		}
		public function get parameter() : *
		{
			return _parameter;
		}
		
		
		public function set thumbnail( str:String ) : void
		{
			_thumbnail = str;
		}
		public function get thumbnail() : String
		{
			return _thumbnail;
		}
		
		
		
	}
}
