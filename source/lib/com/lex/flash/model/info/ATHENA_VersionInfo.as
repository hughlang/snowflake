/**
 * @author ahunt
 */

package com.lex.flash.model.info {

	/* You can edit this by hand, or udpate automatically via the ANT Build-Release and Build-Test scripts */
	public class ATHENA_VersionInfo 
	{
		/* WARNING: Do not change the whitespace formatting for any of these values!! */
		static public var majorRelease : String = "0";
		static public var versionBuild : String = "0"; 
		static public var versionTime : String = "October 13, 2010 15:11:43"; 
	}
}