package com.lex.flash.model.info.error {

	/**
	 * @author hugh
	 */
	
	public class ATHENA_ErrorMessagesInfo 
	{
		public var messages : Array;
		
		public function ErrorMessageInfo() : void
		{
			messages = new Array();
		}
		
	 	public static function parseInfo( xml : XML ) : ATHENA_ErrorMessagesInfo
		{
			if( !xml ) return null;
			
			var res : ATHENA_ErrorMessagesInfo = new ATHENA_ErrorMessagesInfo();
			
			res.messages = parseXmlToErrorMessage( xml.ErrorMessages );
			
			return res;
		}
		
		private static function parseXmlToErrorMessage( xml:XMLList ) : Array
		{
			var errors : Array = [];
			var errorItem : ATHENA_ErrorItemInfo;
			var errorList : XMLList = xml.children();
			
			for (var i : Number = 0; i < errorList.length(); i++) {
				errorItem = new ATHENA_ErrorItemInfo();
				errorItem.id = errorList[i].Id;
				errorItem.label = errorList[i].Label;
				errorItem.message = errorList[i].Message;
				errorItem.categoryId = errorList[i].CategoryId;
				errorItem.categoryName = errorList[i].CategoryName;
				errorItem.itemId = errorList[i].ItemId;
				errorItem.itemName = errorList[i].ItemName;
				errors.push( errorItem );
			}
			return errors;
		}
		
	}
}
