package com.lex.flash.model.info.error {

	/**
	 * @author hugh
	 */
	public class ATHENA_ErrorCodeInfo 
	{
		public static function lookUpErrorCode( code:Number ) : String
		{
			var str : String;
			
			switch( code ) {
				case 400:
					str = "Bad request";
					break;
				case 401:
					str = "Unautorized";
					break;
				case 403:
					str = "Forbidden";
					break;
				case 500:
					str = "Internal Server Error";
					break;
				case 503:
					str = "Service Unavailable";
					break;
			}
			return str;
		}
	}
}
