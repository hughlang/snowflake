package com.lex.flash {

	/**
	 * @author hugh
	 */
	public interface ATHENA_IDispose 
	{
		function dispose() : void;
	}
}
