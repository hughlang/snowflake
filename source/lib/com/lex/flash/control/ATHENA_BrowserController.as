package com.lex.flash.control {
	import com.asual.swfaddress.SWFAddress;
	import com.asual.swfaddress.SWFAddressEvent;
	import com.lex.flash.events.ATHENA_NavEvent;

	import flash.external.ExternalInterface;
	import flash.system.Capabilities;

	/**
	 * @author hugh
	 */
	public class ATHENA_BrowserController 
	{
		//Singleton instance
		private static var inst : ATHENA_BrowserController;
		
		//event dispatcher
		private static var _eventControl : ATHENA_EventController;
		
		//----------------------------------------------------------------------------------------------------------------
		//current address
		private static var _SWFAddressStr : String;
		
		public function set address( str:String ) : void
		{
			//if ( !isIDE() ) {
				_SWFAddressStr = str;
				//Log.i( "Address: _SWFAddressStr = " + _SWFAddressStr );
				SWFAddress.setValue( _SWFAddressStr );
			//}
		}
		
		public function get address() : String
		{
			return _SWFAddressStr;
		}
		
		//----------------------------------------------------------------------------------------------------------------
		public function set title( str:String ) : void
		{
			SWFAddress.setTitle( str );
		}
		
		public function get title() : String
		{
			return SWFAddress.getTitle();
		}
		

		//----------------------------------------------------------------------------------------------------------------
		public function set status( str:String ) : void
		{
			SWFAddress.setStatus( str );
		}
		
		public function get status() : String
		{
			return SWFAddress.getStatus();
		}
		

		public function resetStatus() : void
		{
			SWFAddress.resetStatus();
		}
		

		//----------------------------------------------------------------------------------------------------------------
		public function back() : void
		{
			SWFAddress.back();
		}
		
		public function forward() : void
		{
			SWFAddress.forward();
		}
		
		public function go( delta:Number ) : void
		{
			SWFAddress.go(delta);
		}
		
		//----------------------------------------------------------------------------------------------------------------
		public static function getParameter( param:String) : String
		{
			return SWFAddress.getParameter( param ); 
		}
		
		public static function get parameterNames() : Array
		{
			return SWFAddress.getParameterNames(); 
		}

		public static function get path() : String
		{
			return SWFAddress.getPath(); 
		}
		
		public static function get queryString() : String
		{
			return SWFAddress.getQueryString(); 
		}
		
		
		//----------------------------------------------------------------------------------------------------------------
		public function href( url:String, target:String ) : void
		{
			SWFAddress.href(url, target);
		}
		
		public function popup( url:String, name:String, options:String, handler:String ) : void
		{
			SWFAddress.popup(url, name, options, handler);
		}
		


		//==================================================================================================================
		/**
		 * contructor.
		 * 
		 * @return nothing
		 */	
		public function ATHENA_BrowserController():void
		{
		}
		
		/**
		 * singleton.
		 * 
		 * @return instance of DataLoader
		 */	
		public static function getInstance():ATHENA_BrowserController{
			if( inst == null ) {
				inst = new ATHENA_BrowserController();
				//if ( !isIDE() ) {
					init();
				//}
			}
			
			return inst;
		}
		
		public static function isIDE() : Boolean
		{
			if ( Capabilities.playerType.toLowerCase() == "external" ) {
				return true;
			} else {
				return false;
			}
		}
		
		
		private static function init() : void
		{
			//assign function to receive browser messages
			//SWFAddress.onChange = onSWFAddressChange;
			SWFAddress.addEventListener( SWFAddressEvent.INIT, onSWFAddressInit );
			SWFAddress.addEventListener( SWFAddressEvent.CHANGE, onSWFAddressChange );
			
			//get an instance of the event controller
			_eventControl = ATHENA_EventController.getInstance();
		}
		
		//this receive messages from SWFAddress init
		private static function onSWFAddressInit( e:SWFAddressEvent=null ): void
		{
			//Log.i( String("onSWFAddressInit - SWFAddressEvent| value: " + e.value + " | parametersNames: " + e.parametersNames) );
			var addr : String = e.value;

			_eventControl.dispatchEvent( new ATHENA_NavEvent( ATHENA_NavEvent.BROWSER_DEEPLINK, addr ) );

			
		}
		
		//this recieve message from the  browser interaction
		private static function onSWFAddressChange( e:SWFAddressEvent=null ) : void
		{
			var addr:String = SWFAddress.getValue();
			//Log.i( String("onSWFAddressChange - onSWFAddressChange| _SWFAddressStr:" + _SWFAddressStr + " | addr:" + addr + "\n") );
	
			if ( addr ) {
				
				//if ( addr.indexOf( _SWFAddressStr ) > -1 ) {
				//if ( addr == _SWFAddressStr ) {
					//if we just set this then exit
				//	return;
				//} else {
					//this is either a deeplink or a browser history call
					_eventControl.dispatchEvent( new ATHENA_NavEvent( ATHENA_NavEvent.BROWSER_DEEPLINK, addr ) );
					_SWFAddressStr = addr;
				//}
			//} else {
				//nothing
			}
		
		}
		
		public static function checkOS () : String
		{
			var flashPlayerVersion : String = Capabilities.version;

			var osArray : Array = flashPlayerVersion.split( ' ' );
			
			//The operating system :  WIN, MAC, LNX
			var osType : String = osArray[ 0 ]; 
			
			//The player versions. 9,0,115,0
			var versionArray : Array = osArray[ 1 ].split( ',' );
			var majorVersion : Number = parseInt( versionArray[ 0 ] );
			var majorRevision : Number = parseInt( versionArray[ 1 ] );
			var minorVersion : Number = parseInt( versionArray[ 2 ] );
			var minorRevision : Number = parseInt( versionArray[ 3 ] );
			
//			return osType;
			
			trace( "Operating System : " + osType );
			trace( "Major Version : " + majorVersion );
			trace( "Major Revision : " + majorRevision );
			trace( "Minor Version : " + minorVersion );
			trace( "Minor Revision : " + minorRevision );
			trace( "--other capabilities--" );
			trace( "avHardwareDisable : " + Capabilities.avHardwareDisable );
			trace( "hasAccessibility : " + Capabilities.hasAccessibility );
			trace( "hasAudio : " + Capabilities.hasAudio );
			trace( "hasAudioEncoder : " + Capabilities.hasAudioEncoder );
			trace( "hasEmbeddedVideo : " + Capabilities.hasEmbeddedVideo );
			trace( "hasMP3 : " + Capabilities.hasMP3 );
			trace( "hasPrinting : " + Capabilities.hasPrinting );
			trace( "hasScreenBroadcast : " + Capabilities.hasScreenBroadcast );
			trace( "hasScreenPlayback : " + Capabilities.hasScreenPlayback );
			trace( "hasStreamingAudio : " + Capabilities.hasStreamingAudio );
			trace( "hasVideoEncoder : " + Capabilities.hasVideoEncoder );
			trace( "isDebugger : " + Capabilities.isDebugger );
			trace( "language : " + Capabilities.language );
			trace( "localFileReadDisable : " + Capabilities.localFileReadDisable );
			trace( "manufacturer : " + Capabilities.manufacturer );
			trace( "os : " + Capabilities.os );
			trace( "pixelAspectRatio : " + Capabilities.pixelAspectRatio );
			trace( "playerType : " + Capabilities.playerType );
			trace( "screenColor : " + Capabilities.screenColor );
			trace( "screenDPI : " + Capabilities.screenDPI );			
			trace( "screenResolutionX : " + Capabilities.screenResolutionX );
			trace( "screenResolutionY : " + Capabilities.screenResolutionY );
			trace( "serverString : " + Capabilities.serverString );

			return osType;

		}
		
		public static function checkBrowser() : String
		{
			if( ExternalInterface.available )
			{
				var browser : String;
				var strUserAgent : String = String( ExternalInterface.call( "function () { return navigator.userAgent; }" ) ).toLowerCase();
				
				if ( strUserAgent.indexOf( "firefox" ) != -1 )
				{
					browser = "Firefox";
				}
				else
				if ( strUserAgent.indexOf( "msie" ) != -1)
				{
					browser = "Internet Explorer";
				}
				else
				if ( strUserAgent.indexOf( "safari" ) != -1)
				{
					browser = "Safari";
				}
				else
				if ( strUserAgent.indexOf( "chrome" ) != -1)
				{
					browser = "Google Chrome";
				}
				else
				if ( strUserAgent.indexOf( "opera" ) != -1)
				{
					browser = "Opera";
				}
				else
				{
					browser ="none";
				}
				return browser;
			}
			else
			{
				return null;
			}
		}
	}
}
