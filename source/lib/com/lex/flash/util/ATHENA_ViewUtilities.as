package com.lex.flash.util {
	import flash.geom.Matrix;	
	import flash.display.GradientType;	
	import flash.display.LineScaleMode;	
	import flash.display.Sprite;	
	
	/**
	 * @author James Dean
	**/
	public final class ATHENA_ViewUtilities 
	{
		public function ATHENA_ViewUtilities()
		{
			throw( this + "can not be instantiated" );
		}
		
		/** 
		 *  createSprite - creates and returns a named Sprite
		 *  @param spiteName : String - the name of the Sprite
		 *  @return Sprite
		**/
		public static function createSprite( spriteName : String ) : Sprite
		{
			var base : Sprite = new Sprite();
			base.name = spriteName;
			return base;
		}
		
		/** 
		 * drawRectangle - draws a rectangle into the supplied clip.
		 * @return Sprite
		**/
		public static function drawRectangle ( spriteRef : Sprite , xPos : int , yPos : int , width : int , height : int , fill : Array , fillAlpha : Array , fillRatios : Array , fillMatrix : Matrix = null , stroke : uint = 0x000000 , strokeWidth : uint = 0 , strokeAlpha : Number = 0 , fillGradientType : String = "linear" ) : Sprite
		{
			var isGradient : Boolean;
			
			if ( fillMatrix == null )
			{
				isGradient = false;
			}
			else
			{
				isGradient = true;
			}
			
			var art : Sprite = spriteRef;
			var linGrad : String = ( fillGradientType.toLowerCase() != GradientType.LINEAR ) ? GradientType.RADIAL : GradientType.LINEAR;
			
			if ( !isGradient )
			{
				art.graphics.beginFill( fill[ 0 ] , fillAlpha[ 0 ] );
			}
			else
			{
				art.graphics.beginGradientFill( linGrad , fill , fillAlpha , fillRatios , fillMatrix );
			}
			
			//art.graphics.beginFill( fill , fillAlpha );
			//art.graphics.beginGradientFill( linGrad , linColors , linAlphas , linRatios , linMatrix );
			
			art.graphics.lineStyle( strokeWidth , stroke , strokeAlpha , true , LineScaleMode.NONE );
			art.graphics.drawRect( xPos , yPos , width , height );
			art.graphics.endFill();
			
			return art;
		}
		
		/** 
		 * drawRoundedRectangle - draws a rounded rectangle into the supplied clip.
		 * @return Sprite
		**/
		public static function drawRoundedRectangle (  spriteRef : Sprite , xPos : int , yPos : int , width : int , height : int , fill : Array , fillAlpha : Array , fillRatios : Array , fillMatrix : Matrix = null , cornerRadius : uint = 8 , stroke : uint = 0x000000 , strokeWidth : uint = 0 , strokeAlpha : Number = 0 , fillGradientType : String = "linear" ) : Sprite
		{
			
			var isGradient : Boolean;
			
			if ( fillMatrix == null )
			{
				isGradient = false;
			}
			else
			{
				isGradient = true;
			}
			
			var art : Sprite = spriteRef;
			var linGrad : String = ( fillGradientType.toLowerCase() != GradientType.LINEAR ) ? GradientType.RADIAL : GradientType.LINEAR;
			
			if ( !isGradient )
			{
				art.graphics.beginFill( fill[ 0 ] , fillAlpha[ 0 ] );
			}
			else
			{
				art.graphics.beginGradientFill( linGrad , fill , fillAlpha , fillRatios , fillMatrix );
			}
			
			art.graphics.lineStyle( strokeWidth , stroke , strokeAlpha , true , LineScaleMode.NONE );
			art.graphics.drawRoundRectComplex( xPos , yPos , width , height , cornerRadius , cornerRadius , cornerRadius , cornerRadius  );
			art.graphics.endFill();
			
			return art;
		}
		
		/** 
		 * drawRoundedRectangle - draws a rounded rectangle into the supplied clip.
		 * @return Sprite
		**/
		public static function drawRoundedRectangleComplex (  spriteRef : Sprite , xPos : int , yPos : int , width : int , height : int , fill : Array , fillAlpha : Array , fillRatios : Array , fillMatrix : Matrix = null , topLeftRadius : int = 3 , topRightRadius : int = 3 , bottomLeftRadius : int = 3 , bottomRightRadius : int = 3 , stroke : uint = 0x000000 , strokeWidth : uint = 0 , strokeAlpha : Number = 0 , fillGradientType : String = "linear" ) : Sprite
		{
			
			var isGradient : Boolean;
			
			if ( fillMatrix == null )
			{
				isGradient = false;
			}
			else
			{
				isGradient = true;
			}
			
			var art : Sprite = spriteRef;
			var linGrad : String = ( fillGradientType.toLowerCase() != GradientType.LINEAR ) ? GradientType.RADIAL : GradientType.LINEAR;
			
			if ( !isGradient )
			{
				art.graphics.beginFill( fill[ 0 ] , fillAlpha[ 0 ] );
			}
			else
			{
				art.graphics.beginGradientFill( linGrad , fill , fillAlpha , fillRatios , fillMatrix );
			}
			
			art.graphics.lineStyle( strokeWidth , stroke , strokeAlpha , true , LineScaleMode.NONE );
			art.graphics.drawRoundRectComplex( xPos , yPos , width , height , topLeftRadius , topRightRadius , bottomLeftRadius , bottomRightRadius );
			art.graphics.endFill();
			
			return art;
		}
		
		/** 
		 * drawCircle - draws a circle into the supplied clip.
		 * @return Sprite
		**/
		public static function drawCircle (  spriteRef : Sprite , xPos : int , yPos : int , radius : int , fill : Array , fillAlpha : Array , fillRatios : Array , fillMatrix : Matrix = null , stroke : uint = 0x000000 , strokeWidth : uint = 0 , strokeAlpha : Number = 0 , fillGradientType : String = "linear" ) : Sprite
		{
			var isGradient : Boolean;
			
			if ( fillMatrix == null )
			{
				isGradient = false;
			}
			else
			{
				isGradient = true;
			}
			
			var art : Sprite = spriteRef;
			var linGrad : String = ( fillGradientType.toLowerCase() != GradientType.LINEAR ) ? GradientType.RADIAL : GradientType.LINEAR;
			
			if ( !isGradient )
			{
				art.graphics.beginFill( fill[ 0 ] , fillAlpha[ 0 ] );
			}
			else
			{
				art.graphics.beginGradientFill( linGrad , fill , fillAlpha , fillRatios , fillMatrix );
			}
			
			art.graphics.lineStyle( strokeWidth , stroke , strokeAlpha , true , LineScaleMode.NONE );
			art.graphics.drawCircle( xPos , yPos , radius );

			art.graphics.endFill();
			
			return art;
		}
	}
}
