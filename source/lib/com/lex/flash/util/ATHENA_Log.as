package com.lex.flash.util {
	import org.osflash.thunderbolt.Logger;
	
	/**
	 * The Log class abstracts logging calls.  Using this class, you can output all API/player logging info
	 * via your preferred logging methods.<br/>
	 * All defaults are set to use Logger (org.osflash.thunderbolt.Logger) methods
	 **/
	public class ATHENA_Log
	{
		public static var errorMethod:Function = Logger.error;
		public static var infoMethod:Function = Logger.info;
		public static var warnMethod:Function = Logger.warn;
		
		public static var errorPrefix:String = "";
		public static var infoPrefix:String = "";
		public static var warnPrefix:String = "";
		
		/**
		 * Show an informational log message
		 **/
		public static function i( ... rest ) : void
		{
			var text : String = "";
			for(var i : uint = 0; i < rest.length; i++ )
			{
				text += rest[ i ] + " ";
			}
			infoMethod( infoPrefix + text );
		}
		
		/**
		 * Show an error log message
		 **/
		public static function e( ... rest ) : void
		{
			var text : String = "";
			for(var i : uint = 0; i < rest.length; i++ )
			{
				text += rest[ i ] + " ";
			}
			errorMethod( errorPrefix + text );
		}
		
		/**
		 * Show a warning log message
		 **/
		public static function w( ... rest ) : void
		{
			var text : String = "";
			for(var i : uint = 0; i < rest.length; i++ )
			{
				text += rest[ i ] + " ";
			}
			warnMethod( warnPrefix + text );
		}
		
		
	}
}