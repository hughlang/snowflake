package com.lex.flash.util {

	/**
	 * @author hugh
	 */
	public class ATHENA_FormatHMS 
	{
		
		public static function toString( seconds:Number ) : String
		{
			var h : Number = Math.floor( seconds / 3600) ;
			h = isNaN( h ) ? 0 : h;
			
			var m : Number = Math.floor( ( seconds % 3600 ) / 60) ;
			m = isNaN( m ) ? 0 : m;
			
			var s : Number = Math.floor( (seconds % 3600 ) % 60 );
			s = isNaN( s ) ? 0 : s;
			
			return (h == 0 ? "":(h<10 ? "0"+h.toString()+":" : h.toString()+":"))+(m<10 ? "0"+m.toString() : m.toString())+":"+(s<10 ? "0"+s.toString() : s.toString());
		}
	}
}
