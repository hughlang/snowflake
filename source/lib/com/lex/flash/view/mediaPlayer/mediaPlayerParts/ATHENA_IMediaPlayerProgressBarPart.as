package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerProgressBarPart extends ATHENA_IMediaPlayerPart 
	{
		function updateProgressBar( time:Number, duration:Number ) : void;
	}
}
