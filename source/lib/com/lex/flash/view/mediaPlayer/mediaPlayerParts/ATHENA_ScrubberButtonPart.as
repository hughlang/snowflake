package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Rectangle;

	/**
	 * @author hugh
	 */
	public class ATHENA_ScrubberButtonPart implements ATHENA_IMediaPlayerScrubberButtonPart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		
		protected var _progressBarPart : ATHENA_IMediaPlayerProgressBarPart;
		
		protected var _bounds : Rectangle;

		protected var _dragging : Boolean;
		
		
		public function ATHENA_ScrubberButtonPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper, bounds:Rectangle, progressBarPart:ATHENA_IMediaPlayerProgressBarPart=null )
		{
			_mc = mc;
			_player = player;
			_bounds = bounds;
			_progressBarPart = progressBarPart;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
			if (_mc.stage.hasEventListener(MouseEvent.MOUSE_UP)) _mc.stage.removeEventListener(MouseEvent.MOUSE_UP, stopScrub);
			
			_mc.removeEventListener(MouseEvent.MOUSE_DOWN, startScrub);
			_mc.removeEventListener(MouseEvent.MOUSE_UP, stopScrub);
		}

		
		public function updateScrubberPosition( time:Number, duration:Number ) :void 
		{	
			if ( !_dragging ) {
				var position:Number = ( time / duration) * _bounds.width;
				_mc.x = position;
			}
		}
		
		
		protected function init() : void
		{			
			_dragging = false;
			
			_mc.buttonMode = true;
			_mc.addEventListener(MouseEvent.MOUSE_DOWN, startScrub);
			_mc.addEventListener(MouseEvent.MOUSE_UP, stopScrub);
		}
		
		
		protected function startScrub( e:MouseEvent ) :void 
		{				
			_mc.startDrag(false, _bounds );	
			_dragging = true;
			_mc.stage.addEventListener(MouseEvent.MOUSE_UP, stopScrub);
			_mc.addEventListener(Event.ENTER_FRAME, onUpdateScrubber);
		}
		
		
		protected function stopScrub( e:MouseEvent ) :void 
		{
			_mc.stopDrag();
			_dragging = false;
			_mc.stage.removeEventListener(MouseEvent.MOUSE_UP, stopScrub);
			_mc.removeEventListener(Event.ENTER_FRAME, onUpdateScrubber);
		}
		
		
		protected function onUpdateScrubber( e:Event ) :void 
		{
			if ( _dragging ) {
				var seektime:Number = (_mc.x / _bounds.width) * _player.duration;
				_player.seek( seektime );
				if ( _progressBarPart ) _progressBarPart.updateProgressBar( _player.playhead, _player.duration );
			}
		}

		
	}
}
