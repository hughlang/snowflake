package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_MediaPlayerEvent;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;
	import com.greensock.TweenLite;
	import com.greensock.easing.Quad;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_VolumeScrubberButtonPart implements ATHENA_IMediaPlayerPart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		protected var _mutePart : ATHENA_IMediaPlayerMuteButtonPart;

		protected var _volumeOverlay:MovieClip;
		protected var _volumeMask:MovieClip;

		protected var _volumeLevel:Number;
		protected var _volDragging:Boolean;
		
		
		protected var _ec : ATHENA_EventController;
		
		protected const DELAY : Number = 0.5;
		protected const DURATION : Number = 0.25;
		private const EASING : Function = Quad.easeInOut;
		
		
		public function ATHENA_VolumeScrubberButtonPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper, mutePart:ATHENA_IMediaPlayerMuteButtonPart=null )
		{
			_mc = mc;
			_player = player;
			_mutePart = mutePart;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
			if (_mc.stage.hasEventListener(MouseEvent.MOUSE_UP)) _mc.stage.removeEventListener(MouseEvent.MOUSE_UP, onStopChangeVol);
			
			_volumeOverlay.removeEventListener(MouseEvent.MOUSE_DOWN, onChangeVol);
			_volumeOverlay.removeEventListener(MouseEvent.MOUSE_UP, onStopChangeVol);
			
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SHOW, onFadeUpVolume );
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_HIDE, onFadeDownVolume );
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SET, onSetVolume );
			_ec = null;
		}

		
		protected function init() : void
		{
			_ec = ATHENA_EventController.getInstance();
			_ec.addEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SHOW, onFadeUpVolume );
			_ec.addEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_HIDE, onFadeDownVolume );
			_ec.addEventListener( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SET, onSetVolume );
			
			_volumeOverlay = _mc.volumeOverlay;
			_volumeMask = _mc.volumeMask;
			_volumeOverlay.buttonMode = true;	
			_volumeOverlay.addEventListener(MouseEvent.MOUSE_DOWN, onChangeVol);
			_volumeOverlay.addEventListener(MouseEvent.MOUSE_UP, onStopChangeVol);
			_volumeOverlay.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			_volumeOverlay.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			
			_volDragging = false;
			
			_mc.alpha = 0;
			hideVolumeScrubber();
		}
		
		protected function setVolumeLevel() : void
		{
		}
		
		
		protected function onChangeVol(e:MouseEvent) :void 
		{
			_volDragging = true;
			_mc.stage.addEventListener(MouseEvent.MOUSE_UP, onStopChangeVol);
			if ( _volumeOverlay ) _volumeOverlay.addEventListener(Event.ENTER_FRAME, updateVolume);
		}
		
		
		protected function onStopChangeVol(e:MouseEvent) :void 
		{
			_volDragging = false;
			_mc.stage.removeEventListener(MouseEvent.MOUSE_UP, onStopChangeVol);
			if ( _volumeOverlay ) _volumeOverlay.removeEventListener(Event.ENTER_FRAME, updateVolume);
			fadeVolumeScrubber( false );
		}
		
		
		protected function updateVolume(e:Event) :void 
		{
			if (_volDragging) {
				var level:Number = Number(e.target.mouseY);
				setVolume( Math.max( 0, Math.min( 1 - level/_volumeOverlay.height, 1) ) );
					
				if ( _mutePart ) {
					if ( _mutePart.muted && _volumeLevel > 0 ) _mutePart.setMuteButton( false );
				}
			}
		}
		
		
		protected function setVolume( vol:Number ) : void
		{
			_volumeLevel = vol;
			_player.volume = _volumeLevel;
			
			_volumeMask.y = _volumeOverlay.y + _volumeOverlay.height * (1 - vol );
		}
		
		
		protected function onSetVolume( e:ATHENA_MediaPlayerEvent ) : void
		{
			var vol : Number = Number( e.data );
			setVolume( vol );
		}
		
		
		protected function onFadeUpVolume( e:ATHENA_MediaPlayerEvent ) : void
		{
			fadeVolumeScrubber( true );
		}
		
		
		protected function onRollOver( e:MouseEvent ) : void
		{
			TweenLite.killDelayedCallsTo( hideVolumeScrubber );
			TweenLite.killTweensOf( _mc );
		}
		
		
		protected function onRollOut( e:MouseEvent ) : void
		{
			if( _volDragging ) return;
			fadeVolumeScrubber( false );
		}
		
		
		protected function onFadeDownVolume( e:ATHENA_MediaPlayerEvent ) : void
		{
			fadeVolumeScrubber( false );
		}
		
		
		protected function fadeVolumeScrubber( show:Boolean ) : void
		{
			if(_volDragging) return;
			
			var tweenParams:Object;
			
			_mc.visible = true;
			
			if ( show ) {
				tweenParams = {
								alpha : 1,
								delay : DELAY,
								ease:EASING
								};
			} else {
				tweenParams = {
								alpha : 0,
								delay : DELAY,
								ease:EASING,
								onComplete: hideVolumeScrubber
								};
			}
			
			TweenLite.to( _mc, DURATION, tweenParams );
			
		}
		
		
		protected function hideVolumeScrubber() : void
		{
			_mc.visible = false;
		}
	}
}
