package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_PlayPauseButtonPart extends ATHENA_ButtonBase implements ATHENA_IMediaPlayerPlayPauseButtonPart 
	{
		protected var _player : ATHENA_MediaPlayerWrapper;
		protected var _bg : MovieClip;
		
		

		public function ATHENA_PlayPauseButtonPart( mc:MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			super(mc);
			_player = player;
			init();
		}
		
		
		override public function get name() : String
		{
			return _mc.name;
		}
		
		
		public function pause() : void 
		{
			setPlayButton( true );
			_player.pause();	
		}


		public function play() : void 
		{
			setPlayButton( false );
			_player.play();	
		}
				
				
		public function setPlayButton( enable:Boolean ) : void
		{
			if ( enable ) {
				_mc.gotoAndStop("play");
			} else {
				_mc.gotoAndStop("pause");
			}
		}
		
		
		override protected function onClick( e:MouseEvent ) : void
		{
			if ( _player.playing ) {
				pause();
			} else {
				play();
			}
		}
		
		
		override protected function onRollOver(e:MouseEvent) :void
		{
			_bg.gotoAndStop("on");
		}
		
		
		override protected function onRollOut(e:MouseEvent) :void
		{
			_bg.gotoAndStop("on");
		}
		

		protected function init() : void
		{
			_bg = MovieClip( _mc.getChildByName( "bg" ) );
			
			setPlayButton( false );
		}
		
		
		
 	}
}
