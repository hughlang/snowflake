package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerBefferingAnimationPart extends ATHENA_IMediaPlayerPart 
	{
		function showBufferingAnimation( enable:Boolean ) : void;
	}
}
