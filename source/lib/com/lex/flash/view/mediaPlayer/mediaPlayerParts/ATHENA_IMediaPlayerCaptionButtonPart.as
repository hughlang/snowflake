package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerCaptionButtonPart extends ATHENA_IMediaPlayerPart 
	{
		function get captionEnabled() : Boolean;
		function setCaptionButton( enable:Boolean ) : void;
	}
}
