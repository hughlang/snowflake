package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_RewindButtonPart extends ATHENA_ButtonBase implements ATHENA_IMediaPlayerPart 
	{
		
		protected var _player : ATHENA_MediaPlayerWrapper;
		

		public function ATHENA_RewindButtonPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			super(mc);
			_player = player;
			init();
		}
		
		
		override public function get name() : String
		{
			return _mc.name;
		}
				
		
		override protected function onClick( e:MouseEvent ) : void
		{
			_player.seek( 0 );
		}
		
		
		protected function init() : void
		{
		}
		
		
		
		
	}
}
