package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerBufferBarPart extends ATHENA_IMediaPlayerPart 
	{
		function updateBufferBar(bytesLoaded:Number, bytesTotal:Number) : void;
	}
}
