package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerMuteButtonPart extends ATHENA_IMediaPlayerPart 
	{
		function get muted() : Boolean;
		function setMuteButton( mute:Boolean ) : void;
	}
}
