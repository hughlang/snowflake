package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.ATHENA_IView;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerPart extends ATHENA_IView 
	{
		function get name() : String;
	}
}
