package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerScrubberButtonPart extends ATHENA_IMediaPlayerPart 
	{
		function updateScrubberPosition( time:Number, duration:Number ) :void;
	}
}
