package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerPlayPauseButtonPart extends ATHENA_IMediaPlayerPart 
	{
		function pause() : void;
		function play() : void;
		function setPlayButton( enable:Boolean ) : void;
	}
}
