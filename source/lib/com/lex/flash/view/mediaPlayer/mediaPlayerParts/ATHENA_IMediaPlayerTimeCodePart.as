package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.mediaPlayer.mediaPlayerParts.ATHENA_IMediaPlayerPart;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_IMediaPlayerTimeCodePart extends ATHENA_IMediaPlayerPart 
	{
		function updateTimeCode(time:Number, duration:Number) :void;
	}
}
