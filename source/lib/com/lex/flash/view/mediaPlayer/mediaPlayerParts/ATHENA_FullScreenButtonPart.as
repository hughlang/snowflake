package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_FullScreenButtonPart extends ATHENA_ButtonBase implements ATHENA_IMediaPlayerPart 
	{
		protected var _player : ATHENA_MediaPlayerWrapper;

		public function ATHENA_FullScreenButtonPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			super(mc);
			_player = player;
			init();
		}
		
		
		override public function get name() : String
		{
			return _mc.name;
		}
				
		
		override protected function onClick( e:MouseEvent ) : void
		{
			if (_mc.stage) {
				if ( _mc.stage.displayState == StageDisplayState.NORMAL ) {
					//_mc.stage.fullScreenSourceRect = _player.getRect(_mc.stage);
				}
				
				_mc.stage.displayState = ( _mc.stage.displayState == StageDisplayState.FULL_SCREEN  ) ? StageDisplayState.NORMAL : StageDisplayState.FULL_SCREEN;
			}
		}
		
		
		protected function init() : void
		{
		}
		
		
		
		
	}
}
