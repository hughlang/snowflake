package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_ButtonBase implements ATHENA_IMediaPlayerPart
	{
		protected var _mc : MovieClip;
		
		public function ATHENA_ButtonBase( mc:MovieClip ) 
		{
			_mc = mc;
			init();
		}


		public function dispose() : void
		{
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
		
		
		protected function disposeBase() : void
		{
			_mc.removeEventListener(MouseEvent.ROLL_OVER, onRollOver);
			_mc.removeEventListener(MouseEvent.ROLL_OUT, onRollOut);
			_mc.removeEventListener(MouseEvent.CLICK, onClick);
		}
		
		
		private function init() : void
		{
			_mc.buttonMode = true;
			_mc.mouseChildren = false;
			_mc.addEventListener(MouseEvent.ROLL_OVER, onRollOver);
			_mc.addEventListener(MouseEvent.ROLL_OUT, onRollOut);
			_mc.addEventListener(MouseEvent.CLICK, onClick);
		}
		
		
		protected function onRollOver(e:MouseEvent) :void
		{
			var btn:MovieClip = e.target as MovieClip;
			btn.gotoAndStop("on");
		}
		
		
		protected function onRollOut(e:MouseEvent) :void
		{
			var btn:MovieClip = e.target as MovieClip;
			btn.gotoAndStop("off");
		}
		

		protected function onClick(e:MouseEvent) :void
		{
			
		}
		

		
	}
}
