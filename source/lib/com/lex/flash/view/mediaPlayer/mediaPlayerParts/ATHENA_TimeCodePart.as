package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.model.ATHENA_DataModel;
	import com.lex.flash.util.ATHENA_FormatHMS;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.util.ATHENA_TextManager;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.text.AntiAliasType;
	import flash.text.TextField;

	/**
	 * @author hugh
	 */
	public class ATHENA_TimeCodePart implements ATHENA_IMediaPlayerTimeCodePart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		
		protected var _duration : TextField;
		protected var _currentTime : TextField;
		
		protected var _tm : ATHENA_TextManager;
		
		protected const TIME_CODE_STYLE : String = "media_player_time_code";
		
		
		public function ATHENA_TimeCodePart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			_mc = mc;
			_player = player;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
			_tm = null;
		}

		
		public function updateTimeCode(time:Number, duration:Number) :void
		{
			_duration.htmlText = _tm.styleText( ATHENA_FormatHMS.toString( duration ), TIME_CODE_STYLE );
			_currentTime.htmlText = _tm.styleText( ATHENA_FormatHMS.toString( time ), TIME_CODE_STYLE );
		}
			
				
		protected function init() : void
		{
			_duration = TextField( _mc["duration"] );
			_currentTime = TextField( _mc["currentTime"] );
			
			_tm = ATHENA_TextManager.getInstance();
			
			_duration.antiAliasType = AntiAliasType.ADVANCED;
			_duration = _tm.setStyleSheet( _duration, ATHENA_DataModel.getInstance().stylesheet );
			
			_currentTime.antiAliasType = AntiAliasType.ADVANCED;
			_currentTime = _tm.setStyleSheet( _currentTime, ATHENA_DataModel.getInstance().stylesheet );
			
			updateTimeCode( _player.playhead, _player.duration );
			
		}
		
		
	}
}
