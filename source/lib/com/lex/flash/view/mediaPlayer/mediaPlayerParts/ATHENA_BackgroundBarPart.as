package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_FormatHMS;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.util.ATHENA_ToolTip;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;

	/**
	 * @author hugh
	 */
	public class ATHENA_BackgroundBarPart implements ATHENA_IMediaPlayerPart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		
		protected var _bounds : Rectangle;

		protected var _toolTip : ATHENA_ToolTip;

		protected const DEFAULT_TIME : String = "00:00";
		protected const TOOLTIP_STYLE : String = "captions_tool_tip";
		protected const POS_Y : Number = -30;
		
		
		public function ATHENA_BackgroundBarPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			_mc = mc;
			_player = player;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
			_mc.removeEventListener( MouseEvent.CLICK, onBackgroundBarClick );
			_mc.removeEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_mc.removeEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_mc.removeEventListener( Event.ENTER_FRAME, onEnterFrame );
			
			_mc.removeChild( _toolTip );
			_toolTip = null;
		}

		
		protected function init() : void
		{
			_bounds = new Rectangle( 0, 0, _mc.width, 0);
			
			_mc.addEventListener( MouseEvent.CLICK, onBackgroundBarClick );
			_mc.addEventListener( MouseEvent.ROLL_OVER, onRollOver );
			_mc.addEventListener( MouseEvent.ROLL_OUT, onRollOut );
			_mc.addEventListener( Event.ENTER_FRAME, onEnterFrame );


			_toolTip = new ATHENA_ToolTip( TOOLTIP_STYLE );
			_toolTip.initialize(0, 1, 5, 10, 4, 8);
			_toolTip.setText(DEFAULT_TIME, -1, -1);
			_toolTip.x = 0;
			_toolTip.y = - _toolTip.height;
			_mc.addChild( _toolTip );
			
			_toolTip.visible = false;
		}
		
		
		protected function onBackgroundBarClick(e:MouseEvent) : void 
		{
			var seektime:Number = ( e.localX / _bounds.width) * _player.duration;
			_player.seek( seektime );
		}


		protected function onRollOver( e:MouseEvent ) : void
		{
			_toolTip.visible = true;
			updateTooltip();
		}
		
		
		protected function onRollOut( e:MouseEvent ) : void
		{
			_toolTip.visible = false;
		}
		
		
		protected function onEnterFrame( e:Event ) : void
		{
			if ( _toolTip.visible ) updateTooltip();
		}
		
		
		protected function updateTooltip() : void
		{
			var pt : Point = _mc.globalToLocal( new Point( _mc.stage.mouseX, _mc.stage.mouseY) );
			_toolTip.x = pt.x - _toolTip.width/2;
			_toolTip.setText( ATHENA_FormatHMS.toString( ( pt.x / _bounds.width) * _player.duration ), -1, -1);
		}
		
	}
}
