package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;

	/**
	 * @author hugh
	 */
	public class ATHENA_BufferingAnimationPart implements ATHENA_IMediaPlayerBefferingAnimationPart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		

		
		
		public function ATHENA_BufferingAnimationPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			_mc = mc;
			_player = player;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
		}

		
		public function showBufferingAnimation( enable:Boolean ) : void
		{
			_mc.visible = enable;
		}
		
		
		protected function init() : void
		{
			_mc.visible = false;
			_mc.mouseEnabled = false;
		}
		
		

		
	}
}
