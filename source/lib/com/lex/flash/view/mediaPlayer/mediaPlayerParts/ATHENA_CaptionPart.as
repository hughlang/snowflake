package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_MediaPlayerEvent;
	import com.lex.flash.model.ATHENA_DataModel;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.util.ATHENA_TextManager;
	import com.lex.flash.view.ATHENA_IView;
	import com.greensock.TweenLite;

	import org.osmf.captioning.model.Caption;
	import org.osmf.events.SeekEvent;

	import flash.display.MovieClip;
	import flash.text.AntiAliasType;
	import flash.text.GridFitType;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;

	/**
	 * @author hugh
	 */
	public class ATHENA_CaptionPart implements ATHENA_IMediaPlayerPart
	{

		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		
		protected var _ec : ATHENA_EventController;
		protected var _tm : ATHENA_TextManager;

		protected var _caption : Caption;
		protected var _text : TextField;
		protected var _captionEnabled : Boolean;

		
		protected const CAPTION_STYLE : String = "captions_copy";
		protected const DEFAULT_TEXT : String = "";
		protected const FADE_DURATION : Number = 0.4;
		protected const TEXT_FIELD_X : Number = 4;
		
		
		public function ATHENA_CaptionPart( mc:MovieClip, player:ATHENA_MediaPlayerWrapper, captionEnabled:Boolean=false )
		{
			_mc = mc;
			_player = player;
			_captionEnabled = captionEnabled;
			
			init();
		}
		
		
		public function dispose() : void
		{
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.CAPTION_ENABLED , onEnableCaption );
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.CAPTION_DISABLED , onDisableCaption );
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.CAPTION_SHOW , onShowCaption );
			_ec.removeEventListener( ATHENA_MediaPlayerEvent.CAPTION_HIDE , onHideCaption );
			
			_player.removeEventListener( SeekEvent.SEEKING_CHANGE , onSeekingChange );
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
		
		
		protected function init() : void
		{
			_ec = ATHENA_EventController.getInstance();
			_tm = ATHENA_TextManager.getInstance();
						
			createTextArea ();
			
			_ec.addEventListener( ATHENA_MediaPlayerEvent.CAPTION_ENABLED , onEnableCaption );
			_ec.addEventListener( ATHENA_MediaPlayerEvent.CAPTION_DISABLED , onDisableCaption );
			_ec.addEventListener( ATHENA_MediaPlayerEvent.CAPTION_SHOW , onShowCaption );
			_ec.addEventListener( ATHENA_MediaPlayerEvent.CAPTION_HIDE , onHideCaption );
			
			_player.addEventListener( SeekEvent.SEEKING_CHANGE , onSeekingChange );
			
			
			_mc.alpha = 0;
			
		}
		
		protected function createTextArea () : void
		{
			_text = new TextField ();
			_text.mouseWheelEnabled = false;
			_text.name = "tfCaption";
			_text.width = _mc.width - (3 * TEXT_FIELD_X);
			_text.x = TEXT_FIELD_X;
			_text.multiline = true;
			_text.wordWrap = true;
			_text.selectable = false;
			_text.autoSize = TextFieldAutoSize.LEFT;
			_text.antiAliasType = AntiAliasType.ADVANCED;
			_text.gridFitType = GridFitType.PIXEL;
			
			_text = _tm.setStyleSheet( _text, ATHENA_DataModel.getInstance().stylesheet );
									
			_mc.addChild( _text );
		}
		
		
		protected function onSeekingChange( e : SeekEvent ) : void
		{
			if ( e.seeking )
			{
				fadeCaptionTo( 0 );
				clearCaptionText();
			}
		}
		
		
		protected function onEnableCaption( e : ATHENA_MediaPlayerEvent ) : void
		{
			_captionEnabled = true;
		}
		
		
		protected function onDisableCaption( e : ATHENA_MediaPlayerEvent ) : void
		{
			_captionEnabled = false;
			
			fadeCaptionTo( 0 );
		}
		
		
		protected function onShowCaption( e : ATHENA_MediaPlayerEvent ) : void
		{
			if ( !_captionEnabled ) return;
			
			_caption = e.data as Caption;
			
			_text.htmlText = _tm.styleText( _caption.text, CAPTION_STYLE);
			
			_text.y = ( _mc.height / 2 ) - ( _text.textHeight / 2 );
			
			fadeCaptionTo( 1 );
		}
		
		
		protected function onHideCaption( e : ATHENA_MediaPlayerEvent ) : void
		{
			if ( !_captionEnabled ) return;
			
			var caption : Caption = e.data as Caption;

			if ( _caption && _caption.time == caption.time )
			{
				clearCaptionText();	
							
				fadeCaptionTo( 0 );
			}
		}
		
		
		protected function clearCaptionText () : void
		{
			_text.htmlText = "";
		}
		
		
		protected function fadeCaptionTo( opacity:Number ) : void
		{
			TweenLite.killTweensOf( this );
			TweenLite.to( _mc, FADE_DURATION, {alpha: opacity, delay: 0});
		}
		
	}
}
