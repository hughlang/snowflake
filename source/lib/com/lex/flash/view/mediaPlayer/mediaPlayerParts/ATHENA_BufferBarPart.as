package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.geom.Rectangle;

	/**
	 * @author hugh
	 */
	public class ATHENA_BufferBarPart implements ATHENA_IMediaPlayerBufferBarPart 
	{
		protected var _mc : MovieClip;
		protected var _player : ATHENA_MediaPlayerWrapper;
		
		protected var _bounds : Rectangle;

		
		
		public function ATHENA_BufferBarPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			_mc = mc;
			_player = player;
			init();
		}
		
		
		public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function dispose() : void
		{
		}

		
		public function updateBufferBar(bytesLoaded:Number, bytesTotal:Number) : void
		{
			_mc.width = (bytesLoaded/bytesTotal) * _bounds.width;
		}
		
		
		protected function init() : void
		{
			_bounds = new Rectangle( 0, 0, _mc.width, 0);
			
			_mc.mouseEnabled = false;
			
			_mc.width = 0;
			
		}
		
		

		
	}
}
