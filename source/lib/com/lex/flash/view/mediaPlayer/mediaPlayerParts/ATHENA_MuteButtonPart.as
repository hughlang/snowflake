package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_MediaPlayerEvent;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_MuteButtonPart extends ATHENA_ButtonBase implements ATHENA_IMediaPlayerMuteButtonPart 
	{
		
		protected var _player : ATHENA_MediaPlayerWrapper;
		protected var _isMuted : Boolean;
		protected var _unMuteVolumeLevel : Number;
		

		public function ATHENA_MuteButtonPart( mc:MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			super(mc);
			_player = player;
			init();
		}
		
		
		override public function get name() : String
		{
			return _mc.name;
		}
				
		
		public function get muted() : Boolean
		{
			return _isMuted;
		}
		
		public function setMuteButton( mute:Boolean ) : void
		{
			_isMuted = mute;
			setMuteVolume();
			
			if ( mute ) {
				_mc.gotoAndStop("mute");
			} else {
				_mc.gotoAndStop("unmute");
			}
		}
		
		
		override protected function onClick( e:MouseEvent ) : void
		{
			setMuteButton( !_isMuted );
		}
		
		
		override protected function onRollOver(e:MouseEvent) :void
		{
			ATHENA_EventController.getInstance().dispatchEvent( new ATHENA_MediaPlayerEvent( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SHOW ) );
		}
		
		
		override protected function onRollOut(e:MouseEvent) :void
		{
			ATHENA_EventController.getInstance().dispatchEvent( new ATHENA_MediaPlayerEvent( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_HIDE ) );
		}
		

		protected function init() : void
		{
			_isMuted = false;
		}
		
		
		protected function setMuteVolume() : void
		{
			if ( _isMuted ) { 
				
				_unMuteVolumeLevel = _player.volume;
				_player.volume = 0;
				
			} else {
				
				_player.volume = _unMuteVolumeLevel;
			}
			
			ATHENA_EventController.getInstance().dispatchEvent( new ATHENA_MediaPlayerEvent( ATHENA_MediaPlayerEvent.VOLUME_SLIDER_SET, _player.volume ) );
		}
		
		
	}
}
