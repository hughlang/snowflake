package com.lex.flash.view.mediaPlayer.mediaPlayerParts {
	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_MediaPlayerEvent;
	import com.lex.flash.util.ATHENA_MediaPlayerWrapper;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_CaptionButtonPart extends ATHENA_ButtonBase implements ATHENA_IMediaPlayerCaptionButtonPart 
	{
		
		protected var _player : ATHENA_MediaPlayerWrapper;
		protected var _captionEnabled : Boolean;
		protected var _ccOverState : MovieClip;
		
		protected var _ec : ATHENA_EventController;
		

		public function ATHENA_CaptionButtonPart( mc : MovieClip, player:ATHENA_MediaPlayerWrapper )
		{
			super(mc);
			_player = player;
			init();
		}
		
		
		override public function get name() : String
		{
			return _mc.name;
		}
		
		
		override public function dispose() : void
		{
			disposeBase();
			
			_ec = null;		
		}
		
		
		public function get captionEnabled() : Boolean
		{
			return _captionEnabled;
		}
		
		
		public function setCaptionButton( enable:Boolean ) : void
		{
			_captionEnabled = enable;
			enableCaption();
			
			if ( enable ) {
				_mc.gotoAndStop("on");
			} else {
				_mc.gotoAndStop("off");
			}
		}
		
		
		override protected function onClick( e:MouseEvent ) : void
		{
			setCaptionButton( !_captionEnabled );
		}
		
		
		override protected function onRollOver(e:MouseEvent) :void
		{
			_ccOverState.visible = true;
		}
		
		
		override protected function onRollOut(e:MouseEvent) :void
		{
			_ccOverState.visible = false;
		}
		

		protected function init() : void
		{
			_captionEnabled = false;
			
			_ec = ATHENA_EventController.getInstance();
			
			_ccOverState = MovieClip( _mc.getChildByName( "ccOverState" ) );
			_ccOverState.visible = false;
		}
		
		
		protected function enableCaption() : void
		{
			if ( _captionEnabled ) { 
				_ec.dispatchEvent( new ATHENA_MediaPlayerEvent( ATHENA_MediaPlayerEvent.CAPTION_ENABLED ) );
			} else {
				_ec.dispatchEvent( new ATHENA_MediaPlayerEvent( ATHENA_MediaPlayerEvent.CAPTION_DISABLED ) );
			}
			
		}
		
		
	}
}
