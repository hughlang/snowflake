package com.lex.flash.view.nav {

	import view.nav.NavButton;

	import com.curiousmedia.accessibility.TabIndexManager;
	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_NavEvent;
	import com.lex.flash.events.ATHENA_ViewEvent;
	import com.lex.flash.model.ATHENA_DataModel;
	import com.lex.flash.model.info.nav.ATHENA_NavItemInfo;
	import com.lex.flash.util.ATHENA_Log;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.system.ApplicationDomain;

	/**
	 * @author hugh
	 */
	public class ATHENA_NavBase implements ATHENA_INav
	{

		protected var _mc : MovieClip;
		protected var _ec : ATHENA_EventController;
		protected var _tim : TabIndexManager;
		
		private var _dm : ATHENA_DataModel;
		
		protected var _navItems : Vector.<ATHENA_NavItemInfo>;
		protected var _buttons : Vector.<NavButton>;
		
		protected var _selectedItem : ATHENA_NavItemInfo;
		
		protected var _tabIndex : int;
		
		protected const BUTTON_LINKAGE : String = "NavButtonMC";
		
		
		
		/**
		 * Nav
		 * 
		 * @param mc:MovieClip
		 * @return nothing
		 */
		public function ATHENA_NavBase( mc : MovieClip )
		{
			_mc = mc;
			
			initBase();
		}
		
		
		/**
		 * destructor
		 * 
		 * @return nothing
		 */
		public function dispose() : void
		{
			disposeBase();
		}



		/**
		 * resetNav
		 * 
		 * @return nothing
		 */
		public function resetNav() : void
		{
			_ec.dispatchEvent( new ATHENA_ViewEvent( ATHENA_ViewEvent.NAV_ITEM_RESET ) );
		}
		
		
		/**
		 * baseDestructor
		 * 
		 * @return nothing
		 */
		protected function disposeBase() : void
		{
			disposeBaseButton();
			
			_ec.removeEventListener( ATHENA_ViewEvent.NAV_ITEM_SELECTED, onNavItemSelected );
			_ec.removeEventListener( ATHENA_ViewEvent.SELECT_NAV_ITEM, onSelectNavItem );
			_ec = null;
			_tim = null;
			_dm = null;
		}
		
		
		/**
		 * destructBaseButton
		 * 
		 * @return nothing
		 */
		protected function disposeBaseButton() : void
		{
			for (var i : int = 0; i < _buttons.length; i++) {
				_buttons[i].dispose();
				_buttons[i] = null;
			}
			
			_buttons = null;
			_navItems = null;
		}
		
		

		/**
		 * initBase
		 * 
		 * @return nothing
		 */
		protected function initBase() : void
		{
			//get instance of data model
			_dm = ATHENA_DataModel.getInstance();
			
			//get instance of the tab manager
			_tim = TabIndexManager.instance;
			_tabIndex = 0;
			
			
			//set nav items
			//NOTE: the first element DataModel.nav.navItems[0] is the link to the home page
			//DO NOT INCLUDE IT in the _navItems
			_navItems = new Vector.<ATHENA_NavItemInfo>();
			var items : Vector.<ATHENA_NavItemInfo> = _dm.nav.navItems;
			for (var i : int = 1; i < items.length; i++) {
				_navItems.push( items[i] );
			}
			setNavButtons();
			//positionNavButtons();
			//update start tab order for the rest of the app
			_dm.startTabIndex = _tabIndex;
			
			
			//setup listener
			_ec = ATHENA_EventController.getInstance();
			_ec.addEventListener( ATHENA_ViewEvent.NAV_ITEM_SELECTED, onNavItemSelected );
			_ec.addEventListener( ATHENA_ViewEvent.SELECT_NAV_ITEM, onSelectNavItem );
		}
		
		
		/**
		 * setNavButtons
		 * 
		 * @return nothing
		 */
		protected function setNavButtons() : void
		{
			var mc : MovieClip;
			var navButton : NavButton;
			var selected : Boolean = false;
			
			_buttons = new Vector.<NavButton>();
			
			for (var i : int = 0; i < _navItems.length; i++) {

				try {
					//var AssetClass:Class = ApplicationDomain.currentDomain.getDefinition( BUTTON_LINKAGE ) as Class;
					//mc = new AssetClass();
					mc = _dm.getMovieClipAsset( BUTTON_LINKAGE );
				} catch ( e:Error ) {
					ATHENA_Log.e( "Nav::setNavButtons - " + e.message );
				}
				
				navButton = new NavButton( mc, _navItems[i], selected );
				
				mc.name = _navItems[i].id;
				
				//set keyboard control
				_tim.setTab( mc, ++_tabIndex );
				
				_buttons.push( navButton );
			}
			
		}

		
		/**
		 * positionNavButtons
		 * 
		 * @return nothing
		 */
		protected function positionNavButtons() : void
		{
		}
		
		
		/**
		 * lookupNavItemIndexById
		 * 
		 * @param id
		 * @return int
		 */
		protected function lookupNavItemIndexById( id:String ) : int
		{
			for (var i : int = 0; i < _navItems.length; i++) {
				if ( id == _navItems[i].id ) {
					return i;
				}
			}
			
			return 0;
		}
		
		
		
		/**
		 * onNavItemSelected
		 * 
		 * @param e
		 * @return nothing
		 */
		protected function onNavItemSelected( e : ATHENA_ViewEvent ) : void
		{
			_selectedItem = ATHENA_NavItemInfo( e.data);
			
			//tell the ViewController to change site section
			_ec.dispatchEvent( new ATHENA_NavEvent( ATHENA_NavEvent.CHANGE_SITE_SECTION, _selectedItem) );
			
		}

		
		/**
		 * onSelectNavItem
		 * 
		 * @param e
		 * @return void
		 */
		protected function onSelectNavItem( e:ATHENA_ViewEvent ) : void
		{
			var navId : String = String( e.data );
			selectNavItem( navId );
		}
		
		
		/**
		 * selectNavItem
		 * 
		 * @param id
		 * @return void
		 */
		protected function selectNavItem( id:String ) : void
		{
			var index : int = lookupNavItemIndexById( id );
			_selectedItem  = _navItems[ index ];
		}
		
		
		

		
		
	}
}

