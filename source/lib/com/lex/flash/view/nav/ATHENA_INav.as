package com.lex.flash.view.nav {
	import com.lex.flash.view.ATHENA_IView;
	
	/**
	 * @author hugh
	 */
	public interface ATHENA_INav extends ATHENA_IView 
	{
		function resetNav() : void;
	}
}
