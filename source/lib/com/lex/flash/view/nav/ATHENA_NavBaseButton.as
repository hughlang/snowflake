package com.lex.flash.view.nav {
	import events.ViewEvent;

	import com.lex.flash.control.ATHENA_EventController;
	import com.lex.flash.events.ATHENA_ViewEvent;
	import com.lex.flash.model.info.nav.ATHENA_NavItemInfo;
	import com.lex.flash.view.ATHENA_IView;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	/**
	 * @author hugh
	 */
	public class ATHENA_NavBaseButton implements ATHENA_IView 
	{

		protected var _mc : MovieClip;
		
		protected var _item : ATHENA_NavItemInfo;
		protected var _selected : Boolean;
		
		protected var _ec : ATHENA_EventController;


				
		public function ATHENA_NavBaseButton( mc : MovieClip, item:ATHENA_NavItemInfo, select:Boolean=false )
		{
			_mc = mc;
			_item = item;
			_selected = select;
			
			initBaseButton();
		}
		
		
		public function dispose() : void
		{
			disposeBaseButton();
		}
		
		public function get mc() : MovieClip
		{
			return _mc;
		}
		
		
		protected function disposeBaseButton() : void
		{
			setButtonListeners( false );
			
			
			_ec.removeEventListener( ATHENA_ViewEvent.NAV_ITEM_SELECTED, onNavItemSelected);
			_ec.removeEventListener( ATHENA_ViewEvent.NAV_ITEM_RESET, onNavItemReset);
			_ec = null;
			
			_mc = null;
		}
		
		
		protected function init() : void
		{
		}
		
		
		protected function initBaseButton() : void
		{
			//initialize varaibles
			_mc.mouseChildren = false;
			
			//initialize listener
			_ec = ATHENA_EventController.getInstance();
			_ec.addEventListener( ATHENA_ViewEvent.NAV_ITEM_SELECTED, onNavItemSelected);
			_ec.addEventListener( ATHENA_ViewEvent.NAV_ITEM_RESET, onNavItemReset);
			
			//set button elements visibility
			setBaseButton();
			
			//broadcast initial selection
			if ( _selected ) {
				_ec.dispatchEvent( new ATHENA_ViewEvent( ATHENA_ViewEvent.NAV_ITEM_SELECTED, _item ) );
			}
		}
		
		
		protected function setButton() : void
		{
			setBaseButton();
		}
		
		
		protected function setBaseButton() : void
		{
			if ( _selected ) {
				setButtonListeners( false );
			} else {
				setButtonListeners( true );
			}
		}
		
		
		protected function setButtonListeners( enable:Boolean ) : void
		{
			if ( enable ) {
				_mc.addEventListener( MouseEvent.CLICK, onClick);
				_mc.addEventListener( MouseEvent.ROLL_OVER, onRollOver);
				_mc.addEventListener( MouseEvent.ROLL_OUT, onRollOut);
			} else {
				if (_mc.hasEventListener( MouseEvent.CLICK) ) _mc.removeEventListener( MouseEvent.CLICK, onClick);
				if (_mc.hasEventListener( MouseEvent.ROLL_OVER) ) _mc.removeEventListener( MouseEvent.ROLL_OVER, onRollOver);
				if (_mc.hasEventListener( MouseEvent.ROLL_OUT) ) _mc.removeEventListener( MouseEvent.ROLL_OUT, onRollOut);
			}
			_mc.buttonMode = enable;
		}
		
		
		protected function onClick( event:MouseEvent ) : void
		{
			_selected = true;
			setButton();
			
			//notify the other navigation items
			_ec.dispatchEvent( new ATHENA_ViewEvent( ATHENA_ViewEvent.NAV_ITEM_SELECTED, _item ) );
			
 		}
		
		
		protected function onRollOver( event:MouseEvent ) : void
		{

		}
		
		
		protected function onRollOut( event:MouseEvent ) : void
		{

		}
		

		protected function onNavItemSelected( event:ATHENA_ViewEvent ) : void
		{
			var item : ATHENA_NavItemInfo = ATHENA_NavItemInfo( event.data );
			
			if ( item != _item ) {
				onNavItemReset();
			}
		}
		
		
		protected function onNavItemReset( event:ATHENA_ViewEvent=null ) : void
		{
			if ( _selected ) {
				_selected = false;
				setButton();
			}
		}
		
		
	}
}
