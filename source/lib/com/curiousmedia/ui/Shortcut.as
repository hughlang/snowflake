package com.curiousmedia.ui
{
	/**
	 * represents a keyboard shortcut as an object
	 * 
	 * @author Doug McCluer
	 */	
	 
	public class Shortcut extends Object
	{
		public var targetFunction:Function;
		private var _keys:Array;
		public var ctrlKey:Boolean = false;
		public var altKey:Boolean = false;
		public var shiftKey:Boolean = false;
		private var _comboString:String;
		public var label:String;
				
		/**
		 *Constructor
		 * 
		 * @param label 
		 * 	the name of this shortcut
		 * 
		 * @param targetFunction 
		 * 	the function to be called when this shorcut is invoked
		 * 
		 * @param ctrl
		 * 	indicates whether the ctrl (Cmd) key must be pressed to invoke this shorcut.
		 * 	Note: Due to inconsistent behavior across browsers, use of the ctrl key for shortcuts is discouraged.
		 * 	
		 * @param alt
		 * 	indicates whether the Alt (Option) key must be pressed to invoke this shorcut. 
		 * 
		 * @param shift
		 * 	indicates whether the Shift key must be pressed to invoke this shorcut.
		 * 
		 * @param keyCodes  
		 * 	Array of uints representing keyCodes.  The keys which must be pressed to invoke this shortcut.
		 */
		public function Shortcut(label:String, targetFunction:Function, ctrl:Boolean, alt:Boolean, shift:Boolean, keyCodes:Array=null)
		{
			_keys = new Array();
			this.targetFunction = targetFunction;
			this.label = label;
			this.ctrlKey = ctrl;
			this.altKey = alt;
			this.shiftKey = shift;
			if(keyCodes)
			{
				for(var i:int=0; i<keyCodes.length; i++)
				{
					if(keyCodes[i] is uint)
					{
						addKey(keyCodes[i]);
					}
					else
					{
						throw new Error("Error in Shortcut(): " + keyCodes[i].toString() + "is not uint.");
					}
				}
			}

			_comboString = createComboString();
		} //END Shortcut()
		
		
		/**
		 * adds another keyCode to the list of keys required to invoke this shortcut.
		 * 
		 * @param keyCode
		 * 	keyCode for the key to add
		 */
		public function addKey(keyCode:uint):void
		{
			if( keyCode == 17 )
			{
				ctrlKey = true;
			}
			else if (keyCode == 16)
			{
				shiftKey = true;
			}
			
			if(keyCode < 256)
			{
				_keys.push(keyCode);
				_keys.sort(Array.NUMERIC);
			}
			else
			{
				throw new Error("Error in "+this+".addKey():  keyCodes higher than 255 are not supported");
			}
			_comboString = createComboString();
		}
		
		
		/**
		 * allows you to check the keys required to invoke this shortcut.
		 * 
		 * @return Array of uints.  Note: this does not include keyCodes for the modifier keys (ctrl, alt, shift).
		 * 	Use the ctrlKey, altKey, and shiftKey properties to check modifier keys.   	
		 */
		public function get keys():Array
		{
			return _keys.concat();
		}
		
		
		/**
		 * @return String representation of the key combination required to invoke this shortcut.  
		 * 	Note: this is different from the toString() method.
		 */
		public function get comboString():String
		{
			return _comboString;
		}
		
		
		private function createComboString():String
		{
			var outString:String = "";
			if(ctrlKey)
			{
				if(outString.length >0)
				{
					outString += "+";
				}
				outString += "Ctrl";
			}
			else
			{
				trace("no ctrl");
			}
			
			if(altKey)
			{
				if(outString.length >0)
				{
					outString += "+";
				}
				outString += "Alt";
			}
			
			if(shiftKey)
			{
				if(outString.length >0)
				{
					outString += "+";
				}
				outString += "Shift";
			}
			
			for(var i:int=0; i<_keys.length; i++)
			{
				if(outString.length >0)
				{
					outString += "+";
				}
				switch(_keys[i])
				{
					case 8:
						outString +="Backspace";
					case 9:
						outString +="Tab";
					case 13:
						outString +="Enter";
					case 14:
						outString +="CapsLock";
					case 27:
						outString +="Esc";
					case 32:
						outString +="Space";
					case 33:
						outString +="PageUp";
					case 34:
						outString +="PageDn";
					case 35:
						outString +="End";
					case 36:
						outString +="Home";
					case 37:
						outString +="Left";
					case 38:
						outString +="Up";
					case 39:
						outString +="Right";
					case 40:
						outString +="Down";
					case 45:
						outString +="Insert";
					case 46:
						outString +="Del";
					case 144:
						outString +="NumLock";
					case 145:
						outString +="ScrLk";
					case 19:
						outString +="Pause/Break";
					default:
						outString += String.fromCharCode(_keys[i]);
				}	
			}
			trace("comboString set to " + outString);
			return outString;
		} //END createComboString()
		
		public function toString():String
		{
			var outString:String = "[shortcut " + label + "] - " + _comboString;
			return outString;
		}
		
	} //END class
}