﻿package gs.core.tween
{
    import gs.*;

    public class Tweenable extends Object
    {
        public var timeline:SimpleTimeline;
        public var initted:Boolean;
        protected var _hasUpdate:Boolean;
        public var active:Boolean;
        protected var _delay:Number;
        public var startTime:Number;
        public var nextNode:Tweenable;
        public var cachedTime:Number;
        public var gc:Boolean;
        public var cachedDuration:Number;
        public var prevNode:Tweenable;
        public var cacheIsDirty:Boolean;
        public var vars:Object;
        public var cachedTotalTime:Number;
        public var cachedPaused:Boolean;
        public var cachedTotalDuration:Number;
        public var data:Object;
        public var cachedTimeScale:Number;
        public static const version:Number = 0.1;
        static var _classInitted:Boolean;

        public function Tweenable(param1:Number = 0, param2:Object = null)
        {
            this.vars = param2 || {};
            var _loc_4:* = param1 || 0;
            this.cachedTotalDuration = param1 || 0;
            this.cachedDuration = _loc_4;
            this._delay = this.vars.delay || 0;
            this.cachedTimeScale = this.vars.timeScale || 1;
            this.active = Boolean(param1 == 0 && this._delay == 0);
            var _loc_4:* = 0;
            this.cachedTime = 0;
            this.cachedTotalTime = _loc_4;
            this.data = this.vars.data;
            if (!_classInitted)
            {
                if (isNaN(TweenLite.rootFrame))
                {
                    TweenLite.initClass();
                    _classInitted = true;
                }
                else
                {
                    return;
                }
            }
            var _loc_3:* = this.vars.timeline is SimpleTimeline ? (this.vars.timeline) : (this.vars.useFrames ? (TweenLite.rootFramesTimeline) : (TweenLite.rootTimeline));
            this.startTime = _loc_3.cachedTotalTime + this._delay;
            _loc_3.addChild(this);
            return;
        }// end function

        public function complete(param1:Boolean = false) : void
        {
            return;
        }// end function

        public function set enabled(param1:Boolean) : void
        {
            var _loc_2:* = null;
            if (param1 == this.gc)
            {
                if (param1)
                {
                    if (this is TweenLite)
                    {
                        _loc_2 = (this as TweenLite).target;
                        if (!(_loc_2 in TweenLite.masterList))
                        {
                            TweenLite.masterList[_loc_2] = [this];
                        }
                        else
                        {
                            TweenLite.masterList[_loc_2].push(this);
                        }
                    }
                    this.timeline.addChild(this);
                }
                else
                {
                    this.timeline.remove(this);
                }
            }
            return;
        }// end function

        public function renderTime(param1:Number) : void
        {
            return;
        }// end function

        public function get enabled() : Boolean
        {
            return !this.gc;
        }// end function

        public function get delay() : Number
        {
            return this._delay;
        }// end function

        public function get duration() : Number
        {
            return this.cachedDuration;
        }// end function

        public function set delay(param1:Number) : void
        {
            this.startTime = this.startTime + (param1 - this._delay);
            this._delay = param1;
            return;
        }// end function

        public function set totalDuration(param1:Number) : void
        {
            this.duration = param1;
            return;
        }// end function

        public function set duration(param1:Number) : void
        {
            var _loc_2:* = param1;
            this.cachedTotalDuration = param1;
            this.cachedDuration = _loc_2;
            return;
        }// end function

        public function get totalDuration() : Number
        {
            return this.cachedTotalDuration;
        }// end function

    }
}
