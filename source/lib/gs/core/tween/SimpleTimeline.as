﻿package gs.core.tween
{

    public class SimpleTimeline extends Tweenable
    {
        public var autoRemoveChildren:Boolean;
        protected var _lastChild:Tweenable;
        protected var _firstChild:Tweenable;

        public function SimpleTimeline(param1:Object = null)
        {
            super(0, param1);
            return;
        }// end function

        override public function renderTime(param1:Number) : void
        {
            var _loc_3:* = param1;
            this.cachedTime = param1;
            this.cachedTotalTime = _loc_3;
            var _loc_2:* = this._firstChild;
            while (_loc_2 != null)
            {
                
                if (_loc_2.active || param1 >= _loc_2.startTime && !_loc_2.cachedPaused)
                {
                    _loc_2.renderTime((param1 - _loc_2.startTime) * _loc_2.cachedTimeScale);
                }
                _loc_2 = _loc_2.nextNode;
            }
            return;
        }// end function

        public function addChild(param1:Tweenable) : void
        {
            if (param1.timeline != null && !param1.gc)
            {
                param1.timeline.remove(param1);
            }
            if (this._firstChild != null)
            {
                this._firstChild.prevNode = param1;
                param1.nextNode = this._firstChild;
            }
            else
            {
                param1.nextNode = null;
            }
            this._firstChild = param1;
            param1.prevNode = null;
            param1.timeline = this;
            param1.gc = false;
            return;
        }// end function

        public function remove(param1:Tweenable) : void
        {
            if (param1.nextNode != null)
            {
                param1.nextNode.prevNode = param1.prevNode;
            }
            else if (this._lastChild == param1)
            {
                this._lastChild = param1.prevNode;
            }
            if (param1.prevNode != null)
            {
                param1.prevNode.nextNode = param1.nextNode;
            }
            else if (this._firstChild == param1)
            {
                this._firstChild = param1.nextNode;
            }
            param1.gc = true;
            param1.active = false;
            return;
        }// end function

    }
}
