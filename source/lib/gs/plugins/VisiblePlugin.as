﻿package gs.plugins
{
    import gs.*;

    public class VisiblePlugin extends TweenPlugin
    {
        protected var _tween:TweenLite;
        protected var _hideAtStart:Boolean;
        protected var _target:Object;
        protected var _visible:Boolean;
        public static const VERSION:Number = 1.52;
        public static const API:Number = 1;

        public function VisiblePlugin()
        {
            this.propName = "visible";
            this.overwriteProps = ["visible"];
            this.onComplete = this.onCompleteTween;
            return;
        }// end function

        public function onCompleteTween() : void
        {
            if (!this._hideAtStart && (this._tween.cachedTime != 0 || this._tween.duration == 0))
            {
                this._target.visible = this._visible;
            }
            return;
        }// end function

        protected function init(param1:Object, param2:Boolean, param3:TweenLite) : void
        {
            this._target = param1;
            this._tween = param3;
            this._visible = param2;
            if (this._tween.vars.runBackwards == true && this._tween.vars.renderOnStart != false && param2 == false)
            {
                this._hideAtStart = true;
            }
            return;
        }// end function

        override public function onInitTween(param1:Object, param2, param3:TweenLite) : Boolean
        {
            this.init(param1, Boolean(param2), param3);
            return true;
        }// end function

        override public function set changeFactor(param1:Number) : void
        {
            if (this._hideAtStart && this._tween.cachedTotalTime == 0)
            {
                this._target.visible = false;
            }
            else if (this._target.visible != true)
            {
                this._target.visible = true;
            }
            return;
        }// end function

    }
}
