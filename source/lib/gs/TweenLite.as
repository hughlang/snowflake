﻿package gs
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import gs.core.tween.*;
    import gs.plugins.*;

    public class TweenLite extends Tweenable
    {
        protected var _hasPlugins:Boolean;
        public var propTweenLookup:Object;
        protected var _overwrittenProps:Object;
        public var target:Object;
        public var ease:Function;
        protected var _firstPropTween:PropTween;
        public static var rootTimeline:SimpleTimeline;
        public static var rootFramesTimeline:SimpleTimeline;
        public static var defaultEase:Function = TweenLite.easeOut;
        public static const version:Number = 11.09;
        public static var plugins:Object = {};
        public static var masterList:Dictionary = new Dictionary(false);
        public static var overwriteManager:Object;
        public static var rootFrame:Number;
        public static var killDelayedCallsTo:Function = TweenLite.killTweensOf;
        public static var timingSprite:Sprite = new Sprite();
        static var _reservedProps:Object = {ease:1, delay:1, overwrite:1, onComplete:1, onCompleteParams:1, useFrames:1, runBackwards:1, startAt:1, onUpdate:1, onUpdateParams:1, roundProps:1, onStart:1, onStartParams:1, renderOnStart:1, proxiedEase:1, easeParams:1, yoyo:1, loop:1, onCompleteListener:1, onUpdateListener:1, onStartListener:1, orientToBezier:1, persist:1, timeScale:1, forceImmediateRender:1, repeat:1, repeatDelay:1, timeline:1, data:1};

        public function TweenLite(param1:Object, param2:Number, param3:Object)
        {
            var _loc_4:* = 0;
            var _loc_5:* = null;
            var _loc_6:* = null;
            super(param2, param3);
            this.ease = typeof(this.vars.ease) != "function" ? (defaultEase) : (this.vars.ease);
            if (param1 == null)
            {
                return;
            }
            this.target = param1;
            if (this.vars.easeParams != null)
            {
                this.vars.proxiedEase = this.ease;
                this.ease = this.easeProxy;
            }
            this.propTweenLookup = {};
            if (!(param1 in masterList))
            {
                masterList[param1] = [this];
            }
            else
            {
                _loc_4 = param3.overwrite == undefined || !overwriteManager.enabled && param3.overwrite > 1 ? (overwriteManager.mode) : (int(param3.overwrite));
                if (_loc_4 == 1)
                {
                    _loc_5 = masterList[param1];
                    for each (_loc_6 in _loc_5)
                    {
                        
                        if (!_loc_6.gc)
                        {
                            _loc_6.timeline.remove(_loc_6);
                        }
                    }
                    masterList[param1] = [this];
                }
                else
                {
                    masterList[param1].push(this);
                }
            }
            if (this.active || this.vars.forceImmediateRender)
            {
                this.renderTime(0);
            }
            return;
        }// end function

        protected function easeProxy(param1:Number, param2:Number, param3:Number, param4:Number) : Number
        {
            return this.vars.proxiedEase.apply(null, arguments.concat(this.vars.easeParams));
        }// end function

        override public function renderTime(param1:Number) : void
        {
            var _loc_2:* = NaN;
            var _loc_3:* = false;
            var _loc_4:* = this.cachedTime;
            if (param1 >= this.cachedDuration)
            {
                var _loc_6:* = this.cachedDuration;
                this.cachedTime = this.cachedDuration;
                this.cachedTotalTime = _loc_6;
                _loc_2 = 1;
                _loc_3 = true;
            }
            else if (param1 <= 0)
            {
                var _loc_6:* = 0;
                _loc_2 = 0;
                this.cachedTime = _loc_6;
                this.cachedTotalTime = _loc_6;
                if (param1 < 0)
                {
                    this.active = false;
                }
            }
            else
            {
                var _loc_6:* = param1;
                this.cachedTime = param1;
                this.cachedTotalTime = _loc_6;
                _loc_2 = this.ease(param1, 0, 1, this.cachedDuration);
            }
            if (!this.initted)
            {
                this.init();
                this.active = true;
                if (this.vars.onStart != null)
                {
                    this.vars.onStart.apply(null, this.vars.onStartParams);
                }
            }
            else if (this.cachedTime == _loc_4)
            {
                return;
            }
            var _loc_5:* = this._firstPropTween;
            while (_loc_5 != null)
            {
                
                _loc_5.target[_loc_5.property] = _loc_5.start + _loc_2 * _loc_5.change;
                _loc_5 = _loc_5.nextNode;
            }
            if (_hasUpdate)
            {
                this.vars.onUpdate.apply(null, this.vars.onUpdateParams);
            }
            if (_loc_3)
            {
                this.complete(true);
            }
            return;
        }// end function

        protected function removePropTween(param1:PropTween) : void
        {
            if (param1.nextNode != null)
            {
                param1.nextNode.prevNode = param1.prevNode;
            }
            if (param1.prevNode != null)
            {
                param1.prevNode.nextNode = param1.nextNode;
            }
            else if (this._firstPropTween == param1)
            {
                this._firstPropTween = param1.nextNode;
            }
            return;
        }// end function

        protected function insertPropTween(param1:Object, param2:String, param3:Number, param4, param5:String, param6:Boolean, param7:PropTween) : PropTween
        {
            var _loc_9:* = null;
            var _loc_10:* = 0;
            var _loc_8:* = new PropTween(param1, param2, param3, typeof(param4) == "number" ? (param4 - param3) : (Number(param4)), param5, param6, param7);
            if (param7 != null)
            {
                param7.prevNode = _loc_8;
            }
            if (param6 && param5 == "_MULTIPLE_")
            {
                _loc_9 = param1.overwriteProps;
                _loc_10 = _loc_9.length - 1;
                while (_loc_10 > -1)
                {
                    
                    this.propTweenLookup[_loc_9[_loc_10]] = _loc_8;
                    _loc_10 = _loc_10 - 1;
                }
            }
            else
            {
                this.propTweenLookup[param5] = _loc_8;
            }
            return _loc_8;
        }// end function

        protected function init() : void
        {
            var _loc_1:* = null;
            var _loc_2:* = 0;
            var _loc_3:* = undefined;
            var _loc_4:* = false;
            var _loc_5:* = null;
            var _loc_7:* = null;
            var _loc_6:* = this.vars.isTV == true ? (this.vars.exposedVars) : (this.vars);
            this.propTweenLookup = {};
            if (_loc_6.timeScale != undefined && this.target is Tweenable)
            {
                this._firstPropTween = this.insertPropTween(this.target, "timeScale", this.target.timeScale, _loc_6.timeScale, "timeScale", false, this._firstPropTween);
            }
            for (_loc_1 in _loc_6)
            {
                
                if (_loc_1 in _reservedProps)
                {
                    continue;
                }
                if (_loc_1 in plugins)
                {
                    _loc_3 = new plugins[_loc_1];
                    if (_loc_3.onInitTween(this.target, _loc_6[_loc_1], this) == false)
                    {
                        this._firstPropTween = this.insertPropTween(this.target, _loc_1, this.target[_loc_1], _loc_6[_loc_1], _loc_1, false, this._firstPropTween);
                    }
                    else
                    {
                        this._firstPropTween = this.insertPropTween(_loc_3, "changeFactor", 0, 1, _loc_3.overwriteProps.length == 1 ? (_loc_3.overwriteProps[0]) : ("_MULTIPLE_"), true, this._firstPropTween);
                        this._hasPlugins = true;
                        if (_loc_3.priority != 0)
                        {
                            this._firstPropTween.priority = _loc_3.priority;
                            _loc_4 = true;
                        }
                    }
                    continue;
                }
                this._firstPropTween = this.insertPropTween(this.target, _loc_1, this.target[_loc_1], _loc_6[_loc_1], _loc_1, false, this._firstPropTween);
            }
            if (_loc_4)
            {
                _loc_5 = this._firstPropTween;
                _loc_7 = [];
                while (_loc_5 != null)
                {
                    
                    _loc_7[_loc_7.length] = _loc_5;
                    _loc_5 = _loc_5.nextNode;
                }
                _loc_7.sortOn("priority", Array.NUMERIC | Array.DESCENDING);
                _loc_2 = _loc_7.length - 1;
                while (_loc_2 > -1)
                {
                    
                    _loc_7[_loc_2].nextNode = _loc_7[(_loc_2 + 1)];
                    _loc_7[_loc_2].prevNode = _loc_7[(_loc_2 - 1)];
                    _loc_2 = _loc_2 - 1;
                }
                this._firstPropTween = _loc_7[0];
            }
            if (this.vars.runBackwards == true)
            {
                _loc_5 = this._firstPropTween;
                while (_loc_5 != null)
                {
                    
                    _loc_5.start = _loc_5.start + _loc_5.change;
                    _loc_5.change = -_loc_5.change;
                    _loc_5 = _loc_5.nextNode;
                }
            }
            _hasUpdate = Boolean(this.vars.onUpdate != null);
            if (this._overwrittenProps != null)
            {
                this.killVars(this._overwrittenProps);
            }
            if (TweenLite.overwriteManager.enabled && this._firstPropTween != null && this.target in masterList)
            {
                overwriteManager.manageOverwrites(this, this.propTweenLookup, masterList[this.target]);
            }
            this.initted = true;
            return;
        }// end function

        override public function complete(param1:Boolean = false) : void
        {
            var _loc_2:* = null;
            if (!param1)
            {
                this.renderTime(this.cachedTotalDuration);
                return;
            }
            if (this._hasPlugins)
            {
                _loc_2 = this._firstPropTween;
                while (_loc_2 != null)
                {
                    
                    if (_loc_2.isPlugin && _loc_2.target.onComplete != null)
                    {
                        _loc_2.target.onComplete();
                    }
                    _loc_2 = _loc_2.nextNode;
                }
            }
            if (this.timeline.autoRemoveChildren)
            {
                this.timeline.remove(this);
            }
            else
            {
                this.active = false;
            }
            if (this.vars.onComplete != null)
            {
                this.vars.onComplete.apply(null, this.vars.onCompleteParams);
            }
            return;
        }// end function

        public function killVars(param1:Object, param2:Boolean = true) : void
        {
            var _loc_3:* = null;
            var _loc_4:* = null;
            if (this._overwrittenProps == null)
            {
                this._overwrittenProps = {};
            }
            for (_loc_3 in param1)
            {
                
                if (_loc_3 in this.propTweenLookup)
                {
                    _loc_4 = this.propTweenLookup[_loc_3];
                    if (_loc_4.isPlugin && _loc_4.name == "_MULTIPLE_")
                    {
                        _loc_4.target.killProps(param1);
                        if (_loc_4.target.overwriteProps.length == 0)
                        {
                            this.removePropTween(_loc_4);
                            delete this.propTweenLookup[_loc_3];
                        }
                    }
                    else
                    {
                        this.removePropTween(_loc_4);
                        delete this.propTweenLookup[_loc_3];
                    }
                }
                if (param2)
                {
                    this._overwrittenProps[_loc_3] = 1;
                }
            }
            return;
        }// end function

        public static function delayedCall(param1:Number, param2:Function, param3:Array = null, param4:Boolean = false) : TweenLite
        {
            return new TweenLite(param2, 0, {delay:param1, onComplete:param2, onCompleteParams:param3, useFrames:param4, overwrite:0});
        }// end function

        public static function initClass() : void
        {
            TweenPlugin.activate([TintPlugin, RemoveTintPlugin, FramePlugin, AutoAlphaPlugin, VisiblePlugin, VolumePlugin, EndArrayPlugin]);
            rootFrame = 0;
            rootTimeline = new SimpleTimeline(null);
            rootFramesTimeline = new SimpleTimeline(null);
            rootTimeline.startTime = getTimer() * 0.001;
            rootFramesTimeline.startTime = rootFrame;
            var _loc_1:* = true;
            rootFramesTimeline.autoRemoveChildren = true;
            rootTimeline.autoRemoveChildren = _loc_1;
            timingSprite.addEventListener(Event.ENTER_FRAME, updateAll, false, 0, true);
            if (overwriteManager == null)
            {
                overwriteManager = {mode:1, enabled:false};
            }
            return;
        }// end function

        public static function removeTween(param1:TweenLite) : void
        {
            if (param1 != null)
            {
                param1.timeline.remove(param1);
            }
            return;
        }// end function

        public static function killTweensOf(param1:Object, param2:Boolean = false) : void
        {
            var _loc_3:* = null;
            var _loc_4:* = 0;
            if (param1 != null && param1 in masterList)
            {
                _loc_3 = masterList[param1];
                _loc_4 = _loc_3.length - 1;
                while (_loc_4 > -1)
                {
                    
                    if (!_loc_3[_loc_4].gc)
                    {
                        if (param2)
                        {
                            _loc_3[_loc_4].complete(false);
                        }
                        else
                        {
                            _loc_3[_loc_4].timeline.remove(_loc_3[_loc_4] as Tweenable);
                        }
                    }
                    _loc_4 = _loc_4 - 1;
                }
                delete masterList[param1];
            }
            return;
        }// end function

        public static function from(param1:Object, param2:Number, param3:Object) : TweenLite
        {
            param3.runBackwards = true;
            if (param3.renderOnStart != true && param3.forceImmediateRender != false)
            {
                param3.forceImmediateRender = true;
            }
            return new TweenLite(param1, param2, param3);
        }// end function

        static function easeOut(param1:Number, param2:Number, param3:Number, param4:Number) : Number
        {
            var _loc_5:* = param1 / param4;
            param1 = param1 / param4;
            return (-param3) * _loc_5 * (param1 - 2) + param2;
        }// end function

        static function updateAll(event:Event = null) : void
        {
            var _loc_2:* = null;
            var _loc_3:* = null;
            var _loc_4:* = null;
            var _loc_5:* = 0;
            rootTimeline.renderTime((getTimer() * 0.001 - rootTimeline.startTime) * rootTimeline.cachedTimeScale);
            var _loc_7:* = rootFrame + 1;
            rootFrame = _loc_7;
            rootFramesTimeline.renderTime(rootFrame * rootFramesTimeline.cachedTimeScale);
            if (!(rootFrame % 30))
            {
                _loc_2 = masterList;
                for (_loc_3 in _loc_2)
                {
                    
                    _loc_4 = _loc_2[_loc_3];
                    _loc_5 = _loc_4.length - 1;
                    while (_loc_5 > -1)
                    {
                        
                        if (_loc_4[_loc_5].gc)
                        {
                            _loc_4.splice(_loc_5, 1);
                        }
                        _loc_5 = _loc_5 - 1;
                    }
                    if (_loc_4.length == 0)
                    {
                        delete _loc_2[_loc_3];
                    }
                }
            }
            return;
        }// end function

        public static function to(param1:Object, param2:Number, param3:Object) : TweenLite
        {
            return new TweenLite(param1, param2, param3);
        }// end function

    }
}
