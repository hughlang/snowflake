﻿package gs
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import gs.core.tween.*;
    import gs.events.*;
    import gs.plugins.*;

    public class TweenMax extends TweenLite implements IEventDispatcher
    {
        protected var _reversed:Boolean;
        public var yoyo:Boolean;
        protected var _repeat:int;
        protected var _dispatcher:EventDispatcher;
        protected var _hasUpdateListener:Boolean;
        protected var _pauseTime:Number;
        protected var _repeatDelay:Number;
        public static var removeTween:Function = TweenLite.removeTween;
        private static var _overwriteMode:int = OverwriteManager.enabled ? (OverwriteManager.mode) : (OverwriteManager.init());
        private static var _pauseAllTime:Number;
        public static const version:Number = 11.09;
        public static var killTweensOf:Function = TweenLite.killTweensOf;
        public static var killDelayedCallsTo:Function = TweenLite.killTweensOf;
        private static var _activatedPlugins:Boolean = TweenPlugin.activate([TintPlugin, RemoveTintPlugin, FramePlugin, AutoAlphaPlugin, VisiblePlugin, VolumePlugin, EndArrayPlugin, HexColorsPlugin, BlurFilterPlugin, ColorMatrixFilterPlugin, BevelFilterPlugin, DropShadowFilterPlugin, GlowFilterPlugin, RoundPropsPlugin, BezierPlugin, BezierThroughPlugin, ShortRotationPlugin]);

        public function TweenMax(param1:Object, param2:Number, param3:Object)
        {
            super(param1, param2, param3);
            if (TweenLite.version < 11.09)
            {
                trace("TweenMax error! Please update your TweenLite class or try deleting your ASO files. TweenMax requires a more recent version. Download updates at http://www.TweenMax.com.");
            }
            if (typeof(this.vars.yoyo) == "number" || this.vars.loop != undefined)
            {
                trace("WARNING: Your tween uses the old \'yoyo\' or \'loop\' syntax which technically still works for now but has been deprecated in favor of the new, more flexible \'repeat\' and \'yoyo\' usage. See http://www.TweenMax.com for details.");
            }
            if (this.vars.yoyo == true || !isNaN(this.vars.yoyo))
            {
                this.yoyo = true;
            }
            this._repeat = (this.vars.loop == 0 ? (-1) : (Number(this.vars.loop))) || (this.vars.loop != undefined ? (typeof(this.vars.yoyo) == "number" ? (this.vars.yoyo == 0 ? (-1) : (this.vars.yoyo)) : (this.vars.repeat)) : (0));
            this._repeatDelay = this.vars.repeatDelay || 0;
            this.cacheIsDirty = true;
            if (this.vars.onCompleteListener != null || this.vars.onUpdateListener != null || this.vars.onStartListener != null)
            {
                this.initDispatcher();
                if (param2 == 0 && this.delay == 0)
                {
                    this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.UPDATE));
                    this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.COMPLETE));
                }
            }
            return;
        }// end function

        override public function renderTime(param1:Number) : void
        {
            var _loc_3:* = null;
            var _loc_5:* = false;
            var _loc_6:* = NaN;
            var _loc_8:* = NaN;
            var _loc_2:* = this.cacheIsDirty ? (this.totalDuration) : (this.cachedTotalDuration);
            var _loc_4:* = this.cachedTime;
            if (param1 >= _loc_2)
            {
                if (this._reversed)
                {
                    var _loc_9:* = 0;
                    _loc_6 = 0;
                    this.cachedTime = _loc_9;
                    this.cachedTotalTime = _loc_9;
                }
                else
                {
                    this.cachedTotalTime = _loc_2;
                    this.cachedTime = this.cachedDuration;
                    _loc_6 = 1;
                }
                _loc_5 = true;
            }
            else if (param1 <= 0)
            {
                if (param1 < 0)
                {
                    this.active = false;
                }
                if (this._reversed)
                {
                    this.cachedTotalTime = _loc_2;
                    this.cachedTime = this.cachedDuration;
                    _loc_6 = 1;
                    _loc_5 = true;
                }
                else
                {
                    var _loc_9:* = 0;
                    _loc_6 = 0;
                    this.cachedTime = _loc_9;
                    this.cachedTotalTime = _loc_9;
                }
            }
            else
            {
                var _loc_9:* = this._reversed ? (_loc_2 - param1) : (param1);
                this.cachedTime = this._reversed ? (_loc_2 - param1) : (param1);
                this.cachedTotalTime = _loc_9;
                if (this._repeat == 0)
                {
                    _loc_6 = this.ease(this.cachedTime, 0, 1, this.cachedDuration);
                }
            }
            if (this._repeat != 0)
            {
                _loc_8 = this.cachedDuration + this._repeatDelay;
                this.cachedTime = this.yoyo && this.cachedTotalTime / _loc_8 % 2 >= 1 || !this.yoyo && !(this.cachedTotalTime / _loc_8 % 1) ? (this.cachedDuration - this.cachedTotalTime % _loc_8) : (this.cachedTotalTime % _loc_8);
                if (this.cachedTime >= this.cachedDuration)
                {
                    this.cachedTime = this.cachedDuration;
                    _loc_6 = 1;
                }
                else if (this.cachedTime <= 0)
                {
                    var _loc_9:* = 0;
                    _loc_6 = 0;
                    this.cachedTime = _loc_9;
                }
                else
                {
                    _loc_6 = this.ease(this.cachedTime, 0, 1, this.cachedDuration);
                }
            }
            if (!this.initted)
            {
                this.init();
                if (!this.cachedPaused)
                {
                    this.active = true;
                }
                if (this.vars.onStart != null)
                {
                    this.vars.onStart.apply(null, this.vars.onStartParams);
                }
                if (this._dispatcher != null)
                {
                    this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.START));
                }
            }
            else if (_loc_4 == this.cachedTime)
            {
                return;
            }
            var _loc_7:* = _firstPropTween;
            while (_loc_7 != null)
            {
                
                _loc_7.target[_loc_7.property] = _loc_7.start + _loc_6 * _loc_7.change;
                _loc_7 = _loc_7.nextNode;
            }
            if (_hasUpdate)
            {
                this.vars.onUpdate.apply(null, this.vars.onUpdateParams);
            }
            if (this._hasUpdateListener)
            {
                this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.UPDATE));
            }
            if (_loc_5)
            {
                if (!this._reversed)
                {
                    this.complete(true);
                }
                else if (this._dispatcher != null)
                {
                    this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.REVERSE_COMPLETE));
                }
            }
            return;
        }// end function

        public function set reversed(param1:Boolean) : void
        {
            if (param1 != this._reversed)
            {
                this.reverse(true);
            }
            return;
        }// end function

        public function willTrigger(param1:String) : Boolean
        {
            return this._dispatcher == null ? (false) : (this._dispatcher.willTrigger(param1));
        }// end function

        public function restart(param1:Boolean = false) : void
        {
            this.totalTime = param1 ? (-_delay) : (0);
            this.paused = false;
            return;
        }// end function

        override protected function init() : void
        {
            var _loc_1:* = 0;
            var _loc_2:* = 0;
            var _loc_3:* = null;
            var _loc_4:* = null;
            var _loc_5:* = null;
            var _loc_6:* = null;
            var _loc_7:* = null;
            if (this.vars.startAt != null)
            {
                this.vars.startAt.overwrite = 0;
                new TweenMax(this.target, 0, this.vars.startAt);
            }
            super.init();
            if (this.vars.roundProps is Array && TweenLite.plugins.roundProps != null)
            {
                _loc_5 = this.vars.roundProps;
                _loc_1 = _loc_5.length - 1;
                while (_loc_1 > -1)
                {
                    
                    _loc_3 = _loc_5[_loc_1];
                    _loc_7 = this._firstPropTween;
                    while (_loc_7 != null)
                    {
                        
                        if (_loc_7.name == _loc_3)
                        {
                            if (_loc_7.isPlugin)
                            {
                                _loc_7.target.round = true;
                            }
                            else if (_loc_6 == null)
                            {
                                _loc_6 = new TweenLite.plugins.roundProps();
                                _loc_6.add(_loc_7.target, _loc_3, _loc_7.start, _loc_7.change);
                                _hasPlugins = true;
                                this._firstPropTween = insertPropTween(_loc_6, "changeFactor", 0, 1, "_MULTIPLE_", true, this._firstPropTween);
                            }
                            else
                            {
                                _loc_6.add(_loc_7.target, _loc_3, _loc_7.start, _loc_7.change);
                                this.removePropTween(_loc_7);
                            }
                        }
                        else if (_loc_7.isPlugin && _loc_7.name == "_MULTIPLE_" && !_loc_7.target.round)
                        {
                            _loc_4 = " " + _loc_7.target.overwriteProps.join(" ") + " ";
                            if (_loc_4.indexOf(" " + _loc_3 + " ") != -1)
                            {
                                _loc_7.target.round = true;
                            }
                        }
                        _loc_7 = _loc_7.nextNode;
                    }
                    _loc_1 = _loc_1 - 1;
                }
            }
            return;
        }// end function

        public function get totalProgress() : Number
        {
            return this._reversed ? (1 - this.cachedTotalTime / this.totalDuration) : (this.cachedTotalTime / this.totalDuration);
        }// end function

        protected function adjustStartValues() : void
        {
            var _loc_2:* = NaN;
            var _loc_3:* = NaN;
            var _loc_4:* = null;
            var _loc_5:* = NaN;
            var _loc_6:* = 0;
            var _loc_1:* = this.progress;
            if (_loc_1 != 0)
            {
                _loc_2 = this.ease(_loc_1, 0, 1, 1);
                _loc_3 = 1 / (1 - _loc_2);
                _loc_4 = _firstPropTween;
                while (_loc_4 != null)
                {
                    
                    _loc_5 = _loc_4.start + _loc_4.change;
                    if (_loc_4.isPlugin)
                    {
                        _loc_4.change = (_loc_5 - _loc_2) * _loc_3;
                    }
                    else
                    {
                        _loc_4.change = (_loc_5 - _loc_4.target[_loc_4.property]) * _loc_3;
                    }
                    _loc_4.start = _loc_5 - _loc_4.change;
                    _loc_4 = _loc_4.nextNode;
                }
            }
            return;
        }// end function

        public function set time(param1:Number) : void
        {
            this.totalTime = param1;
            return;
        }// end function

        override public function set delay(param1:Number) : void
        {
            super.delay = param1;
            this.setDirtyCache(true);
            return;
        }// end function

        public function set totalProgress(param1:Number) : void
        {
            this.totalTime = this._reversed ? ((1 - param1) * this.totalDuration) : (this.totalDuration * param1);
            return;
        }// end function

        override public function set duration(param1:Number) : void
        {
            this.timeScale = this.duration / param1;
            return;
        }// end function

        public function resume() : void
        {
            this.paused = false;
            return;
        }// end function

        public function get paused() : Boolean
        {
            return this.cachedPaused;
        }// end function

        public function killProperties(param1:Array) : void
        {
            var _loc_3:* = 0;
            var _loc_2:* = {};
            _loc_3 = param1.length - 1;
            while (_loc_3 > -1)
            {
                
                _loc_2[param1[_loc_3]] = true;
                _loc_3 = _loc_3 - 1;
            }
            killVars(_loc_2);
            return;
        }// end function

        public function get progress() : Number
        {
            return this._reversed ? (1 - this.cachedTime / this.duration) : (this.cachedTime / this.duration);
        }// end function

        public function get totalTime() : Number
        {
            return this.cachedTotalTime;
        }// end function

        override public function complete(param1:Boolean = false) : void
        {
            super.complete(param1);
            if (this._dispatcher != null)
            {
                this._dispatcher.dispatchEvent(new TweenEvent(TweenEvent.COMPLETE));
            }
            return;
        }// end function

        public function invalidate(param1:Boolean = true) : void
        {
            var _loc_2:* = NaN;
            var _loc_3:* = NaN;
            var _loc_4:* = null;
            if (this.initted)
            {
                _loc_2 = this.totalProgress;
                if (!param1)
                {
                    if (this.vars.runBackwards == true)
                    {
                        _loc_4 = _firstPropTween;
                        while (_loc_4 != null)
                        {
                            
                            _loc_4.start = _loc_4.start + _loc_4.change;
                            _loc_4.change = -_loc_4.change;
                            _loc_4 = _loc_4.nextNode;
                        }
                    }
                    this.totalProgress = 0;
                }
                _firstPropTween = null;
                var _loc_5:* = false;
                this._hasUpdateListener = false;
                _hasUpdate = _loc_5;
                _hasPlugins = _loc_5;
                this.init();
                this.cachedTimeScale = this.vars.timeScale || 1;
                _loc_3 = !isNaN(this.vars.delay) ? (this.vars.delay - _delay) : (-_delay);
                _delay = this.vars.delay || 0;
                this.startTime = this.startTime + _loc_3;
                this.setDirtyCache(true);
                if (this.vars.onCompleteListener != null || this.vars.onUpdateListener != null || this.vars.onStartListener != null)
                {
                    this._dispatcher = null;
                    this.initDispatcher();
                }
                if (_loc_2 != 0)
                {
                    if (param1)
                    {
                        this.adjustStartValues();
                    }
                    else
                    {
                        this.totalProgress = _loc_2;
                    }
                }
            }
            return;
        }// end function

        public function dispatchEvent(event:Event) : Boolean
        {
            return this._dispatcher == null ? (false) : (this._dispatcher.dispatchEvent(event));
        }// end function

        public function get reversed() : Boolean
        {
            return this._reversed;
        }// end function

        public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void
        {
            if (this._dispatcher != null)
            {
                this._dispatcher.removeEventListener(param1, param2, param3);
            }
            return;
        }// end function

        public function setDestination(param1:String, param2, param3:Boolean = true) : void
        {
            var _loc_4:* = {};
            _loc_4[param1] = param2;
            this.updateProperties(_loc_4, param3);
            return;
        }// end function

        public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void
        {
            if (this._dispatcher == null)
            {
                this.initDispatcher();
            }
            if (param1 == TweenEvent.UPDATE)
            {
                this._hasUpdateListener = true;
            }
            this._dispatcher.addEventListener(param1, param2, param3, param4, param5);
            return;
        }// end function

        protected function initDispatcher() : void
        {
            if (this._dispatcher == null)
            {
                this._dispatcher = new EventDispatcher(this);
                if (this.vars.onStartListener is Function)
                {
                    this._dispatcher.addEventListener(TweenEvent.START, this.vars.onStartListener, false, 0, true);
                }
                if (this.vars.onUpdateListener is Function)
                {
                    this._dispatcher.addEventListener(TweenEvent.UPDATE, this.vars.onUpdateListener, false, 0, true);
                    this._hasUpdateListener = true;
                }
                if (this.vars.onCompleteListener is Function)
                {
                    this._dispatcher.addEventListener(TweenEvent.COMPLETE, this.vars.onCompleteListener, false, 0, true);
                }
            }
            return;
        }// end function

        protected function setDirtyCache(param1:Boolean = true) : void
        {
            var _loc_2:* = param1 ? (this) : (this.timeline);
            while (_loc_2 != null)
            {
                
                _loc_2.cacheIsDirty = true;
                _loc_2 = _loc_2.timeline;
            }
            return;
        }// end function

        public function set progress(param1:Number) : void
        {
            this.totalTime = this._reversed ? ((1 - param1) * this.duration) : (this.duration * param1);
            return;
        }// end function

        protected function updateProperties(param1:Object, param2:Boolean = true) : void
        {
            var _loc_5:* = null;
            var _loc_6:* = null;
            var _loc_3:* = _firstPropTween;
            var _loc_4:* = this.cachedTotalTime;
            if (this.initted)
            {
                if (!param2)
                {
                    this.time = 0;
                }
                killVars(param1);
                for (_loc_5 in param1)
                {
                    
                    if (_loc_5 in _reservedProps)
                    {
                    }
                    else if (_loc_5 in plugins)
                    {
                        _loc_6 = new plugins[_loc_5];
                        if (_loc_6.onInitTween(this.target, param1[_loc_5], this) == false)
                        {
                            _firstPropTween = insertPropTween(this.target, _loc_5, this.target[_loc_5], param1[_loc_5], _loc_5, false, _firstPropTween);
                        }
                        else
                        {
                            _firstPropTween = insertPropTween(_loc_6, "changeFactor", 0, 1, _loc_6.overwriteProps.length == 1 ? (_loc_6.overwriteProps[0]) : ("_MULTIPLE_"), true, _firstPropTween);
                            _hasPlugins = true;
                        }
                    }
                    else
                    {
                        _firstPropTween = insertPropTween(this.target, _loc_5, this.target[_loc_5], param1[_loc_5], _loc_5, false, _firstPropTween);
                    }
                    this.vars[_loc_5] = param1[_loc_5];
                }
                if (param2 && this.cachedTotalTime != 0)
                {
                    this.adjustStartValues();
                    this.time = _loc_4;
                }
            }
            else
            {
                for (_loc_5 in param1)
                {
                    
                    this.vars[_loc_5] = param1[_loc_5];
                }
            }
            return;
        }// end function

        public function set totalTime(param1:Number) : void
        {
            this.startTime = this.timeline.cachedTotalTime - param1 / this.cachedTimeScale;
            this.renderTime(param1);
            this.setDirtyCache(false);
            this.enabled = true;
            return;
        }// end function

        public function set paused(param1:Boolean) : void
        {
            if (param1 != this.cachedPaused)
            {
                if (!param1)
                {
                    this.startTime = this.timeline.cachedTotalTime - this.cachedTotalTime / this.cachedTimeScale;
                }
                this.setDirtyCache(false);
                this.cachedPaused = param1;
                this.active = Boolean(!this.cachedPaused && this.cachedTotalTime > 0 && this.cachedTotalTime < this.cachedTotalDuration);
            }
            if (!param1 && this.gc)
            {
                this.enabled = true;
            }
            return;
        }// end function

        public function get time() : Number
        {
            return this.cachedTime;
        }// end function

        public function reverse(param1:Boolean = true) : void
        {
            this.startTime = this.timeline.cachedTotalTime - (1 - this.progress) * this.totalDuration / this.cachedTimeScale;
            this._reversed = !this._reversed;
            this.setDirtyCache(false);
            if (param1)
            {
                this.paused = false;
                if (this.progress == 1)
                {
                    this.time = 0;
                }
            }
            else
            {
                this.enabled = true;
            }
            return;
        }// end function

        public function hasEventListener(param1:String) : Boolean
        {
            return this._dispatcher == null ? (false) : (this._dispatcher.hasEventListener(param1));
        }// end function

        public function pause() : void
        {
            this.paused = true;
            return;
        }// end function

        override public function set totalDuration(param1:Number) : void
        {
            this.timeScale = this.totalDuration / param1;
            return;
        }// end function

        override public function get totalDuration() : Number
        {
            if (this.cacheIsDirty)
            {
                this.cachedTotalDuration = this._repeat == -1 ? (Infinity) : (this.cachedDuration * (this._repeat + 1) + this._repeatDelay * this._repeat);
                this.cacheIsDirty = false;
            }
            return this.cachedTotalDuration;
        }// end function

        public function set timeScale(param1:Number) : void
        {
            this.startTime = this.cachedTotalTime - (this.cachedTotalTime - this.startTime) * this.cachedTimeScale / param1;
            this.cachedTimeScale = param1;
            return;
        }// end function

        public function get timeScale() : Number
        {
            return this.cachedTimeScale;
        }// end function

        public static function set globalTimeScale(param1:Number) : void
        {
            var _loc_2:* = param1;
            TweenLite.rootTimeline.cachedTimeScale = param1;
            TweenLite.rootFramesTimeline.cachedTimeScale = _loc_2;
            return;
        }// end function

        public static function pauseAll(param1:Boolean = true, param2:Boolean = false) : void
        {
            changePause(true, param1, param2);
            return;
        }// end function

        public static function getTweensOf(param1:Object) : Array
        {
            var _loc_4:* = 0;
            var _loc_2:* = masterList[param1];
            var _loc_3:* = [];
            if (_loc_2 != null)
            {
                _loc_4 = _loc_2.length - 1;
                while (_loc_4 > -1)
                {
                    
                    if (!_loc_2[_loc_4].gc)
                    {
                        _loc_3[_loc_3.length] = _loc_2[_loc_4];
                    }
                    _loc_4 = _loc_4 - 1;
                }
            }
            return _loc_3;
        }// end function

        public static function killAllDelayedCalls(param1:Boolean = false) : void
        {
            killAll(param1, false, true);
            return;
        }// end function

        public static function get globalTimeScale() : Number
        {
            return TweenLite.rootTimeline.cachedTimeScale;
        }// end function

        public static function killChildTweensOf(param1:DisplayObjectContainer, param2:Boolean = false) : void
        {
            var _loc_4:* = 0;
            var _loc_5:* = null;
            var _loc_6:* = null;
            var _loc_3:* = getAllTweens();
            _loc_4 = _loc_3.length - 1;
            while (_loc_4 > -1)
            {
                
                _loc_5 = _loc_3[_loc_4].target;
                if (_loc_5 is DisplayObject)
                {
                    _loc_6 = _loc_5.parent;
                    while (_loc_6 != null)
                    {
                        
                        if (_loc_6 == param1)
                        {
                            if (param2)
                            {
                                _loc_3[_loc_4].complete(false);
                            }
                            else
                            {
                                TweenLite.removeTween(_loc_3[_loc_4]);
                            }
                        }
                        _loc_6 = _loc_6.parent;
                    }
                }
                _loc_4 = _loc_4 - 1;
            }
            return;
        }// end function

        public static function delayedCall(param1:Number, param2:Function, param3:Array = null, param4:Boolean = false) : TweenMax
        {
            return new TweenMax(param2, 0, {delay:param1, onComplete:param2, onCompleteParams:param3, useFrames:param4, overwrite:0});
        }// end function

        public static function isTweening(param1:Object) : Boolean
        {
            var _loc_3:* = 0;
            var _loc_2:* = getTweensOf(param1);
            _loc_3 = _loc_2.length - 1;
            while (_loc_3 > -1)
            {
                
                if ((_loc_2[_loc_3].active || _loc_2[_loc_3].startTime == _loc_2[_loc_3].cachedTime) && !_loc_2[_loc_3].gc)
                {
                    return true;
                }
                _loc_3 = _loc_3 - 1;
            }
            return false;
        }// end function

        public static function killAll(param1:Boolean = false, param2:Boolean = true, param3:Boolean = true) : void
        {
            var _loc_5:* = false;
            var _loc_6:* = 0;
            var _loc_4:* = getAllTweens();
            _loc_6 = _loc_4.length - 1;
            while (_loc_6 > -1)
            {
                
                _loc_5 = _loc_4[_loc_6].target == _loc_4[_loc_6].vars.onComplete;
                if (_loc_5 == param3 || _loc_5 != param2)
                {
                    if (param1)
                    {
                        _loc_4[_loc_6].complete(false);
                    }
                    else
                    {
                        TweenLite.removeTween(_loc_4[_loc_6]);
                    }
                }
                _loc_6 = _loc_6 - 1;
            }
            return;
        }// end function

        public static function changePause(param1:Boolean, param2:Boolean = true, param3:Boolean = false) : void
        {
            var _loc_5:* = 0;
            var _loc_6:* = false;
            var _loc_4:* = getAllTweens();
            _loc_5 = _loc_4.length - 1;
            while (_loc_5 > -1)
            {
                
                _loc_6 = _loc_4[_loc_5].target == _loc_4[_loc_5].vars.onComplete;
                if (_loc_4[_loc_5] is TweenMax && (_loc_6 == param3 || _loc_6 != param2))
                {
                    (_loc_4[_loc_5] as TweenMax).paused = param1;
                }
                _loc_5 = _loc_5 - 1;
            }
            return;
        }// end function

        public static function from(param1:Object, param2:Number, param3:Object) : TweenMax
        {
            param3.runBackwards = true;
            if (param3.renderOnStart != true && param3.forceImmediateRender != false)
            {
                param3.forceImmediateRender = true;
            }
            return new TweenMax(param1, param2, param3);
        }// end function

        public static function killAllTweens(param1:Boolean = false) : void
        {
            killAll(param1, true, false);
            return;
        }// end function

        public static function getAllTweens() : Array
        {
            var _loc_3:* = null;
            var _loc_4:* = 0;
            var _loc_1:* = masterList;
            var _loc_2:* = [];
            for each (_loc_3 in _loc_1)
            {
                
                _loc_4 = _loc_3.length - 1;
                while (_loc_4 > -1)
                {
                    
                    if (!_loc_3[_loc_4].gc)
                    {
                        _loc_2[_loc_2.length] = _loc_3[_loc_4];
                    }
                    _loc_4 = _loc_4 - 1;
                }
            }
            return _loc_2;
        }// end function

        public static function resumeAll(param1:Boolean = true, param2:Boolean = false) : void
        {
            changePause(false, param1, param2);
            return;
        }// end function

        public static function to(param1:Object, param2:Number, param3:Object) : TweenMax
        {
            return new TweenMax(param1, param2, param3);
        }// end function

    }
}
