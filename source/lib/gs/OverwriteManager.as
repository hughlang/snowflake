﻿package gs
{
    import gs.core.tween.*;

    public class OverwriteManager extends Object
    {
        public static const ALL:int = 1;
        public static var mode:int;
        public static const NONE:int = 0;
        public static var enabled:Boolean;
        public static const AUTO:int = 2;
        public static const CONCURRENT:int = 3;
        public static const version:Number = 5;

        public function OverwriteManager()
        {
            return;
        }// end function

        public static function getGlobalActive(param1:Tweenable) : Boolean
        {
            var _loc_2:* = param1;
            while (_loc_2 != null)
            {
                
                if (!_loc_2.active)
                {
                    return false;
                }
                _loc_2 = _loc_2.timeline;
            }
            return true;
        }// end function

        public static function manageOverwrites(param1:TweenLite, param2:Object, param3:Array) : void
        {
            var _loc_9:* = 0;
            var _loc_10:* = null;
            var _loc_11:* = NaN;
            var _loc_12:* = NaN;
            var _loc_13:* = null;
            var _loc_14:* = NaN;
            var _loc_15:* = null;
            var _loc_4:* = param1.vars;
            var _loc_5:* = _loc_4.overwrite == undefined ? (mode) : (int(_loc_4.overwrite));
            if (_loc_5 < 2 || param3 == null)
            {
                return;
            }
            var _loc_6:* = param1.startTime;
            var _loc_7:* = [];
            var _loc_8:* = [];
            _loc_9 = param3.length - 1;
            while (_loc_9 > -1)
            {
                
                _loc_10 = param3[_loc_9];
                if (_loc_10 == param1 || _loc_10.gc)
                {
                }
                else if (_loc_10.active && getGlobalActive(_loc_10) || _loc_10.timeline == param1.timeline && _loc_10.startTime <= _loc_6 && _loc_10.startTime + _loc_10.totalDuration > _loc_6)
                {
                    _loc_7[_loc_7.length] = _loc_10;
                }
                else if (_loc_10.timeline != param1.timeline)
                {
                    _loc_8[_loc_8.length] = _loc_10;
                }
                _loc_9 = _loc_9 - 1;
            }
            if (_loc_8.length != 0)
            {
                _loc_11 = param1.cachedTimeScale;
                _loc_12 = _loc_6;
                _loc_15 = param1.timeline;
                while (_loc_15 != null)
                {
                    
                    _loc_11 = _loc_11 * _loc_15.cachedTimeScale;
                    _loc_12 = _loc_12 + _loc_15.startTime;
                    _loc_15 = _loc_15.timeline;
                }
                _loc_6 = _loc_11 * _loc_12;
                _loc_9 = _loc_8.length - 1;
                while (_loc_9 > -1)
                {
                    
                    _loc_13 = _loc_8[_loc_9];
                    _loc_11 = _loc_13.cachedTimeScale;
                    _loc_12 = _loc_13.startTime;
                    _loc_15 = _loc_13.timeline;
                    while (_loc_15 != null)
                    {
                        
                        _loc_11 = _loc_11 * _loc_15.cachedTimeScale;
                        _loc_12 = _loc_12 + _loc_15.startTime;
                        _loc_15 = _loc_15.timeline;
                    }
                    _loc_14 = _loc_11 * _loc_12;
                    if (_loc_14 <= _loc_6 && _loc_14 + _loc_13.totalDuration * _loc_11 > _loc_6)
                    {
                        _loc_7[_loc_7.length] = _loc_13;
                    }
                    _loc_9 = _loc_9 - 1;
                }
            }
            if (_loc_7.length == 0)
            {
                return;
            }
            if (_loc_5 == AUTO)
            {
                _loc_9 = _loc_7.length - 1;
                while (_loc_9 > -1)
                {
                    
                    _loc_7[_loc_9].killVars(param2);
                    _loc_9 = _loc_9 - 1;
                }
            }
            else
            {
                _loc_9 = _loc_7.length - 1;
                while (_loc_9 > -1)
                {
                    
                    _loc_7[_loc_9].enabled = false;
                    _loc_9 = _loc_9 - 1;
                }
            }
            return;
        }// end function

        public static function init(param1:int = 2) : int
        {
            if (TweenLite.version < 11)
            {
                trace("TweenLite warning: Your TweenLite class needs to be updated to work with OverwriteManager (or you may need to clear your ASO files). Please download and install the latest version from http://www.tweenlite.com.");
            }
            TweenLite.overwriteManager = OverwriteManager;
            mode = param1;
            enabled = true;
            return mode;
        }// end function

    }
}
