﻿package tonfall.poly
{
    import tonfall.core.*;

    public interface IPolySynthVoice
    {

        public function IPolySynthVoice();

        function stop() : void;

        function start(event:TimeEvent) : void;

        function processAdd(param1:Signal, param2:int) : Boolean;

        function dispose() : void;

    }
}
