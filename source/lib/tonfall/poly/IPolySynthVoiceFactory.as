﻿package tonfall.poly
{
    import tonfall.core.*;

    public interface IPolySynthVoiceFactory
    {

        public function IPolySynthVoiceFactory();

        function create(param1:TimeEventNote) : IPolySynthVoice;

    }
}
