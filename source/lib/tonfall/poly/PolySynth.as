﻿package tonfall.poly
{
    import __AS3__.vec.*;
    import tonfall.core.*;

    final public class PolySynth extends SignalProcessor
    {
        public const output:SignalBuffer;
        public const paramVolume:Parameter;
        private const activeVoices:Vector.<IPolySynthVoice>;
        private var _voicefactory:IPolySynthVoiceFactory;

        public function PolySynth(param1:IPolySynthVoiceFactory)
        {
            this.output = new SignalBuffer();
            this.paramVolume = new Parameter("volume", 1);
            this.activeVoices = new Vector.<IPolySynthVoice>;
            this._voicefactory = param1;
            return;
        }// end function

        override protected function processTimeEvent(event:TimeEvent) : void
        {
            if (event is TimeEventNote)
            {
                this.startVoice(TimeEventNote(event));
            }
            return;
        }// end function

        private function startVoice(param1:TimeEventNote) : void
        {
            var _loc_2:* = this._voicefactory.create(param1);
            _loc_2.start(param1);
            this.activeVoices.push(_loc_2);
            return;
        }// end function

        override protected function processSignals(param1:int) : void
        {
            this.output.zero(param1);
            var _loc_2:* = this.output.current;
            var _loc_3:* = this.activeVoices.length;
            while (--_loc_3 > -1)
            {
                
                if (this.activeVoices[_loc_3].processAdd(_loc_2, param1))
                {
                    this.activeVoices.splice(_loc_3, 1);
                }
            }
            this.output.multiply(param1, this.paramVolume.value);
            this.output.advancePointer(param1);
            return;
        }// end function

    }
}
