﻿package tonfall.display
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import tonfall.core.*;

    final public class ParameterSlider extends Sprite
    {
        private var _dragOffset:Number = 0;
        private var _parameter:Parameter;
        private const textField:TextField;
        private var _dragging:Boolean;
        private const thumb:Thumb;
        private static const TEXT_FORMAT:TextFormat = new TextFormat("Verdana", 9, 0, true);

        public function ParameterSlider(param1:Parameter)
        {
            this.textField = new TextField();
            this.thumb = new Thumb();
            this._parameter = param1;
            this.init();
            return;
        }// end function

        private function mouseUp(event:MouseEvent) : void
        {
            if (this._dragging)
            {
                stage.removeEventListener(MouseEvent.MOUSE_UP, this.mouseUp);
                stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
                this._dragging = false;
            }
            return;
        }// end function

        private function mouseDown(event:MouseEvent) : void
        {
            this._dragging = true;
            this._dragOffset = event.target == this.thumb ? (this.thumb.mouseX - 4) : (0);
            this.update(mouseX);
            stage.addEventListener(MouseEvent.MOUSE_UP, this.mouseUp);
            stage.addEventListener(MouseEvent.MOUSE_MOVE, this.mouseMove);
            return;
        }// end function

        private function update(param1:Number) : void
        {
            var _loc_2:* = (param1 - this._dragOffset - 16) / 176;
            if (_loc_2 < 0)
            {
                _loc_2 = 0;
            }
            else if (_loc_2 > 1)
            {
                _loc_2 = 1;
            }
            this._parameter.value = _loc_2;
            this.updateView();
            return;
        }// end function

        private function init() : void
        {
            graphics.beginFill(16777215);
            graphics.drawRoundRect(0, 0, 288, 24, 4, 4);
            graphics.endFill();
            graphics.beginFill(3355443);
            graphics.drawRect(4, 4, 192, 16);
            graphics.endFill();
            this.textField.defaultTextFormat = TEXT_FORMAT;
            this.textField.selectable = false;
            this.textField.autoSize = TextFieldAutoSize.LEFT;
            this.textField.x = 204;
            this.textField.y = 6;
            this.thumb.y = 4;
            addChild(this.textField);
            addChild(this.thumb);
            addEventListener(MouseEvent.MOUSE_DOWN, this.mouseDown);
            this.updateView();
            return;
        }// end function

        private function updateView() : void
        {
            var _loc_1:* = this._parameter.value;
            this.thumb.x = 4 + _loc_1 * 176;
            this.textField.text = this._parameter.name + " " + Math.round(_loc_1 * 100) + "%";
            return;
        }// end function

        private function mouseMove(event:MouseEvent) : void
        {
            if (this._dragging)
            {
                this.update(mouseX);
            }
            return;
        }// end function

    }
}

import flash.display.*;

import flash.events.*;

import flash.text.*;

import tonfall.core.*;

class Thumb extends Sprite
{

    function Thumb()
    {
        graphics.beginFill(13421772);
        graphics.drawRect(0, 0, 16, 16);
        graphics.endFill();
        buttonMode = true;
        useHandCursor = true;
        cacheAsBitmap = true;
        return;
    }// end function

}

