﻿package tonfall.display
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.media.*;
    import flash.utils.*;
    import tonfall.core.*;

    final public class Spectrum extends Sprite
    {
        private const bitmapWaveform:Bitmap;
        private const bitmapPeaksRight:Bitmap;
        private const outputArray:ByteArray;
        private const bitmapSpectrum:Bitmap;
        private const rectPeak:Rectangle;
        private const WIDTH:int = 352;
        private const HEIGHT:int = 304;
        private const driver:Driver;
        private const bitmapPeaksLeft:Bitmap;
        private const BACKGROUND:uint = 3355443;
        private const rectLine:Rectangle;

        public function Spectrum()
        {
            this.driver = Driver.getInstance();
            this.bitmapWaveform = new Bitmap(new BitmapData(256, 128, false, 3355443), PixelSnapping.ALWAYS);
            this.bitmapSpectrum = new Bitmap(new BitmapData(256, 128, false, 3355443), PixelSnapping.ALWAYS);
            this.bitmapPeaksLeft = new Bitmap(new BitmapData(16, 272, false, 3355443), PixelSnapping.ALWAYS);
            this.bitmapPeaksRight = new Bitmap(new BitmapData(16, 272, false, 3355443), PixelSnapping.ALWAYS);
            this.outputArray = new ByteArray();
            this.rectLine = new Rectangle(0, 0, 1, 0);
            this.rectPeak = new Rectangle(0, 0, 16, 0);
            graphics.beginFill(2236962);
            graphics.drawRoundRect(0, 0, this.WIDTH, this.HEIGHT, 8, 8);
            graphics.endFill();
            this.bitmapWaveform.x = 16;
            this.bitmapWaveform.y = 16;
            addChild(this.bitmapWaveform);
            this.bitmapSpectrum.x = 16;
            this.bitmapSpectrum.y = 160;
            addChild(this.bitmapSpectrum);
            this.bitmapPeaksLeft.x = 288;
            this.bitmapPeaksLeft.y = 16;
            addChild(this.bitmapPeaksLeft);
            this.bitmapPeaksRight.x = 320;
            this.bitmapPeaksRight.y = 16;
            addChild(this.bitmapPeaksRight);
            addEventListener(Event.ADDED_TO_STAGE, this.added);
            addEventListener(Event.REMOVED_FROM_STAGE, this.removed);
            return;
        }// end function

        public function dispose() : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.added);
            removeEventListener(Event.REMOVED_FROM_STAGE, this.removed);
            return;
        }// end function

        private function added(event:Event) : void
        {
            addEventListener(Event.ENTER_FRAME, this.enterFrame);
            return;
        }// end function

        override public function get width() : Number
        {
            return this.WIDTH;
        }// end function

        private function paintSpectrum() : void
        {
            var _loc_2:* = NaN;
            var _loc_3:* = NaN;
            var _loc_4:* = 0;
            var _loc_1:* = this.bitmapSpectrum.bitmapData;
            _loc_1.lock();
            _loc_1.fillRect(_loc_1.rect, this.BACKGROUND);
            SoundMixer.computeSpectrum(this.outputArray, true, 1);
            var _loc_5:* = 0;
            while (_loc_5 < 256)
            {
                
                this.outputArray.position = _loc_5 << 2;
                _loc_2 = this.outputArray.readFloat();
                this.outputArray.position = (_loc_5 | 256) << 2;
                _loc_3 = this.outputArray.readFloat();
                _loc_4 = (_loc_2 > _loc_3 ? (_loc_2) : (_loc_3)) * 128;
                this.rectLine.x = _loc_5;
                this.rectLine.y = 128 - _loc_4;
                this.rectLine.height = _loc_4;
                _loc_1.fillRect(this.rectLine, 11184810);
                _loc_5++;
            }
            _loc_1.unlock();
            return;
        }// end function

        private function paintPeak(param1:BitmapData, param2:Number) : void
        {
            param1.lock();
            var _loc_3:* = (1 - param2) * 272;
            this.rectPeak.y = 0;
            this.rectPeak.height = _loc_3;
            param1.fillRect(this.rectPeak, this.BACKGROUND);
            this.rectPeak.y = _loc_3;
            this.rectPeak.height = 272 - _loc_3;
            param1.fillRect(this.rectPeak, 11184810);
            param1.unlock();
            return;
        }// end function

        private function removed(event:Event) : void
        {
            removeEventListener(Event.ENTER_FRAME, this.enterFrame);
            return;
        }// end function

        override public function get height() : Number
        {
            return this.HEIGHT;
        }// end function

        private function paintWaveform() : void
        {
            var _loc_2:* = NaN;
            var _loc_3:* = NaN;
            var _loc_1:* = this.bitmapWaveform.bitmapData;
            _loc_1.lock();
            _loc_1.fillRect(_loc_1.rect, this.BACKGROUND);
            SoundMixer.computeSpectrum(this.outputArray, false);
            var _loc_4:* = 0;
            while (_loc_4 < 256)
            {
                
                this.outputArray.position = _loc_4 << 2;
                _loc_2 = this.outputArray.readFloat();
                this.outputArray.position = (_loc_4 | 256) << 2;
                _loc_3 = this.outputArray.readFloat();
                _loc_1.setPixel(_loc_4, 64 + _loc_2 * 64, 11184810);
                _loc_1.setPixel(_loc_4, 64 + _loc_3 * 64, 13421772);
                _loc_4++;
            }
            _loc_1.unlock();
            return;
        }// end function

        private function enterFrame(event:Event) : void
        {
            this.paintWaveform();
            this.paintSpectrum();
            this.paintPeak(this.bitmapPeaksLeft.bitmapData, this.driver.leftPeak);
            this.paintPeak(this.bitmapPeaksRight.bitmapData, this.driver.rightPeak);
            return;
        }// end function

    }
}
