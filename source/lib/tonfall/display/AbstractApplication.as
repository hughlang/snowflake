﻿package tonfall.display
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import tonfall.core.*;

    public class AbstractApplication extends Sprite
    {
        protected const engine:Engine;
        protected const driver:Driver;
        private var _showSpectrum:Boolean;
        protected const spectrum:Spectrum;
        private var _sliderIndex:int = 0;

        public function AbstractApplication()
        {
            this.driver = Driver.getInstance();
            this.engine = Engine.getInstance();
            this.spectrum = new Spectrum();
            addEventListener(Event.ADDED_TO_STAGE, this.addedToStage);
            Memory.length = Driver.BLOCK_SIZE << 3;
            this.driver.engine = this.engine;
            setTimeout(this.driver.init, 100);
            return;
        }// end function

        protected function addedToStage(event:Event) : void
        {
            removeEventListener(Event.ADDED_TO_STAGE, this.addedToStage);
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            stage.addEventListener(Event.RESIZE, this.resize);
            stage.frameRate = 1000;
            this.resize();
            return;
        }// end function

        public function addParameterSlider(param1:Parameter) : ParameterSlider
        {
            var _loc_2:* = new ParameterSlider(param1);
            _loc_2.x = 16;
            var _loc_3:* = this;
            _loc_3._sliderIndex = this._sliderIndex + 1;
            _loc_2.y = 36 * this._sliderIndex++ + 16;
            addChild(_loc_2);
            return _loc_2;
        }// end function

        public function get showSpectrum() : Boolean
        {
            return this._showSpectrum;
        }// end function

        protected function resize(event:Event = null) : void
        {
            this.spectrum.x = stage.stageWidth - this.spectrum.width >> 1;
            this.spectrum.y = stage.stageHeight - this.spectrum.height >> 1;
            return;
        }// end function

        public function set showSpectrum(param1:Boolean) : void
        {
            if (this._showSpectrum != param1)
            {
                if (param1)
                {
                    addChild(this.spectrum);
                }
                else
                {
                    removeChild(this.spectrum);
                }
                this._showSpectrum = param1;
            }
            return;
        }// end function

    }
}
