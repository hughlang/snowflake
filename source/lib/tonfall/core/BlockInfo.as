﻿package tonfall.core
{

    final public class BlockInfo extends Object
    {
        private var _numSignals:int;
        private var _barTo:Number;
        private var _barFrom:Number;

        public function BlockInfo()
        {
            return;
        }// end function

        public function get numSignals() : int
        {
            return this._numSignals;
        }// end function

        public function toString() : String
        {
            return "[BlockInfo numSignals: " + this._numSignals + ", barFrom: " + this._barFrom.toFixed(3) + ", barTo: " + this._barTo.toFixed(3) + "]";
        }// end function

        public function get barTo() : Number
        {
            return this._barTo;
        }// end function

        function reset(param1:int, param2:Number, param3:Number) : void
        {
            this._numSignals = param1;
            this._barFrom = param2;
            this._barTo = param3;
            return;
        }// end function

        public function get barFrom() : Number
        {
            return this._barFrom;
        }// end function

    }
}
