﻿package tonfall.core
{
    import flash.events.*;
    import flash.media.*;
    import flash.utils.*;

    final public class Driver extends Object
    {
        private var _running:Boolean;
        private const sound:Sound;
        private var _soundChannel:SoundChannel;
        private var _engine:Engine;
        private const zeroBytes:ByteArray;
        private const fillBytes:ByteArray;
        private var _latency:Number = 0;
        public static const BLOCK_SIZE:int = 3072;
        private static var instance:Driver = null;

        public function Driver()
        {
            this.sound = new Sound();
            this.zeroBytes = new ByteArray();
            this.fillBytes = new ByteArray();
            if (instance != null)
            {
                throw new Error("AudioDriver is Singleton.");
            }
            this.zeroBytes.length = BLOCK_SIZE << 3;
            this.fillBytes.length = BLOCK_SIZE << 3;
            this.sound.addEventListener(SampleDataEvent.SAMPLE_DATA, this.sampleData);
            return;
        }// end function

        public function set engine(param1:Engine) : void
        {
            this._engine = param1;
            return;
        }// end function

        public function get engine() : Engine
        {
            return this._engine;
        }// end function

        public function get leftPeak() : Number
        {
            if (this._soundChannel == null)
            {
                return 0;
            }
            return this._soundChannel.leftPeak;
        }// end function

        public function get running() : Boolean
        {
            return this._running;
        }// end function

        public function stop() : void
        {
            this.sound.removeEventListener(SampleDataEvent.SAMPLE_DATA, this.sampleData);
            return;
        }// end function

        public function set running(param1:Boolean) : void
        {
            this._running = param1;
            return;
        }// end function

        public function init() : void
        {
            if (this._soundChannel != null)
            {
                throw new Error("Cannot inited twice.");
            }
            this._soundChannel = this.sound.play();
            this._running = true;
            return;
        }// end function

        public function get volume() : Number
        {
            if (this._soundChannel == null)
            {
                return 0;
            }
            return this._soundChannel.soundTransform.volume;
        }// end function

        public function get rightPeak() : Number
        {
            if (this._soundChannel == null)
            {
                return 0;
            }
            return this._soundChannel.rightPeak;
        }// end function

        public function start() : void
        {
            this.sound.addEventListener(SampleDataEvent.SAMPLE_DATA, this.sampleData);
            return;
        }// end function

        public function set volume(param1:Number) : void
        {
            if (this._soundChannel == null)
            {
                return;
            }
            var _loc_2:* = new SoundTransform();
            _loc_2.volume = param1;
            this._soundChannel.soundTransform = _loc_2;
            return;
        }// end function

        private function sampleData(event:SampleDataEvent) : void
        {
            var event:* = event;
            if (this._soundChannel != null)
            {
                this._latency = event.position / 44.1 - this._soundChannel.position;
            }
            if (this._engine == null || !this._running)
            {
                event.data.writeBytes(this.zeroBytes);
            }
            else
            {
                this.fillBytes.position = 0;
                try
                {
                    this._engine.render(this.fillBytes, BLOCK_SIZE);
                }
                catch (e:Error)
                {
                    trace("Error while rendering Audio.", e.getStackTrace());
                    return;
                }
                event.data.writeBytes(this.fillBytes);
            }
            return;
        }// end function

        public function get latency() : Number
        {
            return this._latency;
        }// end function

        public static function getInstance() : Driver
        {
            if (instance == null)
            {
                instance = new Driver;
            }
            return instance;
        }// end function

    }
}
