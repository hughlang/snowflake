﻿package tonfall.core
{

    public class SignalProcessor extends Processor
    {

        public function SignalProcessor()
        {
            return;
        }// end function

        protected function processSignals(param1:int) : void
        {
            throw new Error("Method \"processSignals\" is marked abstract.");
        }// end function

        protected function processTimeEvent(event:TimeEvent) : void
        {
            throw new Error("Method \"processTimeEvent\" is marked abstract.");
        }// end function

        final override public function process(param1:BlockInfo) : void
        {
            var _loc_2:* = null;
            var _loc_5:* = 0;
            var _loc_3:* = 0;
            var _loc_4:* = param1.numSignals;
            while (events.length)
            {
                
                _loc_2 = events.shift();
                _loc_5 = engine.deltaBlockIndexAt(_loc_2.barPosition) - _loc_3;
                if (_loc_5 > 0)
                {
                    this.processSignals(_loc_5);
                    _loc_4 = _loc_4 - _loc_5;
                    _loc_3 = _loc_3 + _loc_5;
                }
                this.processTimeEvent(_loc_2);
                _loc_2.dispose();
            }
            if (_loc_4)
            {
                this.processSignals(_loc_4);
            }
            return;
        }// end function

    }
}
