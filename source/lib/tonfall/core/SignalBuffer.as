﻿package tonfall.core
{
    import __AS3__.vec.*;

    final public class SignalBuffer extends Object
    {
        private var _vector:Vector.<Signal>;
        private var _length:int;
        private var _index:int;
        private var _current:Signal;

        public function SignalBuffer(param1:int = 0)
        {
            this.init(param1 <= 0 ? (Driver.BLOCK_SIZE) : (param1));
            return;
        }// end function

        public function get vector() : Vector.<Signal>
        {
            return this._vector;
        }// end function

        public function zero(param1:int) : void
        {
            var _loc_2:* = this._current;
            var _loc_3:* = 0;
            while (_loc_3 < param1)
            {
                
                var _loc_4:* = 0;
                _loc_2.r = 0;
                _loc_2.l = _loc_4;
                _loc_2 = _loc_2.next;
                _loc_3++;
            }
            return;
        }// end function

        public function deltaPointer(param1:int) : Signal
        {
            var _loc_2:* = this._index + param1;
            if (_loc_2 < 0)
            {
                _loc_2 = _loc_2 + this._length;
            }
            else if (_loc_2 >= this._length)
            {
                _loc_2 = _loc_2 - this._length;
            }
            return this._vector[_loc_2];
        }// end function

        private function init(param1:int) : void
        {
            var _loc_2:* = null;
            var _loc_3:* = null;
            this._vector = new Vector.<Signal>(param1, true);
            var _loc_5:* = new Signal();
            this._vector[0] = new Signal();
            _loc_2 = _loc_5;
            _loc_3 = _loc_5;
            var _loc_4:* = 1;
            while (_loc_4 < param1)
            {
                
                var _loc_5:* = new Signal();
                this._vector[_loc_4] = new Signal();
                _loc_3.next = _loc_5;
                _loc_3 = _loc_5;
                _loc_4++;
            }
            var _loc_5:* = _loc_2;
            _loc_3.next = _loc_2;
            this._current = _loc_5;
            this._index = 0;
            this._length = param1;
            return;
        }// end function

        public function get current() : Signal
        {
            return this._current;
        }// end function

        public function advancePointer(param1:int) : void
        {
            if (param1 == 0)
            {
                return;
            }
            this._index = this._index + param1;
            if (this._index < 0)
            {
                this._index = this._index + this._length;
            }
            else if (this._index >= this._length)
            {
                this._index = this._index - this._length;
            }
            this._current = this._vector[this._index];
            return;
        }// end function

        public function multiply(param1:int, param2:Number) : void
        {
            var _loc_3:* = this._current;
            var _loc_4:* = 0;
            while (_loc_4 < param1)
            {
                
                _loc_3.l = _loc_3.l * param2;
                _loc_3.r = _loc_3.r * param2;
                _loc_3 = _loc_3.next;
                _loc_4++;
            }
            return;
        }// end function

    }
}
