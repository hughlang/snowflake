﻿package tonfall.core
{
    import __AS3__.vec.*;

    public class Processor extends Object
    {
        protected const engine:Engine;
        protected const events:Vector.<TimeEvent>;

        public function Processor()
        {
            this.events = new Vector.<TimeEvent>;
            this.engine = Engine.getInstance();
            return;
        }// end function

        private function sortOnPosition(event:TimeEvent, param2:TimeEvent) : int
        {
            if (event.barPosition > param2.barPosition)
            {
                return 1;
            }
            if (event.barPosition < param2.barPosition)
            {
                return -1;
            }
            return 0;
        }// end function

        public function addTimeEvent(event:TimeEvent) : void
        {
            if (this.events.indexOf(event) > -1)
            {
                throw new Error("Element already exists.");
            }
            this.events.push(event);
            this.events.sort(this.sortOnPosition);
            return;
        }// end function

        public function process(param1:BlockInfo) : void
        {
            throw new Error("Method \"process\" is marked abstract.");
        }// end function

    }
}
