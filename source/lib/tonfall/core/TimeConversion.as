﻿package tonfall.core
{

    final public class TimeConversion extends Object
    {

        public function TimeConversion()
        {
            return;
        }// end function

        public static function millisToBars(param1:Number, param2:Number) : Number
        {
            return param1 * param2 / 240 / 1000;
        }// end function

        public static function barsToMillis(param1:Number, param2:Number) : Number
        {
            return param1 * 240 / param2 * 1000;
        }// end function

        public static function numSamplesToBars(param1:Number, param2:Number) : Number
        {
            return param1 * param2 / 240 / samplingRate;
        }// end function

        public static function barsToNumSamples(param1:Number, param2:Number) : Number
        {
            return param1 * 240 / param2 * samplingRate;
        }// end function

    }
}
