﻿package tonfall.core
{
    import __AS3__.vec.*;
    import flash.utils.*;

    final public class Engine extends Object
    {
        private var _input:SignalBuffer;
        private var _bpm:Number;
        private var _bar:Number;
        private var _processors:Vector.<Processor>;
        private const blockInfo:BlockInfo;
        private static var instance:Engine = null;

        public function Engine()
        {
            this.blockInfo = new BlockInfo();
            if (instance != null)
            {
                throw new Error("AudioEngine is Singleton.");
            }
            this._bar = 0;
            this._bpm = 120;
            this._processors = new Vector.<Processor>;
            return;
        }// end function

        public function get processors() : Vector.<Processor>
        {
            return this._processors;
        }// end function

        public function set input(param1:SignalBuffer) : void
        {
            this._input = param1;
            return;
        }// end function

        public function set bar(param1:Number) : void
        {
            this._bar = param1;
            return;
        }// end function

        public function deltaBlockIndexAt(param1:Number) : int
        {
            var _loc_2:* = TimeConversion.barsToNumSamples(param1 - this._bar, this._bpm);
            if (_loc_2 < 0 || _loc_2 >= Driver.BLOCK_SIZE)
            {
                throw new Error("Index out of Block. index: " + _loc_2);
            }
            return _loc_2;
        }// end function

        private function writeInput(param1:ByteArray, param2:int) : void
        {
            var _loc_3:* = this._input.current;
            var _loc_4:* = 0;
            while (_loc_4 < param2)
            {
                
                param1.writeFloat(_loc_3.l);
                param1.writeFloat(_loc_3.r);
                _loc_3 = _loc_3.next;
                _loc_4++;
            }
            return;
        }// end function

        function render(param1:ByteArray, param2:int) : void
        {
            var _loc_3:* = this._bar + TimeConversion.numSamplesToBars(param2, this._bpm);
            this.blockInfo.reset(param2, this._bar, _loc_3);
            this.renderProcessors();
            this._bar = _loc_3;
            if (this._input != null)
            {
                this.writeInput(param1, param2);
            }
            return;
        }// end function

        private function renderProcessors() : void
        {
            var _loc_1:* = 0;
            var _loc_2:* = this._processors.length;
            while (_loc_1 < _loc_2)
            {
                
                this._processors[_loc_1].process(this.blockInfo);
                _loc_1++;
            }
            return;
        }// end function

        public function get bar() : Number
        {
            return this._bar;
        }// end function

        public function set bpm(param1:Number) : void
        {
            this._bpm = param1;
            return;
        }// end function

        public function get bpm() : Number
        {
            return this._bpm;
        }// end function

        public static function getInstance() : Engine
        {
            if (instance == null)
            {
                instance = new Engine;
            }
            return instance;
        }// end function

    }
}
