﻿package tonfall.core
{

    final public class Parameter extends Object
    {
        private var _value:Number;
        private var _name:String;

        public function Parameter(param1:String, param2:Number = 0)
        {
            this._name = param1;
            this._value = param2;
            return;
        }// end function

        public function get value() : Number
        {
            return this._value;
        }// end function

        public function set value(param1:Number) : void
        {
            this._value = param1;
            return;
        }// end function

        public function get name() : String
        {
            return this._name;
        }// end function

    }
}
