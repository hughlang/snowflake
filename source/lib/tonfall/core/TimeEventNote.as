﻿package tonfall.core
{

    final public class TimeEventNote extends TimeEvent
    {
        public var note:Number = 60;
        public var barDuration:Number = 0;
        public var velocity:Number = 1;

        public function TimeEventNote()
        {
            return;
        }// end function

        public function toString() : String
        {
            return "[TimeEventNote barPosition: " + barPosition + ", barDuration: " + this.barDuration + ", note: " + this.note + ", velocity: " + this.velocity + "]";
        }// end function

    }
}
