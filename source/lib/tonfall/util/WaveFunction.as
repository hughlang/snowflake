﻿package tonfall.util
{

    final public class WaveFunction extends Object
    {

        public function WaveFunction()
        {
            return;
        }// end function

        public static function normSawtooth(param1:Number) : Number
        {
            return param1;
        }// end function

        public static function normTriangle(param1:Number) : Number
        {
            if (param1 < 0.5)
            {
                return param1 * 2;
            }
            return 2 - param1 * 2;
        }// end function

        public static function biSawtooth(param1:Number) : Number
        {
            return param1 * 2 - 1;
        }// end function

        public static function normSquare(param1:Number, param2:Number = 0.5) : Number
        {
            return param1 < param2 ? (1) : (0);
        }// end function

        public static function normSinus(param1:Number) : Number
        {
            return Math.sin(param1 * 2 * Math.PI) * 0.5 + 0.5;
        }// end function

        public static function biSquare(param1:Number, param2:Number = 0.5) : Number
        {
            return param1 < param2 ? (1) : (-1);
        }// end function

        public static function biTriangle(param1:Number) : Number
        {
            if (param1 < 0.5)
            {
                return param1 * 4 - 1;
            }
            return 3 - param1 * 4;
        }// end function

        public static function biSinus(param1:Number) : Number
        {
            return Math.sin(param1 * 2 * Math.PI);
        }// end function

    }
}
