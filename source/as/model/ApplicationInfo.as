﻿package model
{
    import model.info.*;

    public class ApplicationInfo extends Object
    {
        public var twitterInfo:TwitterInfo;
        public var cdnURL:String;
        public var loadFile:String;
        public var bitlyURL:String;
        public var sendImageFile:String;
        public var facebookInfo:FacebookInfo;
        public var saveFile:String;
        public var flakeParameterName:String;
        public var flakePrefix:String;
        public var originURL:String;
        public var flakeSuffix:String;
        public var cdnXMLPath:String;
        public var likeSortFile:String;
        public var cdnImagePath:String;
        public var recentSortFile:String;
        public var loadLikes:String;
        public var snowFallFile:String;
        public var charityActive:Boolean;

        public function ApplicationInfo() : void
        {
            return;
        }// end function

        public static function parseInfo(param1:XML) : ApplicationInfo
        {
            if (!param1)
            {
                return null;
            }
            var _loc_2:* = new ApplicationInfo;
            _loc_2.originURL = param1["flurrious"]["originURL"];
            _loc_2.cdnURL = param1["flurrious"]["cdnURL"];
            _loc_2.cdnImagePath = param1["flurrious"]["cdnImagePath"];
            _loc_2.cdnXMLPath = param1["flurrious"]["cdnXMLPath"];
            _loc_2.loadFile = param1["flurrious"]["loadFlake"];
            _loc_2.loadLikes = _loc_2.originURL + param1["flurrious"]["loadFlakeLike"];
            _loc_2.snowFallFile = _loc_2.originURL + param1["flurrious"]["snowFallFile"];
            _loc_2.recentSortFile = _loc_2.originURL + param1["flurrious"]["recentSortFile"];
            _loc_2.likeSortFile = _loc_2.originURL + param1["flurrious"]["likeSortFile"];
            _loc_2.saveFile = _loc_2.originURL + param1["flurrious"]["saveFlake"];
            _loc_2.sendImageFile = _loc_2.originURL + param1["flurrious"]["sendImageFile"];
            _loc_2.charityActive = param1["flurrious"]["charityActive"] == "1" ? (true) : (false);
            _loc_2.flakePrefix = param1["flurrious"]["flakePrefix"];
            _loc_2.flakeSuffix = param1["flurrious"]["flakeSuffix"];
            _loc_2.flakeParameterName = param1["flurrious"]["flakeParameterName"];
            _loc_2.facebookInfo = new FacebookInfo();
            _loc_2.facebookInfo.postURL = param1["facebook"]["postURL"];
            _loc_2.facebookInfo.titleParameterName = param1["facebook"]["titleParameterName"];
            _loc_2.facebookInfo.summaryParameterName = param1["facebook"]["summaryParameterName"];
            _loc_2.facebookInfo.urlParameterName = param1["facebook"]["urlParameterName"];
            _loc_2.facebookInfo.charityTitle = param1["facebook"]["charityTitle"];
            _loc_2.facebookInfo.charityBody = param1["facebook"]["charityBody"];
            _loc_2.facebookInfo.noCharityTitle = param1["facebook"]["noCharityTitle"];
            _loc_2.facebookInfo.noCharityBody = param1["facebook"]["noCharityBody"];
            _loc_2.twitterInfo = new TwitterInfo();
            _loc_2.twitterInfo.postURL = param1["twitter"]["postURL"];
            _loc_2.twitterInfo.charityBody = param1["twitter"]["charityBody"];
            _loc_2.twitterInfo.noCharityBody = param1["twitter"]["noCharityBody"];
            _loc_2.bitlyURL = param1["bitly"]["postURL"];
            return _loc_2;
        }// end function

    }
}
