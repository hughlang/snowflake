﻿package model
{

    public class FlashVars extends Object
    {
        public var main_file:String;
        public var assets:String;
        public var xml_dir:String;
        public var tracking_file:String;
        public var snowflakeID:String;
        public var root_dir:String;
        public var config_file:String;

        public function FlashVars()
        {
            return;
        }// end function

    }
}
