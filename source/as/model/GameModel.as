package model
{
	import com.creativebottle.starlingmvc.binding.Bindings;

	public class GameModel
	{
		public function GameModel()
		{
		}
		[Bindings]
		public var bindings:Bindings;
		
		public function set score(value:int):void
		{
			if(value != _score)
			{
				_score = value;
				
				bindings.invalidate(this, "score");
			}
		}
		
		public function get score():int
		{
			return _score;
		}
		private var _score:int;
	}
}