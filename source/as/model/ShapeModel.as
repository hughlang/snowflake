﻿package model
{
    import __AS3__.vec.*;
    
    import control.*;
    
    import events.*;
    
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import flash.utils.*;
    
    import model.info.*;
    
    import util.*;
    
    import view.shapes.*;

    public class ShapeModel extends EventDispatcher
    {
        public var glow:Number = 0;
        private var _flakeID:String;
        private var _backgroundData:BackgroundData;
        public var fillColor:Number = 16777215;
        private var _sendFlakeDataNotification:Boolean;
        private var _dm:DataModel;
        private var _loader:URLLoader;
        public var type:String = "line_art";
        public var largeBMP:Bitmap;
        public var lineColor:Number = 0xFFFFFF;
        private var _soundData:SoundData;
        private var _bitly:String;
        public var alpha:Number = 1;
        public var lineThickness:Number = 1;
        private var _flakeLikeCount:String;
        public var blur:Number = 0;
        private var _shapeData:Vector.<ShapeData>;
        private var _messageLoader:URLLoader;
		public static const LINE_ART:String = "line_art";
        public static const SMOOTH_LINE:String = "smooth_line";
        private static var inst:ShapeModel;
        public static const FREE_LINE:String = "free_line";
        public static const SQUARE_EMPTY:String = "square_empty";
        public static const CIRCLE_EMPTY:String = "circle_empty";
        public static const SQUARE_FULL:String = "square_full";
        public static const TRIANGLE_FULL:String = "triangle_full";
        public static const TRIANGLE_EMPTY:String = "triangle_empty";
        public static const CIRCLE_FULL:String = "circle_full";

        public function ShapeModel()
        {
            this._shapeData = new Vector.<ShapeData>;
            this._soundData = new SoundData();
            this._backgroundData = new BackgroundData();
            this._dm = DataModel.getInstance();
            this.addListeners();
            return;
        }// end function

        public function loadSnowFlakeMessage() : void
        {
            var _loc_2:* = null;
            trace("Loading snow flake message...");
            this.clearShapeData();
            this._messageLoader = new URLLoader();
            this._messageLoader.addEventListener(Event.COMPLETE, this.onMessageLoaded);
            this._messageLoader.addEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            var _loc_1:* = this._dm.flakeInfo;
            this._flakeID = _loc_1.id;
            if (_loc_1.useCDN)
            {
                _loc_2 = DataModel.appData.cdnURL + DataModel.appData.cdnXMLPath + DataModel.appData.flakePrefix + _loc_1.id + DataModel.appData.flakeSuffix;
            }
            else
            {
                _loc_2 = DataModel.appData.originURL + DataModel.appData.loadFile + "?" + DataModel.appData.flakeParameterName + "=" + _loc_1.id;
            }
            this._messageLoader.load(new URLRequest(_loc_2));
            return;
        }// end function

        public function undoLastStep() : void
        {
            this._shapeData.pop();
            return;
        }// end function

        function securityErrorHandler(event:SecurityErrorEvent) : void
        {
            trace("securityErrorHandler:" + event);
            return;
        }// end function

        public function sendSnowFlake(param1:Sprite) : void
        {
            var prop:String;
            var tempString:String;
            var flake_mc:* = param1;
            trace("Sending snow flake...");
            var url:* = DataModel.appData.saveFile;
            var request:* = new URLRequest(url);
            var requestVars:* = new URLVariables();
            requestVars.from_name = this._dm.FROM_NAME;
            requestVars.from_location = this._dm.FROM_LOCATION;
            requestVars.message = this._dm.USER_MESSAGE;
            requestVars.from_email = this._dm.FROM_USER_EMAIL;
            requestVars.to_email1 = this._dm.FROM_EMAIL1;
            requestVars.to_name1 = this._dm.FROM_NAME1;
            requestVars.to_email2 = this._dm.FROM_EMAIL2;
            requestVars.to_name2 = this._dm.FROM_NAME2;
            requestVars.to_email3 = this._dm.FROM_EMAIL3;
            requestVars.to_name3 = this._dm.FROM_NAME3;
            requestVars.shapeLength = this._shapeData.length;
            var pngStream:* = ImageGenerator.getJpgString(flake_mc);
            var jpgStream:* = ImageGenerator.getJpgStringFromBMP(this.largeBMP);
            requestVars.image = pngStream;
            requestVars.imageLarge = jpgStream;
            var i:Number;
            while (i < this._shapeData.length)
            {
                
                tempString;
                tempString = tempString + (this._shapeData[i].type + "|");
                tempString = tempString + (this._shapeData[i].lineThickness + "|");
                tempString = tempString + (this._shapeData[i].lineColor + "|");
                tempString = tempString + (this._shapeData[i].fillColor + "|");
                tempString = tempString + (this._shapeData[i].alpha + "|");
                tempString = tempString + (this._shapeData[i].blur + "|");
                tempString = tempString + (this._shapeData[i].glow + "|");
                tempString = tempString + (this._shapeData[i].data + "|");
                requestVars["shapeVars" + i] = tempString;
                i = (i + 1);
            }
            requestVars.sound = this._soundData.sequence;
            requestVars.background = this._backgroundData.jason;
            request.data = requestVars;
            request.method = URLRequestMethod.POST;
            var urlLoader:* = new URLLoader();
            urlLoader = new URLLoader();
            urlLoader.addEventListener(Event.COMPLETE, this.saveCompleteHandler, false, 0, true);
            urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, this.httpStatusHandler, false, 0, true);
            urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, this.securityErrorHandler, false, 0, true);
            urlLoader.addEventListener(IOErrorEvent.IO_ERROR, this.ioErrorHandler, false, 0, true);
            var _loc_3:* = 0;
            var _loc_4:* = requestVars;
            while (_loc_4 in _loc_3)
            {
                
                prop = _loc_4[_loc_3];
            }
            try
            {
                urlLoader.load(request);
            }
            catch (e:Error)
            {
                trace(e);
            }
            return;
        }// end function

        private function onDataLoaded(event:Event) : void
        {
            this._loader.removeEventListener(Event.COMPLETE, this.onDataLoaded);
            this._loader.removeEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            trace("snow flake data loaded...");
            this._sendFlakeDataNotification = true;
            XML.ignoreWhitespace = true;
            var _loc_2:* = new XML(event.target["data"]);
            this.parseFlakeData(_loc_2);
            return;
        }// end function

        public function get sound() : SoundData
        {
            return this._soundData;
        }// end function

        private function loadBitly() : void
        {
            var _loc_2:* = null;
            trace("loading bit.ly...");
            this._loader = new URLLoader();
            this._loader.addEventListener(Event.COMPLETE, this.onBitlyLoaded);
            this._loader.addEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            var _loc_1:* = "http://www.flurrious.com/?snowflakeID=" + this._flakeID;
            _loc_1 = escape(_loc_1);
            _loc_2 = DataModel.appData.bitlyURL + "login=" + DataModel.BITLY_LOGIN + "&apiKey=" + DataModel.BITLY_KEY + "&format=xml&longUrl=" + _loc_1;
            this._loader.load(new URLRequest(_loc_2));
            return;
        }// end function

        public function saveToDesktop(param1:Sprite) : void
        {
            var _loc_2:* = ImageGenerator.getJpgStream(param1);
            var _loc_3:* = new URLRequestHeader("Content-type", "application/octet-stream");
            var _loc_4:* = new URLRequest(DataModel.appData.sendImageFile);
            _loc_4.requestHeaders.push(_loc_3);
            var _loc_5:* = URLRequestMethod.POST;
            _loc_4.method = URLRequestMethod.POST;
            var _loc_5:* = _loc_2;
            _loc_4.data = _loc_2;
            navigateToURL(_loc_4, "_blank");
            return;
        }// end function

        public function clearShapeData() : void
        {
            var _loc_1:* = this._shapeData.length - 1;
            while (_loc_1 >= 0)
            {
                
                this._shapeData.pop();
                _loc_1 = _loc_1 - 1;
            }
            return;
        }// end function

        private function onBitlyLoaded(event:Event) : void
        {
            trace("bit.ly loaded...");
            this._loader.removeEventListener(Event.COMPLETE, this.onBitlyLoaded);
            this._loader.removeEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            var _loc_2:* = XML(event.target["data"]);
            this._bitly = unescape(_loc_2["data"]["url"]);
            this._dm.flakeInfo.bitly = this._bitly;
            trace("shortURL:", this._bitly, " | longURL:", unescape(_loc_2["data"]["long_url"]));
            if (this._sendFlakeDataNotification)
            {
                EventController.getInstance().dispatchEvent(new SnowFlakeEvent(SnowFlakeEvent.GENERATE_FLAKE));
                EventController.getInstance().dispatchEvent(new SnowFlakeEvent(SnowFlakeEvent.GENERATE_FLAKE_AUDIO));
                EventController.getInstance().dispatchEvent(new SnowFlakeEvent(SnowFlakeEvent.GENERATE_FLAKE_BACKGROUND));
                this._sendFlakeDataNotification = false;
            }
            return;
        }// end function

        public function saveShape(param1:AbstractShape) : void
        {
            var data:ShapeData = new ShapeData();
            data.type = this.type;
            data.lineColor = this.lineColor;
            data.fillColor = this.fillColor;
            data.lineThickness = this.lineThickness;
            data.alpha = this.alpha;
            data.blur = this.blur;
            data.glow = this.glow;
            data.data = param1.shapeData;
            this._shapeData.push(data);
            return;
        }// end function

        function saveCompleteHandler(event:Event) : void
        {
            var _loc_3:* = false;
            this._sendFlakeDataNotification = false;
            var _loc_2:* = XML(event.target["data"]);
            this.parseFlakeData(_loc_2);
            return;
        }// end function

        public function set sound(param1:SoundData) : void
        {
            var _loc_2:* = param1;
            this._soundData = param1;
            return;
        }// end function

        public function get getShapeData() : Vector.<ShapeData>
        {
            return this._shapeData;
        }// end function

        private function onLikeLoaded(event:Event) : void
        {
            trace("likes loaded...");
            this._loader.removeEventListener(Event.COMPLETE, this.onLikeLoaded);
            this._loader.removeEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            this._flakeLikeCount = String(event.target["data"]);
            this._dm.flakeInfo.likes = this._flakeLikeCount;
            this.loadBitly();
            return;
        }// end function

        public function get background() : BackgroundData
        {
            return this._backgroundData;
        }// end function

        public function get flakeLikeCount() : String
        {
            return this._flakeLikeCount;
        }// end function

        private function addListeners() : void
        {
            EventController.getInstance().addEventListener(SliderEvent.CHANGE_LINE_WIDTH, this.onChangeParam);
            EventController.getInstance().addEventListener(SliderEvent.CHANGE_ALPHA, this.onChangeParam);
            EventController.getInstance().addEventListener(SliderEvent.CHANGE_BLUR, this.onChangeParam);
            EventController.getInstance().addEventListener(SliderEvent.CHANGE_GLOW, this.onChangeParam);
            return;
        }// end function

        public function resetTools() : void
        {
            this.type = FREE_LINE;
            return;
        }// end function

        private function onMessageLoaded(event:Event) : void
        {
            this._messageLoader.removeEventListener(Event.COMPLETE, this.onMessageLoaded);
            this._messageLoader.removeEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            trace("snow flake message loaded...");
            XML.ignoreWhitespace = true;
            var _loc_2:* = XML(event.target["data"]);
            this._dm.flakeInfo.id = this._flakeID;
            this._dm.flakeInfo.message = _loc_2["message"];
            this._dm.flakeInfo.name = _loc_2["my_name"];
            this._dm.flakeInfo.location = _loc_2["location"];
            EventController.getInstance().dispatchEvent(new SnowFlakeEvent(SnowFlakeEvent.DISPLAY_MESSAGE));
            return;
        }// end function

        private function parseFlakeData(param1:XML) : void
        {
            var _loc_6:* = null;
            var _loc_7:* = null;
            var _loc_8:* = null;
            var _loc_9:* = null;
            var _loc_10:* = NaN;
            trace("parsing snow flake data...");
            this._flakeID = param1.@id;
            var _loc_2:* = param1.shapeVars.children();
            var _loc_3:* = 0;
            while (_loc_3 < _loc_2.length())
            {
                
                _loc_6 = _loc_2[_loc_3].split("|");
                _loc_7 = new ShapeData();
                _loc_7.type = _loc_6[0];
                _loc_7.lineThickness = _loc_6[1];
                _loc_7.lineColor = _loc_6[2];
                _loc_7.fillColor = _loc_6[3];
                _loc_7.alpha = _loc_6[4];
                _loc_7.blur = _loc_6[5];
                _loc_7.glow = _loc_6[6];
                _loc_8 = new Vector.<Number>;
                _loc_9 = String(_loc_6[7]).split(",");
                _loc_10 = 0;
                while (_loc_10 < _loc_9.length)
                {
                    
                    _loc_8.push(_loc_9[_loc_10]);
                    _loc_10 = _loc_10 + 1;
                }
                _loc_7.data = _loc_8;
                this._shapeData.push(_loc_7);
                _loc_3 = _loc_3 + 1;
            }
            this._soundData = new SoundData();
            var _loc_4:* = param1.sound;
            if (_loc_4 && _loc_4 != "")
            {
                this._soundData.sequence = _loc_4;
            }
            this._backgroundData = new BackgroundData();
            var _loc_5:* = param1.background;
            if (_loc_5 && _loc_5 != "")
            {
                this._backgroundData.jason = _loc_5;
            }
            this._dm.flakeInfo.id = this._flakeID;
            this._dm.flakeInfo.likes = this._flakeLikeCount;
            this._dm.flakeInfo.message = param1["message"];
            this._dm.flakeInfo.name = param1["my_name"];
            this._dm.flakeInfo.location = param1["location"];
            this.loadBitly();
            return;
        }// end function

        function httpStatusHandler(event:HTTPStatusEvent) : void
        {
            return;
        }// end function

        public function set background(param1:BackgroundData) : void
        {
            var _loc_2:* = param1;
            this._backgroundData = param1;
            return;
        }// end function

        public function updateShape() : void
        {
            if (this._shapeData.length > 0)
            {
                this._shapeData[(this._shapeData.length - 1)].lineThickness = this.lineThickness;
                this._shapeData[(this._shapeData.length - 1)].alpha = this.alpha;
                this._shapeData[(this._shapeData.length - 1)].blur = this.blur;
                this._shapeData[(this._shapeData.length - 1)].glow = this.glow;
            }
            return;
        }// end function

        private function errorLoadingData(event:IOErrorEvent) : void
        {
            dispatchEvent(new ApplicationEvent(ApplicationEvent.DISPLAY_ERROR, event.text));
            return;
        }// end function

        function ioErrorHandler(event:IOErrorEvent) : void
        {
            trace("ShapeModel:ioErrorHandler: " + event);
            return;
        }// end function

        public function loadSnowFlake() : void
        {
            var _loc_2:* = null;
            trace("Loading snow flake data...");
            this.clearShapeData();
            this._loader = new URLLoader();
            this._loader.addEventListener(Event.COMPLETE, this.onDataLoaded);
            this._loader.addEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            var _loc_1:* = this._dm.flakeInfo;
            this._flakeID = _loc_1.id;
            if (_loc_1.useCDN)
            {
                _loc_2 = DataModel.appData.cdnURL + DataModel.appData.cdnXMLPath + DataModel.appData.flakePrefix + _loc_1.id + DataModel.appData.flakeSuffix;
            }
            else
            {
                _loc_2 = DataModel.appData.originURL + DataModel.appData.loadFile + "?" + DataModel.appData.flakeParameterName + "=" + _loc_1.id;
            }
            this._loader.load(new URLRequest(_loc_2));
            trace(_loc_2);
            return;
        }// end function

        private function onChangeParam(event:SliderEvent) : void
        {
			var sliderValue:Number;
            switch(String(event.type))
            {
                case SliderEvent.CHANGE_LINE_WIDTH:
                {
                    sliderValue = Math.round(Number(event.data));
                    this.lineThickness = Math.round(Number(event.data));
                    break;
                }
                case SliderEvent.CHANGE_ALPHA:
                {
                    sliderValue = Number(event.data);
                    this.alpha = Number(event.data);
                    break;
                }
                case SliderEvent.CHANGE_BLUR:
                {
                    sliderValue = Number(event.data);
                    this.blur = Number(event.data);
                    break;
                }
                case SliderEvent.CHANGE_GLOW:
                {
                    sliderValue = Number(event.data);
                    this.glow = Number(event.data);
                    break;
                }
                default:
                {
                    break;
                }
            }
            return;
        }// end function

        public function get flakeID() : String
        {
            return this._flakeID;
        }// end function

        private function loadLikes() : void
        {
            var _loc_1:* = null;
            trace("loading likes...");
            this._loader = new URLLoader();
            this._loader.addEventListener(Event.COMPLETE, this.onLikeLoaded);
            this._loader.addEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingData);
            _loc_1 = DataModel.appData.loadLikes + "?" + DataModel.appData.flakeParameterName + "=" + this._flakeID;
            this._loader.load(new URLRequest(_loc_1));
            return;
        }// end function

        public static function getInstance() : ShapeModel
        {
            if (inst == null)
            {
                inst = new ShapeModel;
            }
            return inst;
        }// end function

    }
}
