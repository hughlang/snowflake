﻿package model
{
    import events.*;
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import model.info.*;

    public class DataModel extends EventDispatcher
    {
        public var FROM_NAME:String;
        private var _loader:URLLoader;
        public var FROM_NAME3:String;
        public var flakeInfo:SnowFallFlakeInfo;
        private var _cntr:Number = 0;
        public var MAIN_MOVIECLIP:MovieClip;
        public var MODE:String;
        public var FROM_LOCATION:String;
        public var FROM_EMAIL2:String;
        public var FROM_EMAIL3:String;
        public var FROM_NAME1:String;
        public var FROM_NAME2:String;
        public var USER_MESSAGE:String;
        private const _loadingMethods:Array;
        public var FROM_EMAIL1:String;
        public var FROM_USER_EMAIL:String;
        public static var flakeX:Number;
        public static const BITLY_LOGIN:String = "flurrious";
        public static var flakeY:Number;
        public static var state:String = "closed";
        public static const SCREEN_VIEW:String = "screen_view";
        public static var trackInfo:Object;
        public static const CIRCLE_Y_POINT:Number = 300;
        public static var initScreen:String = "screen_intro";
        public static var XML_DIR:String = "xml/";
        public static const IMAGE_HEIGHT:int = 400;
        public static const SCREEN_CREATE:String = "screen_create";
        public static const IMAGE_WIDTH:int = 560;
        public static var CURRENT_PAGE:String = "";
        public static var ALLOWDOWNLOAD:Boolean = true;
        public static const SCREEN_INTRO:String = "screen_intro";
        public static const BITLY_KEY:String = "R_0a18371ee4f7398cd1bf210eb9bbd3cd";
        public static var APPLICATIONNAME:String = "";
        public static var APPDATAURL:String = "config.xml";
        private static var inst:DataModel;
        public static var MAINAPPPATH:String = "";
        public static const OPEN:String = "open";
        public static const TILT_DURATION:Number = 3;
        public static var appData:ApplicationInfo;
        public static const CLOSED:String = "closed";
        public static const SCREEN_SNOWFALL:String = "screen_snowfall";

        public function DataModel() : void
        {
//            this._loadingMethods = ["onApplicationDataLoaded"];
            this.flakeInfo = new SnowFallFlakeInfo();
            return;
        }// end function

        private function onApplicationDataLoaded(event:Event) : void
        {
            XML.ignoreWhitespace = true;
            var _loc_2:* = new XML(event.target["data"]);
            appData = ApplicationInfo.parseInfo(_loc_2);
            this._loader.removeEventListener(Event.COMPLETE, this.onApplicationDataLoaded);
            this.applicationDataLoaded();
            return;
        }// end function

        public function postToFacebook() : void
        {
            var url:* = appData.facebookInfo.postURL;
            var title:* = appData.charityActive ? (appData.facebookInfo.charityTitle) : (appData.facebookInfo.noCharityTitle);
            var body:* = appData.charityActive ? (appData.facebookInfo.charityBody) : (appData.facebookInfo.noCharityBody);
            var pattern:* = /|url_token|""|url_token|/;
            body = body.replace(pattern, this.flakeInfo.bitly);
            url = url + (appData.facebookInfo.titleParameterName + "=" + title);
            url = url + ("&" + appData.facebookInfo.summaryParameterName + "=" + body);
            url = url + ("&" + appData.facebookInfo.urlParameterName + "=" + this.flakeInfo.bitly);
            trace("postToFacebook::", url);
            var req:* = new URLRequest(url);
            var window:String;
            try
            {
                navigateToURL(req, window);
            }
            catch (e:Error)
            {
                trace("Navigate to URL failed " + e.getStackTrace());
            }
            return;
        }// end function

        public function loadApplicationConfigurationFile() : void
        {
            this._loader = new URLLoader();
            this._loader.addEventListener(Event.COMPLETE, this.onApplicationDataLoaded);
            this._loader.addEventListener(IOErrorEvent.IO_ERROR, this.errorLoadingAppData);
            this._loader.load(new URLRequest(XML_DIR + APPDATAURL));
            return;
        }// end function

        private function errorLoadingAppData(event:ErrorEvent) : void
        {
            dispatchEvent(new ApplicationEvent(ApplicationEvent.DISPLAY_ERROR, event.text));
            return;
        }// end function

        public function postToTwitter() : void
        {
            var url:* = appData.twitterInfo.postURL;
            var body:* = appData.charityActive ? (appData.twitterInfo.charityBody) : (appData.twitterInfo.noCharityBody);
            var pattern:* = /|url_token|""|url_token|/;
            body = body.replace(pattern, this.flakeInfo.bitly);
            url = url + body;
            trace("postToTwitter::", url);
            var req:* = new URLRequest(url);
            var window:String;
            try
            {
                navigateToURL(req, window);
            }
            catch (e:Error)
            {
                trace("Navigate to URL failed " + e.getStackTrace());
            }
            return;
        }// end function

        private function applicationDataLoaded() : void
        {
            var _loc_1:* = this;
            var _loc_2:* = this._cntr + 1;
            _loc_1._cntr = _loc_2;
            if (this._cntr == this._loadingMethods.length)
            {
                dispatchEvent(new ApplicationEvent(ApplicationEvent.APP_DATA_LOADED));
            }
            return;
        }// end function

        public static function getInstance() : DataModel
        {
            if (inst == null)
            {
                inst = new DataModel;
            }
            return inst;
        }// end function

    }
}
