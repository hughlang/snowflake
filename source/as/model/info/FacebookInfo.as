﻿package model.info
{

    public class FacebookInfo extends Object
    {
        public var charityTitle:String;
        public var noCharityTitle:String;
        public var urlParameterName:String;
        public var summaryParameterName:String;
        public var postURL:String;
        public var noCharityBody:String;
        public var charityBody:String;
        public var titleParameterName:String;

        public function FacebookInfo()
        {
            return;
        }// end function

    }
}
