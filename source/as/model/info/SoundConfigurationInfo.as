﻿package model.info
{

    public class SoundConfigurationInfo extends Object
    {
        public var time_sig:int;
        public var scale:int;
        public var note_length:int;
        public var audioSequence:Array;
        public var note_max_tonality:int;
        public var bar_num:int;
        public var BPM:int;

        public function SoundConfigurationInfo()
        {
            return;
        }// end function

    }
}
