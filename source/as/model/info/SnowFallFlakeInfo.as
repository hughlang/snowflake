﻿package model.info
{

    public class SnowFallFlakeInfo extends Object
    {
        public var id:String;
        public var useCDN:Boolean;
        public var message:String;
        public var name:String;
        public var location:String;
        public var bitly:String;
        public var image:String;
        public var likes:String;

        public function SnowFallFlakeInfo()
        {
            this.reset();
            return;
        }// end function

        public function reset() : void
        {
            this.image = "";
            this.name = "";
            this.location = "";
            this.message = "";
            this.id = "";
            this.useCDN = false;
            this.likes = "";
            this.bitly = "";
            return;
        }// end function

    }
}
