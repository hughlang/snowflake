﻿package model
{
    import view.shapes.*;

    public class ShapeFactory extends Object
    {

        public function ShapeFactory()
        {
            return;
        }// end function

        public static function generateShape(type:String) : AbstractShape
        {
            var shape:AbstractShape = null;
            switch(type)
            {
				case ShapeModel.LINE_ART:
				{
					shape = new LineArt();
					break;
				}
                case ShapeModel.SMOOTH_LINE:
                {
                    shape = new SmoothLine();
                    break;
                }
                case ShapeModel.FREE_LINE:
                {
                    shape = new FreeLine();
                    break;
                }
                case ShapeModel.CIRCLE_EMPTY:
                {
                    shape = new CircleEmpty();
                    break;
                }
                case ShapeModel.CIRCLE_FULL:
                {
                    shape = new CircleFull();
                    break;
                }
                case ShapeModel.SQUARE_EMPTY:
                {
                    shape = new SquareEmpty();
                    break;
                }
                case ShapeModel.SQUARE_FULL:
                {
                    shape = new SquareFull();
                    break;
                }
                case ShapeModel.TRIANGLE_EMPTY:
                {
                    shape = new TriangleEmpty();
                    break;
                }
                case ShapeModel.TRIANGLE_FULL:
                {
                    shape = new TriangleFull();
                    break;
                }
                default:
                {
                    break;
                }
            }
            return shape;
        }// end function

    }
}
