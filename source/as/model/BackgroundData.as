﻿package model
{

    public class BackgroundData extends Object
    {
        public var starFieldBrightness:Number;
        public var nebulaeAlpha:Number;
        public var starsAmount:int;
        public var jason:String;
        public var starFieldAlpha:Number;
        public var spaceAlpha:Number;

        public function BackgroundData()
        {
            return;
        }// end function

    }
}
