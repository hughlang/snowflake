﻿package model
{

    public class ShapeData extends Object
    {
        public var lineThickness:Number;
        public var fillColor:Number;
        public var alpha:Number;
        public var data:Object;
        public var glow:Number;
        public var lineColor:Number;
        public var blur:Number;
        public var type:String;

        public function ShapeData()
        {
            return;
        }// end function

    }
}
