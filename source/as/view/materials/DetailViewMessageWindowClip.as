﻿package view.materials
{
    import flash.display.*;
    import flash.text.*;

    dynamic public class DetailViewMessageWindowClip extends MovieClip
    {
        public var emailBtn:MovieClip;
        public var likeBtn:MovieClip;
        public var fbBtn:MovieClip;
        public var playBtn:MovieClip;
        public var twitterBtn:MovieClip;
        public var creator:TextField;
        public var url:TextField;
        public var location:TextField;
        public var likeCount:TextField;

        public function DetailViewMessageWindowClip()
        {
            return;
        }// end function

    }
}
