﻿package view.background
{
    import __AS3__.vec.*;
    import com.greensock.*;
    import control.*;
    import events.*;
    import flash.display.*;
    import view.materials.background.*;

    public class StarField extends Sprite
    {
        public var currentAmount:int;
        private var _firstRun:Boolean;
        private var _star1:Star1;
        private var _star2:Star2;
        private var _starArray:Vector.<Bitmap>;
        private var _h:Number;
        private var _typeOfStars:int;
        private var _mc:MovieClip;
        private var _amountStar2:int;
        private var _newAmount:int;
        private var _initialAmount:int;
        private var _w:Number;

        public function StarField(param1:MovieClip)
        {
            this._mc = param1;
            this._w = this._mc.stage.stageWidth;
            this._h = this._mc.stage.stageHeight;
            this._initialAmount = 1000;
            this._typeOfStars = 2;
            this._amountStar2 = Math.ceil(this._initialAmount * 0.004);
            this._firstRun = true;
            this._star1 = new Star1(29, 24);
            this._star2 = new Star2(84, 75);
            EventController.getInstance().addEventListener(SliderEvent.ADD_STARS, this.onAddStars);
            this.initStars();
            return;
        }// end function

        public function resize() : void
        {
            this._w = this._mc.stage.stageWidth;
            this._h = this._mc.stage.stageHeight;
            var _loc_1:* = 0;
            while (_loc_1 < this._starArray.length)
            {
                
                this._starArray[_loc_1].x = Math.random() * this._w;
                this._starArray[_loc_1].y = Math.random() * this._h;
                _loc_1++;
            }
            return;
        }// end function

        private function onAddStars(event:SliderEvent) : void
        {
            if (!this._firstRun)
            {
                this._newAmount = Math.round(Number(event.data));
                if (this._newAmount < this.currentAmount)
                {
                    this.fadeOutStar(this.currentAmount - this._newAmount);
                }
                if (this._newAmount > this.currentAmount)
                {
                    this.addStars(this._newAmount - this.currentAmount);
                }
            }
            else
            {
                this._firstRun = false;
            }
            return;
        }// end function

        private function addStars(param1:int) : void
        {
            var _loc_2:* = 0;
            var _loc_3:* = null;
            this._amountStar2 = Math.ceil(param1 * 0.004);
            var _loc_4:* = 0;
            while (_loc_4 < param1)
            {
                
                if (_loc_4 < param1 - this._amountStar2)
                {
                    _loc_2 = 1;
                }
                else
                {
                    _loc_2 = 2;
                }
                switch(_loc_2)
                {
                    case 1:
                    {
                        _loc_3 = new Bitmap(this._star1);
                        _loc_3.alpha = 0;
                        var _loc_5:* = 1 - Math.random() * 0.8;
                        _loc_3.scaleY = 1 - Math.random() * 0.8;
                        _loc_3.scaleX = _loc_5;
                        _loc_3.x = Math.random() * this._w;
                        _loc_3.y = Math.random() * this._h;
                        addChild(_loc_3);
                        this.fadeInStar(_loc_3, 0.7);
                        break;
                    }
                    case 2:
                    {
                        _loc_3 = new Bitmap(this._star2);
                        _loc_3.alpha = 0;
                        var _loc_5:* = 1 - Math.random() * 0.3;
                        _loc_3.scaleY = 1 - Math.random() * 0.3;
                        _loc_3.scaleX = _loc_5;
                        _loc_3.x = Math.random() * this._w;
                        _loc_3.y = Math.random() * this._h;
                        addChild(_loc_3);
                        this.fadeInStar(_loc_3, 0.4);
                        break;
                    }
                    default:
                    {
                        break;
                        break;
                    }
                }
                this._starArray.push(_loc_3);
                _loc_4 = _loc_4 + 1;
            }
            this.currentAmount = this._starArray.length;
            return;
        }// end function

        public function fadeInStar(param1:Bitmap, param2:Number) : void
        {
            var _loc_3:* = 1 - Math.random() * param2;
            TweenLite.to(param1, 0.5, {alpha:_loc_3});
            return;
        }// end function

        public function restore(param1:int) : void
        {
            var _loc_2:* = 0;
            var _loc_3:* = null;
            this.fadeOutStar(this.currentAmount);
            this._amountStar2 = Math.ceil(param1 * 0.004);
            var _loc_4:* = 0;
            while (_loc_4 < param1)
            {
                
                if (_loc_4 < param1 - this._amountStar2)
                {
                    _loc_2 = 1;
                }
                else
                {
                    _loc_2 = 2;
                }
                switch(_loc_2)
                {
                    case 1:
                    {
                        _loc_3 = new Bitmap(this._star1);
                        _loc_3.alpha = 1 - Math.random() * 0.7;
                        var _loc_5:* = 1 - Math.random() * 0.8;
                        _loc_3.scaleY = 1 - Math.random() * 0.8;
                        _loc_3.scaleX = _loc_5;
                        break;
                    }
                    case 2:
                    {
                        _loc_3 = new Bitmap(this._star2);
                        var _loc_5:* = 1 - Math.random() * 0.3;
                        _loc_3.scaleY = 1 - Math.random() * 0.3;
                        _loc_3.scaleX = _loc_5;
                        _loc_3.alpha = 1 - Math.random() * 0.3;
                        break;
                    }
                    default:
                    {
                        break;
                        break;
                    }
                }
                _loc_3.x = Math.random() * this._w;
                _loc_3.y = Math.random() * this._h;
                addChild(_loc_3);
                this._starArray.push(_loc_3);
                _loc_4 = _loc_4 + 1;
            }
            this.currentAmount = this._starArray.length;
            return;
        }// end function

        private function initStars() : void
        {
            var _loc_1:* = 0;
            var _loc_2:* = null;
            this._starArray = new Vector.<Bitmap>;
            var _loc_3:* = 0;
            while (_loc_3 < this._initialAmount)
            {
                
                if (_loc_3 < this._initialAmount - this._amountStar2)
                {
                    _loc_1 = 1;
                }
                else
                {
                    _loc_1 = 2;
                }
                switch(_loc_1)
                {
                    case 1:
                    {
                        _loc_2 = new Bitmap(this._star1);
                        _loc_2.alpha = 1 - Math.random() * 0.7;
                        var _loc_4:* = 1 - Math.random() * 0.8;
                        _loc_2.scaleY = 1 - Math.random() * 0.8;
                        _loc_2.scaleX = _loc_4;
                        break;
                    }
                    case 2:
                    {
                        _loc_2 = new Bitmap(this._star2);
                        var _loc_4:* = 1 - Math.random() * 0.3;
                        _loc_2.scaleY = 1 - Math.random() * 0.3;
                        _loc_2.scaleX = _loc_4;
                        _loc_2.alpha = 1 - Math.random() * 0.3;
                        break;
                    }
                    default:
                    {
                        break;
                        break;
                    }
                }
                _loc_2.x = Math.random() * this._w;
                _loc_2.y = Math.random() * this._h * 2;
                addChild(_loc_2);
                this._starArray.push(_loc_2);
                _loc_3++;
            }
            this.currentAmount = this._starArray.length;
            return;
        }// end function

        public function fadeOutStar(param1:int) : void
        {
            var _loc_2:* = null;
            var _loc_3:* = 0;
            while (_loc_3 < param1)
            {
                
                _loc_2 = this._starArray.pop();
                TweenLite.to(_loc_2, 0.3, {alpha:0, onComplete:this.deleteStars, onCompleteParams:[_loc_2]});
                _loc_3++;
            }
            this.currentAmount = this._starArray.length;
            return;
        }// end function

        public function removeExtra() : void
        {
            var _loc_1:* = 0;
            while (_loc_1 < this._starArray.length)
            {
                
                if (this._starArray[_loc_1].y > this._mc.height)
                {
                    removeChild(this._starArray[_loc_1]);
                    this._starArray.splice(_loc_1, 1);
                }
                _loc_1++;
            }
            return;
        }// end function

        private function deleteStars(param1:Bitmap) : void
        {
            removeChild(param1);
            return;
        }// end function

    }
}
