﻿package view.controls
{
    import control.*;
    import events.*;
	import graphics.*;
    import flash.display.*;
    import gs.*;

    public class SliderView extends Sprite
    {
        public var slider3Pos:Number = 100;
        private var _iconSpeaker:IconSpeaker;
        private var _scaleSlider:CircSlider;
        public var slider6Pos:Number = 100;
        private var _bpmSlider:CircSlider;
        public var slider2Pos:Number = 100;
        private var _flakeView:Sprite;
        private var _ec:EventController;
        public var slider5Pos:Number = 100;
        public var slider1Pos:Number = 100;
        private var _barSlider:CircSlider;
        private var _noteLengthSlider:CircSlider;
        private var _icon1:IconLineWeigt;
        private var _icon2:IconAlpha;
        private var _icon3:IconBlur;
        private var _icon4:IconGlow;
        private var _icon5:IconNebulae;
        private var _icon6:IconStar;
        private var _icon7:IconStar;
        public var slider4Pos:Number = 100;
        private var _iconBPM:IconBPM;
        private var _musicView:Sprite;
        private var slider1:CircSlider;
        private var slider2:CircSlider;
        private var slider3:CircSlider;
        private var slider4:CircSlider;
        private var slider5:CircSlider;
        private var slider6:CircSlider;
        private var _iconWave:IconWave;
        private var _iconNote:IconNote;
        private var _volumeSlider:CircSlider;
        private var slider7:CircSlider;
        public var slider7Pos:Number = 100;

        public function SliderView()
        {
            this.init();
            return;
        }// end function

        private function bpmSliderListener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_BPM, Number(event.data)));
            return;
        }// end function

        private function initFlakeListeners() : void
        {
            this.slider1.addEventListener(SliderEvent.POSITION, this.slider1Listener);
            this.slider2.addEventListener(SliderEvent.POSITION, this.slider2Listener);
            this.slider3.addEventListener(SliderEvent.POSITION, this.slider3Listener);
            this.slider4.addEventListener(SliderEvent.POSITION, this.slider4Listener);
            this.slider5.addEventListener(SliderEvent.POSITION, this.slider5Listener);
            this.slider6.addEventListener(SliderEvent.POSITION, this.slider6Listener);
            this.slider7.addEventListener(SliderEvent.POSITION, this.slider7Listener);
            return;
        }// end function

        private function initFlakeView() : void
        {
            var _loc_1:* = 15;
            this.slider1 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this.slider1.x = 90;
            this._icon1 = new IconLineWeigt();
            this._icon1.x = this.slider1.getStartPoint().x + this.slider1.x - this._icon1.width / 2;
            this._icon1.y = this.slider1.getStartPoint().y - _loc_1 - this._icon1.height;
            this._flakeView.addChild(this._icon1);
            this.slider2 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this.slider2.x = 120;
            this._icon2 = new IconAlpha();
            this._icon2.x = this.slider2.getStartPoint().x + this.slider2.x - this._icon2.width / 2;
            this._icon2.y = this.slider2.getStartPoint().y - _loc_1 - this._icon2.height;
            this._flakeView.addChild(this._icon2);
            this.slider3 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this.slider3.x = 150;
            this._icon3 = new IconBlur();
            this._icon3.x = this.slider3.getStartPoint().x + this.slider3.x - this._icon3.width / 2;
            this._icon3.y = this.slider3.getStartPoint().y - _loc_1 - this._icon3.height;
            this._flakeView.addChild(this._icon3);
            this.slider4 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this.slider4.x = 180;
            this._icon4 = new IconGlow();
            this._icon4.x = this.slider4.getStartPoint().x + this.slider4.x - this._icon4.width / 2;
            this._icon4.y = this.slider4.getStartPoint().y - _loc_1 - this._icon4.height;
            this._flakeView.addChild(this._icon4);
            this.slider5 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRightBlueBlack");
            this.slider5.x = -30;
            this.slider5.scaleX = -1;
            this._icon5 = new IconNebulae();
            this._icon5.x = -this.slider5.getStartPoint().x + this.slider5.x - this._icon5.width / 2;
            this._icon5.y = this.slider5.getStartPoint().y - _loc_1 - this._icon5.height;
            this._flakeView.addChild(this._icon5);
            this.slider6 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this.slider6.x = -60;
            this.slider6.scaleX = -1;
            this._icon6 = new IconStar();
            this._icon6.x = -this.slider6.getStartPoint().x + this.slider6.x - this._icon6.width / 2;
            this._icon6.y = this.slider6.getStartPoint().y - _loc_1 - this._icon6.height;
            this._flakeView.addChild(this._icon6);
            this.slider7 = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRightWhiteBlack");
            this.slider7.x = -90;
            this.slider7.scaleX = -1;
            this._icon7 = new IconStar();
            this._icon7.x = -this.slider7.getStartPoint().x + this.slider7.x - this._icon7.width / 2;
            this._icon7.y = this.slider7.getStartPoint().y - _loc_1 - this._icon7.height;
            this._flakeView.addChild(this._icon7);
            this._flakeView.addChild(this.slider1);
            this._flakeView.addChild(this.slider2);
            this._flakeView.addChild(this.slider3);
            this._flakeView.addChild(this.slider4);
            this._flakeView.addChild(this.slider5);
            this._flakeView.addChild(this.slider6);
            this._flakeView.addChild(this.slider7);
            this.initFlakeListeners();
            this.slider1.goto = 0.1;
            this.slider2.goto = 0.7;
            this.slider3.goto = 0.05;
            this.slider4.goto = 0.75;
            this.slider5.goto = 0.5;
            this.slider6.goto = 1;
            this.slider7.goto = 0.5;
            return;
        }// end function

        private function init() : void
        {
            this._ec = EventController.getInstance();
            this._flakeView = new Sprite();
            addChild(this._flakeView);
            this.initFlakeView();
            this._musicView = new Sprite();
            this._musicView.visible = false;
            addChild(this._musicView);
            this.initMusicView();
            return;
        }// end function

        public function destroy() : void
        {
            this.removeListeners();
            this.removeMusicListeners();
            TweenLite.killTweensOf(this);
            this.slider1.destroy();
            this.slider2.destroy();
            this.slider3.destroy();
            this.slider4.destroy();
            this.slider5.destroy();
            this.slider6.destroy();
            this.slider7.destroy();
            this._flakeView.removeChild(this.slider1);
            this._flakeView.removeChild(this.slider2);
            this._flakeView.removeChild(this.slider3);
            this._flakeView.removeChild(this.slider4);
            this._flakeView.removeChild(this.slider5);
            this._flakeView.removeChild(this.slider6);
            this._flakeView.removeChild(this.slider7);
            this._bpmSlider.destroy();
            this._noteLengthSlider.destroy();
            this._scaleSlider.destroy();
            this._volumeSlider.destroy();
            return;
        }// end function

        private function setNoteLengthSlider(event:SliderEvent) : void
        {
            this._noteLengthSlider.goto = Number(event.data);
            return;
        }// end function

        public function switchToMusic() : void
        {
            this._musicView.visible = true;
            this._flakeView.visible = false;
            return;
        }// end function

        private function removeListeners() : void
        {
            this.slider1.removeEventListener(SliderEvent.POSITION, this.slider1Listener);
            this.slider2.removeEventListener(SliderEvent.POSITION, this.slider2Listener);
            this.slider3.removeEventListener(SliderEvent.POSITION, this.slider3Listener);
            this.slider4.removeEventListener(SliderEvent.POSITION, this.slider4Listener);
            this.slider5.removeEventListener(SliderEvent.POSITION, this.slider5Listener);
            this.slider6.removeEventListener(SliderEvent.POSITION, this.slider6Listener);
            this.slider7.removeEventListener(SliderEvent.POSITION, this.slider7Listener);
            return;
        }// end function

        private function setVolumeSlider(event:SliderEvent) : void
        {
            this._volumeSlider.goto = Number(event.data);
            return;
        }// end function

        private function scaleSliderListener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_SCALE, Number(event.data)));
            return;
        }// end function

        private function setScaleSlider(event:SliderEvent) : void
        {
            this._scaleSlider.goto = Number(event.data);
            return;
        }// end function

        public function switchToDraw() : void
        {
            this._musicView.visible = false;
            this._flakeView.visible = true;
            return;
        }// end function

        private function initMusicView() : void
        {
            var _loc_1:* = 15;
            this._noteLengthSlider = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this._noteLengthSlider.x = 30;
            this._iconWave = new IconWave();
            this._iconWave.x = this._noteLengthSlider.getStartPoint().x + this._noteLengthSlider.x - this._iconWave.width / 2;
            this._iconWave.y = this._noteLengthSlider.getStartPoint().y - _loc_1 - this._iconWave.height;
            this._musicView.addChild(this._iconWave);
            this._scaleSlider = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this._scaleSlider.x = 60;
            this._iconNote = new IconNote();
            this._iconNote.x = this._scaleSlider.getStartPoint().x + this._scaleSlider.x - this._iconNote.width / 2;
            this._iconNote.y = this._scaleSlider.getStartPoint().y - _loc_1 - this._iconNote.height;
            this._musicView.addChild(this._iconNote);
            this._bpmSlider = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this._bpmSlider.x = 90;
            this._iconBPM = new IconBPM();
            this._iconBPM.x = this._bpmSlider.getStartPoint().x + this._bpmSlider.x - this._iconBPM.width / 2;
            this._iconBPM.y = this._bpmSlider.getStartPoint().y - _loc_1 - this._iconBPM.height;
            this._musicView.addChild(this._iconBPM);
            this._volumeSlider = new CircSlider(90, SnowFlakeGenerator.RADIUS, 30, "SliderArcRight");
            this._volumeSlider.x = -30;
            this._volumeSlider.scaleX = -1;
            this._iconSpeaker = new IconSpeaker();
            this._iconSpeaker.x = -this._volumeSlider.getStartPoint().x + this._volumeSlider.x - this._iconSpeaker.width / 2;
            this._iconSpeaker.y = this._volumeSlider.getStartPoint().y - _loc_1 - this._iconSpeaker.height;
            this._musicView.addChild(this._iconSpeaker);
            this._musicView.addChild(this._noteLengthSlider);
            this._musicView.addChild(this._scaleSlider);
            this._musicView.addChild(this._bpmSlider);
            this._musicView.addChild(this._volumeSlider);
            this.initMusicListeners();
            this._noteLengthSlider.goto = 0.5;
            this._scaleSlider.goto = 0.5;
            this._bpmSlider.goto = 0.5;
            this._volumeSlider.goto = 0.5;
            return;
        }// end function

        private function slider4Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_GLOW, Number(event.data) / 100));
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_FILTER));
            return;
        }// end function

        private function slider5Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.ADD_REMOVE_NEBULAE, Number(event.data) / 100));
            return;
        }// end function

        private function initMusicListeners() : void
        {
            this._noteLengthSlider.addEventListener(SliderEvent.POSITION, this.noteLengthSliderListener);
            this._scaleSlider.addEventListener(SliderEvent.POSITION, this.scaleSliderListener);
            this._bpmSlider.addEventListener(SliderEvent.POSITION, this.bpmSliderListener);
            this._volumeSlider.addEventListener(SliderEvent.POSITION, this.volumeSliderListener);
            this._ec.addEventListener(SliderEvent.SET_RELEASE, this.setNoteLengthSlider);
            this._ec.addEventListener(SliderEvent.SET_BPM, this.setBPMSlider);
            this._ec.addEventListener(SliderEvent.SET_SCALE, this.setScaleSlider);
            this._ec.addEventListener(SliderEvent.SET_VOLUME, this.setVolumeSlider);
            return;
        }// end function

        private function slider1Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_LINE_WIDTH, Math.floor(Number(event.data) / 20)));
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_FILTER));
            return;
        }// end function

        private function slider2Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_ALPHA, Number(event.data) / 100));
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_FILTER));
            return;
        }// end function

        private function removeMusicListeners() : void
        {
            this._noteLengthSlider.removeEventListener(SliderEvent.POSITION, this.noteLengthSliderListener);
            this._scaleSlider.removeEventListener(SliderEvent.POSITION, this.scaleSliderListener);
            this._bpmSlider.removeEventListener(SliderEvent.POSITION, this.bpmSliderListener);
            this._volumeSlider.removeEventListener(SliderEvent.POSITION, this.volumeSliderListener);
            this._ec.removeEventListener(SliderEvent.SET_RELEASE, this.setNoteLengthSlider);
            this._ec.removeEventListener(SliderEvent.SET_BPM, this.setBPMSlider);
            this._ec.removeEventListener(SliderEvent.SET_SCALE, this.setScaleSlider);
            this._ec.removeEventListener(SliderEvent.SET_VOLUME, this.setVolumeSlider);
            return;
        }// end function

        private function slider6Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.ADD_STARS, Number(event.data) * 10));
            return;
        }// end function

        private function slider7Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_STARS_BRIGHTNESS, Number(event.data) / 100));
            return;
        }// end function

        private function noteLengthSliderListener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_RELEASE, Number(event.data)));
            return;
        }// end function

        private function setBPMSlider(event:SliderEvent) : void
        {
            this._bpmSlider.goto = Number(event.data);
            return;
        }// end function

        private function volumeSliderListener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_VOLUME, Number(event.data)));
            return;
        }// end function

        private function slider3Listener(event:SliderEvent) : void
        {
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_BLUR, (Math.floor(Number(event.data) / 5) - 1)));
            this._ec.dispatchEvent(new SliderEvent(SliderEvent.CHANGE_FILTER));
            return;
        }// end function

    }
}
