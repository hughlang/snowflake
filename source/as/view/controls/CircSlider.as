﻿package view.controls
{
    import events.*;
	import graphics.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.system.*;
    import gs.*;

    public class CircSlider extends Sprite
    {
        private var _linkageId:String;
        private var _handle:SliderHandle;
        private var _angleRange:Number;
        private var _minusSgin:Sprite;
        private var _box:Sprite;
        private var _handleHolder:Sprite;
        private var _plusSgin:Sprite;
        private var _per:Number;
        private var _angleShape:Sprite;
        private var _startAngle:Number;
        private var _radius:Number;
        private var bottmAngle:Number;
        private var topAngle:Number;

        public function CircSlider(param1:Number, param2:Number, param3:Number, param4:String = null)
        {
            this._startAngle = param1;
            this._angleRange = param3;
            this._radius = param2;
            this._linkageId = param4;
            this._angleShape = new Sprite();
            addChild(this._angleShape);
            this.calcAngles();
            if (this._linkageId)
            {
                this.setSliderArc();
            }
            else
            {
                this.drawSegments();
            }
            this.drawEnds();
            this.createHandle();
            this._angleShape.mouseEnabled = false;
            return;
        }// end function

        public function getStartPoint() : Point
        {
            var _loc_1:* = new Point();
            _loc_1.x = Math.cos(this.topAngle) * this._radius;
            _loc_1.y = Math.sin(this.topAngle) * this._radius;
            return _loc_1;
        }// end function

        public function set goto(param1:Number) : void
        {
            var _loc_2:* = Math.round(param1 * 100) / 100;
            var _loc_3:* = this._startAngle - this._angleRange;
            var _loc_4:* = this._startAngle + this._angleRange;
            var _loc_5:* = _loc_4 - _loc_3;
            var _loc_6:* = _loc_3 + _loc_5 * _loc_2;
            _loc_6 = _loc_6 / 180 * Math.PI;
            this._handle.x = Math.sin(_loc_6) * this._radius;
            this._handle.y = Math.cos(_loc_6) * this._radius;
            dispatchEvent(new SliderEvent(SliderEvent.POSITION, _loc_2 * 100));
            return;
        }// end function

        private function createHandle() : void
        {
            this._handleHolder = new Sprite();
            addChild(this._handleHolder);
            this._handle = new SliderHandle();
            this._handle.buttonMode = true;
            this._handle.mouseChildren = false;
            this._handle.x = Math.cos(this.topAngle) * this._radius;
            this._handle.y = Math.sin(this.topAngle) * this._radius;
            this._handleHolder.addChild(this._handle);
            this._handle.addEventListener(MouseEvent.MOUSE_DOWN, this.onStartDrag);
            return;
        }// end function

        private function calcAngles() : void
        {
            this.topAngle = this._startAngle - this._angleRange - 90;
            this.topAngle = this.topAngle / 180 * Math.PI;
            this.bottmAngle = this._startAngle + this._angleRange - 90;
            this.bottmAngle = this.bottmAngle / 180 * Math.PI;
            return;
        }// end function

        private function onStopDrag(event:MouseEvent) : void
        {
            this._handle.stopDrag();
            this._handle.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMM);
            stage.removeEventListener(MouseEvent.MOUSE_UP, this.onStopDrag);
            stage.removeEventListener(MouseEvent.MOUSE_MOVE, this.onMM);
            TweenLite.to(this._handle.icon, 0.2, {alpha:0.2});
            this.goto = 1 - this._per;
            return;
        }// end function

        private function onStartDrag(event:MouseEvent) : void
        {
            this.makeBox();
            this.drawBox();
            var _loc_2:* = this._box.getRect(this._box);
            this._handle.startDrag(false, _loc_2);
            stage.addEventListener(MouseEvent.MOUSE_MOVE, this.onMM);
            stage.addEventListener(MouseEvent.MOUSE_UP, this.onStopDrag);
            TweenLite.to(this._handle.icon, 0.3, {alpha:8});
            return;
        }// end function

        private function drawEnds() : void
        {
            this._minusSgin = new Sprite();
            this._minusSgin.graphics.lineStyle(1, 16777215);
            this._minusSgin.graphics.moveTo(-1, 0);
            this._minusSgin.graphics.lineTo(1, 0);
            this._minusSgin.graphics.endFill();
            this._minusSgin.x = this.getEndPoint().x - 2;
            this._minusSgin.y = this.getEndPoint().y + 8;
            this._minusSgin.alpha = 0.5;
            addChild(this._minusSgin);
            this._plusSgin = new Sprite();
            this._plusSgin.graphics.lineStyle(1, 16777215);
            this._plusSgin.graphics.moveTo(-2, 0);
            this._plusSgin.graphics.lineTo(2, 0);
            this._plusSgin.graphics.moveTo(0, -2);
            this._plusSgin.graphics.lineTo(0, 2);
            this._plusSgin.graphics.endFill();
            this._plusSgin.x = this.getStartPoint().x - 1;
            this._plusSgin.y = this.getStartPoint().y - 6;
            this._plusSgin.alpha = 0.5;
            addChild(this._plusSgin);
            return;
        }// end function

        public function destroy() : void
        {
            return;
        }// end function

        public function getEndPoint() : Point
        {
            var _loc_1:* = new Point();
            _loc_1.x = Math.cos(this.bottmAngle) * this._radius;
            _loc_1.y = Math.sin(this.bottmAngle) * this._radius;
            return _loc_1;
        }// end function

        private function drawBox() : void
        {
            this._box.graphics.lineStyle(1, 16777215, 0.5);
            this._box.graphics.moveTo(Math.cos(this.topAngle) * this._radius, Math.sin(this.topAngle) * this._radius);
            this._box.graphics.lineTo(this._radius, Math.sin(this.topAngle) * this._radius);
            this._box.graphics.lineTo(this._radius, Math.sin(this.bottmAngle) * this._radius);
            this._box.graphics.lineTo(Math.cos(this.topAngle) * this._radius, Math.sin(this.bottmAngle) * this._radius);
            this._box.graphics.lineTo(Math.cos(this.bottmAngle) * this._radius, Math.sin(this.topAngle) * this._radius);
            return;
        }// end function

        private function setSliderArc() : void
        {
            var _loc_1:* = null;
            var _loc_2:* = ApplicationDomain.currentDomain.getDefinition(this._linkageId) as Class;
            _loc_1 = new _loc_2;
            _loc_1.x = this._radius * Math.cos(this.topAngle);
            _loc_1.y = this._radius * Math.sin(this.topAngle);
            addChild(_loc_1);
            return;
        }// end function

        private function drawSegments() : void
        {
            var _loc_15:* = NaN;
            var _loc_16:* = NaN;
            var _loc_17:* = NaN;
            var _loc_18:* = NaN;
            var _loc_19:* = NaN;
            var _loc_20:* = NaN;
            var _loc_21:* = NaN;
            var _loc_22:* = NaN;
            var _loc_23:* = NaN;
            this._angleShape.graphics.lineStyle(0.5, 16777215, 0.2);
            var _loc_1:* = this.bottmAngle - this.topAngle;
            var _loc_2:* = Math.ceil(_loc_1 * 4 / Math.PI);
            var _loc_3:* = _loc_1 / _loc_2;
            var _loc_4:* = this._radius * Math.cos(this.topAngle);
            var _loc_5:* = this._radius * Math.sin(this.topAngle);
            var _loc_6:* = _loc_4;
            var _loc_7:* = _loc_5;
            var _loc_8:* = 0;
            var _loc_9:* = 0;
            var _loc_10:* = this.topAngle;
            var _loc_11:* = this._radius * Math.cos(this.topAngle);
            var _loc_12:* = this._radius * Math.sin(this.topAngle);
            this._angleShape.graphics.moveTo(_loc_11, _loc_12);
            var _loc_13:* = 1 / this._radius;
            var _loc_14:* = 0;
            while (_loc_14 < _loc_2)
            {
                
                _loc_10 = _loc_10 + _loc_3;
                _loc_8 = this._radius * Math.cos(_loc_10);
                _loc_9 = this._radius * Math.sin(_loc_10);
                _loc_15 = _loc_8;
                _loc_16 = _loc_9;
                _loc_17 = (_loc_4 + _loc_8) * _loc_13;
                _loc_18 = (_loc_5 + _loc_9) * _loc_13;
                _loc_19 = Math.sqrt(_loc_17 * _loc_17 + _loc_18 * _loc_18);
                _loc_17 = _loc_17 / _loc_19;
                _loc_18 = _loc_18 / _loc_19;
                _loc_20 = this._radius * _loc_17;
                _loc_21 = this._radius * _loc_18;
                _loc_22 = 2 * _loc_20 - 0.5 * (_loc_6 + _loc_15);
                _loc_23 = 2 * _loc_21 - 0.5 * (_loc_7 + _loc_16);
                this._angleShape.graphics.curveTo(_loc_22, _loc_23, _loc_15, _loc_16);
                _loc_6 = _loc_15;
                _loc_7 = _loc_16;
                _loc_4 = _loc_8;
                _loc_5 = _loc_9;
                _loc_14 = _loc_14 + 1;
            }
            return;
        }// end function

        private function makeBox() : void
        {
            this._box = new Sprite();
            this._box.x = stage.stageWidth / 2;
            this._box.y = stage.stageHeight / 2;
            return;
        }// end function

        private function onMM(event:MouseEvent) : void
        {
            var _loc_2:* = this._box.getRect(this._box);
            var _loc_3:* = _loc_2.height;
            var _loc_4:* = Math.round((this._handle.y + _loc_3 / 2) / _loc_3 * 100) / 100;
            if (this._startAngle > 0)
            {
                _loc_4 = 1 - _loc_4;
            }
            else
            {
                _loc_4 = _loc_4;
            }
            var _loc_5:* = this._startAngle - this._angleRange;
            var _loc_6:* = this._startAngle + this._angleRange;
            var _loc_7:* = _loc_6 - _loc_5;
            var _loc_8:* = _loc_5 + _loc_7 * _loc_4;
            _loc_8 = _loc_8 / 180 * Math.PI;
            this._handle.x = Math.sin(_loc_8) * this._radius;
            dispatchEvent(new SliderEvent(SliderEvent.POSITION, _loc_4 * 100));
            this._per = 1 - _loc_4;
            return;
        }// end function

    }
}
