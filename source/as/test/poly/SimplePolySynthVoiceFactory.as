﻿package test.poly
{
    import tonfall.core.*;
    import tonfall.poly.*;

    final public class SimplePolySynthVoiceFactory extends Object implements IPolySynthVoiceFactory
    {
        public static const INSTANCE:SimplePolySynthVoiceFactory = new SimplePolySynthVoiceFactory;

        public function SimplePolySynthVoiceFactory()
        {
            return;
        }// end function

        public function create(param1:TimeEventNote) : IPolySynthVoice
        {
            return new SimplePolySynthVoice();
        }// end function

    }
}
