﻿package test.poly
{
    import tonfall.core.*;
    import tonfall.poly.*;
    import tonfall.util.*;

    final public class SimplePolySynthVoice extends Object implements IPolySynthVoice
    {
        private const engine:Engine;
        private var _phaseIncr:Number;
        private var _duration:int;
        private var _remaining:int;
        private const volume:Number = 0.2;
        private var _phase:Number;

        public function SimplePolySynthVoice()
        {
            this.engine = Engine.getInstance();
            return;
        }// end function

        public function start(event:TimeEvent) : void
        {
            var _loc_2:* = TimeEventNote(event);
            this._phase = 0;
            this._phaseIncr = noteToFrequency(_loc_2.note) / samplingRate;
            var _loc_3:* = TimeConversion.barsToNumSamples(_loc_2.barDuration, this.engine.bpm);
            this._remaining = TimeConversion.barsToNumSamples(_loc_2.barDuration, this.engine.bpm);
            this._duration = _loc_3;
            return;
        }// end function

        public function stop() : void
        {
            return;
        }// end function

        public function processAdd(param1:Signal, param2:int) : Boolean
        {
            var _loc_3:* = NaN;
            var _loc_4:* = NaN;
            var _loc_5:* = 0;
            while (_loc_5 < param2)
            {
                
                if (this._remaining)
                {
                    var _loc_6:* = this;
                    _loc_6._remaining = this._remaining - 1;
                    _loc_3 = --this._remaining / this._duration - 1;
                    _loc_3 = 1 - _loc_3 * _loc_3;
                    _loc_4 = WaveFunction.biSinus(this._phase) * _loc_3 * this.volume;
                    param1.l = param1.l + _loc_4;
                    param1.r = param1.r + _loc_4;
                    param1 = param1.next;
                    this._phase = this._phase + this._phaseIncr;
                    if (this._phase >= 1)
                    {
                        (this._phase - 1);
                    }
                }
                else
                {
                    return true;
                }
                _loc_5++;
            }
            return false;
        }// end function

        public function dispose() : void
        {
            return;
        }// end function

    }
}
