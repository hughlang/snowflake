﻿package test.effects
{
    import tonfall.core.*;

    final public class Delay extends Processor
    {
        private var _bufferSize:int;
        public const output:SignalBuffer;
        private var _input:SignalBuffer;
        private var _buffer:SignalBuffer;
        private var _feedback:Number = 0.4;
        private var _wet:Number = 0.5;
        private var _dry:Number = 0.8;
        private var _bufferIndex:int;

        public function Delay(param1:int)
        {
            this.output = new SignalBuffer();
            this._bufferSize = param1 / 1000 * samplingRate;
            this._buffer = new SignalBuffer(this._bufferSize);
            this._bufferIndex = 0;
            return;
        }// end function

        public function get feedback() : Number
        {
            return this._feedback;
        }// end function

        public function get input() : SignalBuffer
        {
            return this._input;
        }// end function

        public function set wet(param1:Number) : void
        {
            this._wet = param1;
            return;
        }// end function

        public function set feedback(param1:Number) : void
        {
            this._feedback = param1;
            return;
        }// end function

        public function set input(param1:SignalBuffer) : void
        {
            this._input = param1;
            return;
        }// end function

        public function set dry(param1:Number) : void
        {
            this._dry = param1;
            return;
        }// end function

        public function get dry() : Number
        {
            return this._dry;
        }// end function

        public function get wet() : Number
        {
            return this._wet;
        }// end function

        override public function process(param1:BlockInfo) : void
        {
            var _loc_5:* = NaN;
            var _loc_6:* = NaN;
            var _loc_2:* = this._buffer.current;
            var _loc_3:* = this._input.current;
            var _loc_4:* = this.output.current;
            var _loc_7:* = 0;
            var _loc_8:* = param1.numSignals;
            while (_loc_7 < _loc_8)
            {
                
                _loc_5 = _loc_2.l;
                _loc_6 = _loc_2.r;
                _loc_2.l = _loc_3.l + _loc_5 * this._feedback;
                _loc_2.r = _loc_3.r + _loc_6 * this._feedback;
                _loc_4.l = _loc_3.l * this._dry + _loc_2.l * this._wet;
                _loc_4.r = _loc_3.r * this._dry + _loc_2.r * this._wet;
                _loc_2 = _loc_2.next;
                _loc_3 = _loc_3.next;
                _loc_4 = _loc_4.next;
                _loc_7++;
            }
            this._buffer.advancePointer(param1.numSignals);
            this.output.advancePointer(param1.numSignals);
            return;
        }// end function

    }
}
