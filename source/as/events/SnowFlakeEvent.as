﻿package events
{
    import flash.events.*;

    public class SnowFlakeEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const SHOW_EMAIL_FORM:String = "show_email_form";
        public static const GENERATE_FLAKE_AUDIO:String = "generate_flake_audio";
        public static const SHOW_FORM:String = "show_form";
        public static const EASTER_EGG_AUDIO:String = "easter_egg_audio";
        public static const DISPLAY_MESSAGE:String = "display_message";
        public static const GENERATE_FLAKE:String = "generate_flake";
        public static const OUT_OF_BOUNDS:String = "out_of_bounds";
        public static const EASTER_EGG:String = "easter_egg";
        public static const ANIMATE_TOGGLE:String = "animate_toggle";
        public static const FADE_IN_BG:String = "fade_in_bg";
        public static const HIDE_EMAIL_FORM:String = "hide_email_form";
        public static const MODE_DRAW:String = "mode_draw";
        public static const GENERATE_FLAKE_BACKGROUND:String = "generate_flake_background";
        public static const UNDO:String = "undo";
        public static const CLEAR:String = "clear";
        public static const INIT_STAGE:String = "init_stage";
        public static const CANCEL_FORM:String = "cancel_form";
        public static const MODE:String = "mode";
        public static const LOAD_FILE:String = "load_file";
        public static const SAVE_TO_DESKTOP:String = "save_to_desktop";
        public static const MODE_MUSIC:String = "mode_music";
        public static const SAVE_FILE:String = "save_file";

        public function SnowFlakeEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new SnowFlakeEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
