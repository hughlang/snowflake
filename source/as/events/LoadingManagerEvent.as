﻿package events
{
    import flash.events.*;

    public class LoadingManagerEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const LOAD_COMPLETE:String = "load_complete";
        public static const LOAD_PROGRESS:String = "load_progress";
        public static const LOAD_ERROR:String = "load_error";

        public function LoadingManagerEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new LoadingManagerEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
