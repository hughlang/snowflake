﻿package events
{
    import flash.events.*;

    public class SoundEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const SOUND_CLEAR_NOTE:String = "sound_clear_note";
        public static const SOUND_CHANGE:String = "sound_change";
        public static const SOUND_STEP:String = "sound_step";

        public function SoundEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new SoundEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
