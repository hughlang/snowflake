﻿package events
{
    import flash.events.*;

    public class StageEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const HIDE_SAVE_FORM_DONE:String = "hide_save_form_done";
        public static const HIDE_THANKS_DONE:String = "hide_thanks_done";

        public function StageEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new StageEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
