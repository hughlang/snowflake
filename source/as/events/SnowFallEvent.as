﻿package events
{
    import flash.events.*;

    public class SnowFallEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const SHOW_LOADER:String = "show_loader";
        public static const SHOW_MESSAGE:String = "showMessage";
        public static const HIDE_MESSAGE:String = "hideMessage";
        public static const TILT_DOWN:String = "tilt_down";
        public static const SHOW_SNOWFLAKE:String = "show_snowflake";
        public static const TILT_UP_DONE:String = "tilt_up_done";
        public static const HIDE_SNOWFLAKE:String = "hide_snowflake";
        public static const TILT_DOWN_DONE:String = "tilt_up_done";
        public static const CREATE_FLAKES_FROM_SNOWFALL:String = "create_flakes_from_snowfall";
        public static const TILT_UP:String = "tilt_up";
        public static const HIDE_LOADER:String = "hide_loader";

        public function SnowFallEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new SnowFallEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
