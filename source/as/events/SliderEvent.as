﻿package events
{
    import flash.events.*;

    public class SliderEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const SET_BPM:String = "set_bpm";
        public static const ADD_STARS:String = "add_stars";
        public static const CHANGE_BAR_NUMBER:String = "change_bar_number";
        public static const CHANGE_VOLUME:String = "change_volume";
        public static const CHANGE_LINE_WIDTH:String = "change_line_width";
        public static const CHANGE_GLOW:String = "change_glow";
        public static const SET_VOLUME:String = "set_volume";
        public static const CHANGE_BLUR:String = "change_blur";
        public static const CHANGE_RELEASE:String = "change_release";
        public static const CHANGE_SCALE:String = "change_scale";
        public static const CHANGE_FILTER:String = "change_filter";
        public static const POSITION:String = "SliderEvent_position";
        public static const SET_RELEASE:String = "set_release";
        public static const SET_SCALE:String = "set_scale";
        public static const CHANGE_ALPHA:String = "change_alpha";
        public static const ADD_REMOVE_NEBULAE:String = "add_remove_nebulae";
        public static const SET_BAR_NUMBER:String = "set_bar_number";
        public static const CHANGE_BPM:String = "change_bpm";
        public static const CHANGE_STARS_BRIGHTNESS:String = "change_stars_brightness";

        public function SliderEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new SliderEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
