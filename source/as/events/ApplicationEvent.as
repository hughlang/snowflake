﻿package events
{
    import flash.events.*;

    public class ApplicationEvent extends Event
    {
        private var _cancelable:Boolean;
        private var _bubbles:Boolean;
        private var _type:String;
        public var data:Object;
        public static const HIDE_MOUSE_HAND:String = "hide_mouse_hand";
        public static const SHOW_MOUSE_HAND:String = "show_mouse_hand";
        public static const SELECT_SCREEN:String = "select_screen";
        public static const DISPLAY_ERROR:String = "display_error";
        public static const DATA_LOADED:String = "data_loaded";
        public static const CHANGE_NAV:String = "change_nav";
        public static const APP_DATA_LOADED:String = "app_data_loaded";
        public static const INIT_VIEWS:String = "init_views";

        public function ApplicationEvent(param1:String, param2:Object = null, param3:Boolean = false, param4:Boolean = false)
        {
            super(param1, param3, param4);
            this.data = param2;
            this._type = param1;
            this._bubbles = param3;
            this._cancelable = param4;
            return;
        }// end function

        override public function clone() : Event
        {
            return new ApplicationEvent(this._type, this.data, this._bubbles, this._cancelable);
        }// end function

    }
}
