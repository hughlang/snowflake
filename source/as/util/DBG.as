package util
{
	import com.demonsters.debugger.MonsterDebugger;
	public class DBG 
	{
		public function DBG()
		{
		}
		
		public static function log(msg:String):void 
		{
//			trace(msg);
			MonsterDebugger.log(msg);
		}
		public static function trace(caller:*, object:*):void 
		{
			MonsterDebugger.trace(caller, object);
			
		}
	}
}