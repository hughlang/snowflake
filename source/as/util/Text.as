﻿package util
{
    import flash.text.*;

    public class Text extends TextField
    {

        public function Text(param1:String, param2:TextFormat, param3:Number = 100, param4:Boolean = true, param5:Boolean = false, param6:Boolean = true)
        {
            if (param2)
            {
                defaultTextFormat = param2;
            }
            if (param5)
            {
                htmlText = param1;
            }
            else
            {
                text = param1;
            }
            embedFonts = param6;
            selectable = false;
            wordWrap = param4;
            multiline = param4;
            if (param3 == 0)
            {
                width = textWidth + 5;
            }
            else
            {
                width = param3;
            }
            height = textHeight + 5;
            return;
        }// end function

    }
}
