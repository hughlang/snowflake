﻿package util
{
    import flash.text.*;

    public class Formats extends Object
    {

        public function Formats()
        {
            return;
        }// end function

        public static function nameFormat() : TextFormat
        {
            var _loc_1:* = new TextFormat();
            _loc_1.font = "Trebuchet MS";
            _loc_1.size = 12;
            _loc_1.color = 16777215;
            return _loc_1;
        }// end function

        public static function errorFormat() : TextFormat
        {
            var _loc_1:* = new TextFormat();
            _loc_1.font = "Arial";
            _loc_1.size = 10;
            _loc_1.color = 16711680;
            return _loc_1;
        }// end function

    }
}
