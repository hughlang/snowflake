﻿package util
{
    import com.adobe.images.*;
    import com.dynamicflash.util.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;

    public class ImageGenerator extends EventDispatcher
    {

        public function ImageGenerator()
        {
            return;
        }// end function

        public static function createCompositeImage(param1:Sprite, param2:Sprite) : Bitmap
        {
            var _loc_3:* = new Sprite();
            var _loc_4:* = takeScreenShot(param1);
            var _loc_5:* = takeScreenShotWH(param2, 560, 400);
            _loc_3.addChild(_loc_5);
            _loc_4.x = _loc_5.width / 2 - _loc_4.width / 2;
            _loc_4.y = _loc_5.height / 2 - _loc_4.height / 2;
            _loc_3.addChild(_loc_4);
            return takeScreenShot(_loc_3);
        }// end function

        public static function getJpgStream(param1:Sprite) : ByteArray
        {
            var _loc_2:* = new BitmapData(param1.width * 0.12, param1.height * 0.12, true, 0);
            var _loc_3:* = param1.getBounds(param1);
            var _loc_4:* = new Matrix();
            _loc_4.identity();
            _loc_4.scale(0.1, 0.1);
            _loc_4.translate((-_loc_3.x) * 0.12, (-_loc_3.y) * 0.12);
            _loc_2.draw(param1, _loc_4);
            var _loc_5:* = PNGEncoder.encode(_loc_2);
            return _loc_5;
        }// end function

        public static function getJpgString(param1:Sprite) : String
        {
            var _loc_2:* = 0.1;
            var _loc_3:* = new BitmapData(param1.width * (_loc_2 + 0.2), param1.height * (_loc_2 + 0.2), true, 0);
            var _loc_4:* = param1.getBounds(param1);
            var _loc_5:* = new Matrix();
            _loc_5.identity();
            _loc_5.scale(_loc_2, _loc_2);
            _loc_5.translate((-_loc_4.x) * (_loc_2 + 0.2), (-_loc_4.y) * (_loc_2 + 0.2));
            _loc_3.draw(param1, _loc_5);
            var _loc_6:* = PNGEncoder.encode(_loc_3);
            var _loc_7:* = Base64.encodeByteArray(_loc_6);
            return _loc_7;
        }// end function

        public static function takeScreenShot(param1:Sprite) : Bitmap
        {
            var _loc_2:* = new BitmapData(param1.width, param1.height, true, 0);
            var _loc_3:* = param1.getBounds(param1);
            var _loc_4:* = new Matrix();
            _loc_4.identity();
            _loc_4.scale(1, 1);
            _loc_4.translate(-_loc_3.x, -_loc_3.y);
            _loc_2.draw(param1, _loc_4);
            var _loc_5:* = new Bitmap(_loc_2, "auto", true);
            _loc_5.smoothing = true;
            return _loc_5;
        }// end function

        public static function takeScreenShotWH(param1:Sprite, param2:Number, param3:Number) : Bitmap
        {
            var _loc_4:* = new BitmapData(param2, param3, false, 0);
            var _loc_5:* = new Matrix();
            _loc_5.identity();
            _loc_5.scale(1, 1);
            _loc_5.translate((-param1.stage.stageWidth) / 2 + 560 / 2, (-param1.stage.stageHeight) / 2 + 400 / 2);
            _loc_4.draw(param1, _loc_5);
            var _loc_6:* = new Bitmap(_loc_4, "auto", true);
            _loc_6.smoothing = true;
            return _loc_6;
        }// end function

        public static function getJpgStringFromBMP(param1:Bitmap) : String
        {
            var _loc_2:* = new JPGEncoder(85);
            var _loc_3:* = _loc_2.encode(param1.bitmapData);
            var _loc_4:* = Base64.encodeByteArray(_loc_3);
            return _loc_4;
        }// end function

    }
}
