﻿package util
{
    import br.com.stimuli.loading.*;
    import events.*;
    import flash.display.*;
    import flash.events.*;

    public class LoadingManager extends EventDispatcher
    {
        private var _loader:BulkLoader;
        private var cueArray:Array;
        public static const IMAGE:String = "image";
        private static var inst:LoadingManager;
        public static const MOVIE_CLIP:String = "movie_clip";

        public function LoadingManager() : void
        {
            this.cueArray = [];
            this._loader = new BulkLoader("_as3_mvc_template_loader");
            this._loader.logLevel = BulkLoader.LOG_ERRORS;
            this._loader.addEventListener(BulkLoader.COMPLETE, this.onAllItemsLoaded);
            this._loader.addEventListener(BulkLoader.PROGRESS, this.onAllItemsProgress);
            return;
        }// end function

        public function acquireBatch(param1:Array) : void
        {
            var _loc_2:* = 0;
            var _loc_3:* = 0;
            while (_loc_3 < param1.length)
            {
                
                if (this._loader.getBitmap(param1[_loc_3]))
                {
                    _loc_2++;
                }
                else
                {
                    this._loader.add(param1[_loc_3], {id:param1[_loc_3], maxTries:6, priority:param1.length - _loc_3});
                }
                _loc_3 = _loc_3 + 1;
            }
            if (_loc_2 == param1.length)
            {
                dispatchEvent(new LoadingManagerEvent(LoadingManagerEvent.LOAD_COMPLETE));
            }
            else
            {
                this._loader.start();
            }
            return;
        }// end function

        public function acquireSingle(param1:String) : void
        {
            if (this._loader.getBitmap(param1))
            {
                dispatchEvent(new LoadingManagerEvent(LoadingManagerEvent.LOAD_COMPLETE));
            }
            else
            {
                this._loader.add(param1, {id:param1, maxTries:6, priority:10});
                this._loader.start();
            }
            return;
        }// end function

        public function getImageByName(param1:String) : Bitmap
        {
            var bmp:Bitmap;
            var bmpData:BitmapData;
            var str:* = param1;
            try
            {
                bmpData = this._loader.getBitmap(str).bitmapData.clone();
                bmp = new Bitmap(bmpData);
            }
            catch (e:Error)
            {
                trace("Error getting BMP: ", str, e);
                bmp = new Bitmap(new BitmapData(1, 1));
            }
            return bmp;
        }// end function

        private function onAllItemsLoaded(event:Event) : void
        {
            dispatchEvent(new LoadingManagerEvent(LoadingManagerEvent.LOAD_COMPLETE));
            return;
        }// end function

        private function onAllItemsProgress(event:BulkProgressEvent) : void
        {
            var _loc_2:* = Math.floor(event.percentLoaded * 100);
            if (Math.round(event.percentLoaded * 100) < 100)
            {
                dispatchEvent(new LoadingManagerEvent(LoadingManagerEvent.LOAD_PROGRESS, _loc_2));
            }
            return;
        }// end function

        public function getObjectByName(param1:String, param2:String)
        {
            switch(param2)
            {
                case IMAGE:
                {
                    return this._loader.getBitmap(param1);
                }
                case MOVIE_CLIP:
                {
                    return this._loader.getMovieClip(param1);
                }
                default:
                {
                    break;
                }
            }
            return;
        }// end function

        public static function getInstance() : LoadingManager
        {
            if (inst == null)
            {
                inst = new LoadingManager;
            }
            return inst;
        }// end function

    }
}
