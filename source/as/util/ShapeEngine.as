﻿package util
{
    import flash.display.*;
    import flash.geom.*;

    public class ShapeEngine extends Object
    {

        public function ShapeEngine()
        {
            return;
        }// end function

        public static function drawTriangleSpr(param1:Sprite, param2:Point, param3:Number, param4:Number) : Sprite
        {
            var _loc_5:* = new Sprite();
            _loc_5.graphics.copyFrom(param1.graphics);
            _loc_5.graphics.moveTo(param2.x, param2.y - param3 / 2);
            _loc_5.graphics.lineTo(param2.x + param3 / 2, param2.y + param3 / 2);
            _loc_5.graphics.lineTo(param2.x - param3 / 2, param2.y + param3 / 2);
            _loc_5.graphics.lineTo(param2.x, param2.y - param3 / 2);
            _loc_5.rotation = param4;
            return _loc_5;
        }// end function

        public static function drawTriangle(param1:Sprite, param2:Point, param3:Number, param4:Number) : void
        {
            param1.graphics.moveTo(param2.x, param2.y);
            param1.graphics.lineTo(param2.x + Math.cos(param4 + 45 * Math.PI / 180) * param3, param2.y + Math.sin(param4 + 45 * Math.PI / 180) * param3);
            param1.graphics.lineTo(param2.x + Math.cos(param4 + 90 * Math.PI / 180) * param3, param2.y + Math.sin(param4 + 90 * Math.PI / 180) * param3);
            param1.graphics.lineTo(param2.x, param2.y);
            return;
        }// end function

    }
}
