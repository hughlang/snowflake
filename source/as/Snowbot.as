package
{
	import com.demonsters.debugger.MonsterDebugger;
	
	import control.RootController;
	
	import flash.desktop.NativeApplication;
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.geom.Rectangle;
	
	import starling.core.Starling;
	
	import util.DBG;
	import util.StringTool;
	
	/**
	 * SWF meta data defined for iPad 1 & 2 in landscape mode. 
	 */	
	[SWF(frameRate="60", width="1024", height="752", backgroundColor="0x000000")]
	
	/**
	 * This is the main class of the project. 
	 * 
	 * @author hsharma
	 * 
	 */
	public class Snowbot extends Sprite
	{
		/** Starling object. */
		private var mStarling:Starling;
		
		public function Snowbot()
		{
			super();
			
			// set general properties
			MonsterDebugger.initialize(this);
			
			stage.scaleMode = StageScaleMode.NO_SCALE;
//			stage.align = StageAlign.TOP_LEFT;

			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		/**
		 * On added to stage. 
		 * @param event
		 * 
		 */
		protected function onAddedToStage(event:Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			// Create a suitable viewport for the screen size.
			// 
			// On the iPhone, the viewPort will fill the complete screen; on the iPad, there
			// will be black bars at the left and right, because it has a different aspect ratio.
			// Since the game is scaled up a little, it won't be as perfectly sharp as on the 
			// iPhone. Alernatively, you can exchange this code with the one used in the Starling
			// demo: it does not scale the viewPort, but adds bars on all sides.
			
			var screenWidth:int  = stage.fullScreenWidth;
			var screenHeight:int = stage.fullScreenHeight;
			var viewPort:Rectangle = new Rectangle();
			viewPort.height = screenHeight;
			viewPort.width = screenWidth; 

			DBG.trace(this, StringTool.format("width x height = {0} / {1}", screenWidth, screenHeight));
//			if (stage.fullScreenHeight / stage.fullScreenWidth < Constants.ASPECT_RATIO)
//			{
//				viewPort.height = screenHeight;
//				viewPort.width  = int(viewPort.height / Constants.ASPECT_RATIO);
//				viewPort.x = int((screenWidth - viewPort.width) / 2);
//			}
//			else
//			{
//				viewPort.width = screenWidth; 
//				viewPort.height = int(viewPort.width * Constants.ASPECT_RATIO);
//				viewPort.y = int((screenHeight - viewPort.height) / 2);
//			}
			
			// While Stage3D is initializing, the screen will be blank. To avoid any flickering, 
			// we display a startup image for now, but will remove it when Starling is ready to go.
			//
			// (Note that we *cannot* embed the "Default*.png" images, because then they won't
			//  be copied into the package any longer once they are embedded.)
			
			//			var startupImage:Sprite = createStartupImage(viewPort, screenWidth > 320);
			//			addChild(startupImage);
			
			// Set up Starling

			// Initialize Starling object.
			Starling.multitouchEnabled = true;  // useful on mobile devices
			Starling.handleLostContext = false; // not necessary on iOS. Saves a lot of memory!
			
			mStarling = new Starling(Game, stage);
			mStarling.simulateMultitouch = false;
			mStarling.enableErrorChecking = false;
			
			// Define basic anti aliasing.
			mStarling.antiAliasing = 1;
			
			// Show statistics for memory usage and fps.
			mStarling.showStats = true;
			
			// Position stats.
			mStarling.showStatsAt("left", "bottom");
			
			mStarling.stage3D.addEventListener(Event.CONTEXT3D_CREATE, function(e:Event):void 
			{
				// Starling is ready! We remove the startup image and start the game.
				DBG.log("Starling is ready");
				
				//				removeChild(startupImage);
				mStarling.start();
			});
			
			
			// When the game becomes inactive, we pause Starling; otherwise, the enter frame event
			// would report a very long 'passedTime' when the app is reactivated. 
			
			NativeApplication.nativeApplication.addEventListener(Event.ACTIVATE, 
				function (e:Event):void { mStarling.start(); });
			
			NativeApplication.nativeApplication.addEventListener(Event.DEACTIVATE, 
				function (e:Event):void { mStarling.stop(); });

		}	

//		private function createStartupImage(viewPort:Rectangle, isHD:Boolean):Sprite
//		{
//			var sprite:Sprite = new Sprite();
//			
//			var background:Bitmap = isHD ?
//				new AssetEmbeds_2x.Background() : new AssetEmbeds_1x.Background();
//			
//			var loadingIndicator:Bitmap = isHD ?
//				new AssetEmbeds_2x.Loading() : new AssetEmbeds_1x.Loading();
//			
//			background.smoothing = true;
//			sprite.addChild(background);
//			
//			loadingIndicator.smoothing = true;
//			loadingIndicator.x = (background.width - loadingIndicator.width) / 2;
//			loadingIndicator.y =  background.height * 0.75;
//			sprite.addChild(loadingIndicator);
//			
//			sprite.x = viewPort.x;
//			sprite.y = viewPort.y;
//			sprite.width  = viewPort.width;
//			sprite.height = viewPort.height;
//			
//			return sprite;
//		}

	}
}